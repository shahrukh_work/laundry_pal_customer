//
//  Notification.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/2/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias NotificationResults = (_ data: Notification?, _ error: NSError?) -> Void

class Notification: Mappable {
    
    var notification = -1
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        notification <- map["notifications"]
    }
    
    class func getNotification(completion: @escaping NotificationResults) {
        
        APIClient.shared.getNotification { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<Notification>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
    
    class func setNotification(status: Int, completion: @escaping PostApiCompletionHandler) {
        
        APIClient.shared.setNotification(status: status) { (data, error, status) in
            
            if error != nil {
                
                if let data = Mapper<PostApi>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}
