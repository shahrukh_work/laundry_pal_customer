//
//  Featured.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/6/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias FeaturedResult = (_ data: Featured?, _ error: NSError?) -> Void

class Featured: Mappable {
    
    var featured = [Features]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        featured <- map["featured items"]
    }
    
    class func getFeatured(offset: Int, limit: Int, featuredType: Int, completion: @escaping FeaturedResult) {
        
        APIClient.shared.getFeatured(offset: offset, limit: limit, featuredType: featuredType) { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<Featured>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}

class Features: Mappable {
    
    var id = -1
    var name = ""
    var featuredType = ""
    var description = ""
    var image = ""
    var status = -1
    var createdAt = ""
    var updatedAt = ""
    
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id           <- map["id"]
        name         <- map["name"]
        featuredType <- map["featured_type"]
        description  <- map["description"]
        image        <- map["image"]
        status       <- map["status"]
        createdAt    <- map["created_at"]
        updatedAt    <- map["updated_at"]
    }
}
