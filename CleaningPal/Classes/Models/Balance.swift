//
//  Balance.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/2/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper


typealias BalanceResult = (_ data: Balance?, _ error: NSError?) -> Void

class Balance: Mappable {
    
    var availableAmount = -1
    var discount = -1
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        availableAmount <- map["available_amount"]
        discount <- map["discount"]
    }
    
    class func getBalance(completion: @escaping BalanceResult) {
        
        APIClient.shared.getBalance { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<Balance>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}
