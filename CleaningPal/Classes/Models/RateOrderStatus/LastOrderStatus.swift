

import Foundation
import ObjectMapper

typealias LastOrderStatusCompletion = (_ data: OrderStatusData?, _ error: NSError?) -> Void

class LastOrderStatus: Mappable {
	var error : Bool?
	var message : String?
    var data = Mapper<OrderStatusData>().map(JSON: [:])!
	var errors : [String]?

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		error <- map["error"]
		message <- map["message"]
		data <- map["data"]
		errors <- map["errors"]
	}

    class func getLastOrderStatus(_ completion: @escaping LastOrderStatusCompletion) {
        
        APIClient.shared.getLastOrderStatus { (result, error, status) in
            
            if error == nil {
                
                if let data = Mapper<OrderStatusData>().map(JSONObject: result) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}
