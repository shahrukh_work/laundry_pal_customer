

import Foundation
import ObjectMapper

class OrderStatusData : Mappable {
    var data = Mapper<OrderStatusKeys>().map(JSON: [:])

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		data <- map["data"]
	}

}

class OrderStatusKeys: Mappable {
    var id = -1
    var isSkip = false
    var isRate = false
    
    required init?(map: Map) {

    }

    func mapping(map: Map) {

        id <- map["id"]
        isSkip <- map["is_skip"]
        isRate <- map["is_rate"]
    }
}
