//
//  UserStatus.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/21/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias UserStatusResponse = (_ data: UserStatus?, _ error: NSError?) -> Void

class UserStatus: Mappable {
    
    var status = -4
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        status <- map["status"]
    }
    
    class func getCustomerStatus(phone: String, completion: @escaping UserStatusResponse) {
        
        APIClient.shared.customerStatus(phone: phone) { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<UserStatus>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}
