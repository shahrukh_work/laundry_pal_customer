//
//  GST.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/15/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias GSTResponse = (_ data: Gst?, _ error: NSError?) -> Void

class Gst: Mappable {
    
    var gstRate = 0
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        gstRate <- map["tax_rate"]
    }
    
    class func getGSTRate(state: String, completion: @escaping GSTResponse) {
        
        APIClient.shared.getGST(state: state) { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<Gst>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}
