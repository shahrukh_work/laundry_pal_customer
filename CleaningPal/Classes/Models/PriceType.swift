//
//  PriceType.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 6/26/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation

class PriceType {
    var name = ""
    var isCollapsed = true
    
    init(name: String) {
        self.name = name
    }
}
