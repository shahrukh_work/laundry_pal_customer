//
//  Location.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/10/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

class Location: Mappable {
    
    var locationName = ""
    var locationCoordinates = ""
    var city = ""
    var state = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        locationName            <- map["location_name"]
        locationCoordinates     <- map["location_coordinates"]
        city                    <- map["loc_city"]
        state                   <- map["loc_state"]
    }
}
