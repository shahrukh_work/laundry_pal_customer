//
//  SelectedType.swift
//  CleaningPal
//
//  Created by Mac on 14/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import  UIKit

enum SelectedType {
    case men
    case women
    case kids
}

class PersonType {
    var title = ""
    var type: SelectedType = .men
    var isSelected = false
    
}

class CategoryType {
    var title = ""
    var image: UIImage? = nil
    var isSelected = false
}
