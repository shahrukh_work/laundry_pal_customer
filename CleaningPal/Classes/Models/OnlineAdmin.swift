//
//  OnlineAdmin.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/15/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias OnlineAdminResults = (_ data: OnlineAdmin?, _ error: NSError?) -> Void

class OnlineAdmin: Mappable {
    
    var id = -1
    var name = ""
    var image = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id    <- map["id"]
        name  <- map["name"]
        image <- map["image"]
    }
    
    class func getOnlineAdmin(completion: @escaping OnlineAdminResults) {
        
        APIClient.shared.getOnlineAdmin { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<OnlineAdmin>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}
