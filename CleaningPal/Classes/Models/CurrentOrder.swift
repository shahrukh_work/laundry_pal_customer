//
//  CurrentOrder.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/9/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias CurrentOrderResults = (_ data: CurrentOrder?, _ error: NSError?) -> Void

class CurrentOrder: Mappable {
    
    var error = false
    var message = ""
    var data = [CurrentOrderData]()
    var errors = [String]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error   <- map["error"]
        message <- map["message"]
        data    <- map["data"]
        errors  <- map["errors"]
    }
    
    class func getCurrentOrder(completion: @escaping CurrentOrderResults) {
        
        APIClient.shared.getCurrentOrders { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<CurrentOrder>().map(JSONObject: data) {
                    
                    if data.error == true {
                        completion(nil, NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey : data.message]))
                        return
                    }
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}

class CurrentOrderData: Mappable {
    
    var id = -1
    var totalItems = -1
    var totalPrice = -1
    var discountedPrice = -1
    var paymentMethod = ""
    var pickupDateTime = ""
    var orderType = ""
    var status = -1
    var availableStartTime = ""
    var availableEndTime = ""
    var pickupAddress = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id                  <- map["id"]
        totalItems          <- map["total_items"]
        totalPrice          <- map["total_price"]
        discountedPrice     <- map["discounted_price"]
        paymentMethod       <- map["payment_method"]
        pickupDateTime      <- map["pickup_date"]
        orderType           <- map["order_type"]
        status              <- map["status"]
        availableStartTime  <- map["available_start_time"]
        availableEndTime    <- map["available_end_time"]
        pickupAddress       <- map["pickup_address"]
    }
}
