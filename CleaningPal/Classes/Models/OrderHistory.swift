//
//  OrderHistory.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/6/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias OrderHistoryResults = (_ data: OrderHistories?, _ error: NSError?) -> Void

class OrderHistories: Mappable {
    
    var orderHistory = [OrderHistroy]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        orderHistory <- map["Order History"]
    }
    
    class func getOrderHistory(offset: Int, limit: Int, completion: @escaping OrderHistoryResults) {
        
        APIClient.shared.getOrderHistory(offset: offset, limit: limit) { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<OrderHistories>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}

class OrderHistroy: Mappable {
    
    var orderId = -1
    var pickupDateTime = ""
    var totalPrice = -1
    var customerName = ""
    var driverName = ""
    var quantity = -1
    var availableStartTime = ""
    var availableEndTime = ""
    var discountedPrice = 0
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        
        orderId             <- map["orders_id"]
        pickupDateTime      <- map["pickup_datetime"]
        totalPrice          <- map["total_price"]
        customerName        <- map["customer_name"]
        driverName          <- map["driver_name"]
        quantity            <- map["quantity"]
        availableStartTime  <- map["available_start_time"]
        availableEndTime    <- map["available_end_time"]
        discountedPrice     <- map["discounted_price"]
    }
}
