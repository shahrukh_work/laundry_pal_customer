//
//  PostApis.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 5/29/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper
import UIKit


typealias PostApiCompletionHandler = (_ data: PostApi?,_ error: NSError?) -> Void

class PostApi: Mappable {
    
    var message = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        message  <- map["data"]
    }
    
    class func fcmUpdate(fcmToken: String, completion: @escaping PostApiCompletionHandler) {
        
        APIClient.shared.fcmUpdate(fcmToken: fcmToken) { (data, error, status) in
            
            if error != nil {
                
                if let data = Mapper<PostApi>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
    
    class func updateProfile(firstName: String, lastName: String, phone: String, email:String, image: UIImage, completion: @escaping PostApiCompletionHandler) {
        
        APIClient.shared.updateProfileData(firstName: firstName, lastName: lastName, email: email, phoneNumber: phone, profileImage: image) { (data, error, status) in
            
            if error != nil {
                
                if let data = Mapper<PostApi>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: error?.localizedDescription ?? "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
    
    class func rateDriver(driverId: Int, performance: String, stars: Double, message: String,  completion: @escaping PostApiCompletionHandler) {
        
        APIClient.shared.rateDriver(driverId: driverId, performance: performance, stars: stars, message: message) { (data, error, status) in
            
            if error != nil {
                
                if let data = Mapper<PostApi>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
    
    class func cancelOrder(orderID: Int, completion: @escaping PostApiCompletionHandler) {
        
        APIClient.shared.cancelOrder(orderId: orderID) { (data, error, status) in
            
            if error != nil {
                
                if let data = Mapper<PostApi>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
    
    class func sendComplaint(email: String, message: String, completion: @escaping PostApiCompletionHandler) {
        
        APIClient.shared.sendComplaint(email: email, message: message) { (data, error, status) in
            
            if error != nil {
                
                if let data = Mapper<PostApi>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
    
    class func customerLogout(completion: @escaping PostApiCompletionHandler) {
        
        APIClient.shared.logoutUser { (data, error, status) in
            
            if error != nil {
                
                if let data = Mapper<PostApi>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}
