//
//  BottomFeatured.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/10/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias BottomFeaturedResults = (_ data: BottomFeatured?, _ error: NSError?) -> Void

class BottomFeatured: Mappable {
    
    var featured = [BottomFeatures]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        featured      <- map["featured items"]
    }
    
    class func getFeatured(completion: @escaping BottomFeaturedResults) {
        
        APIClient.shared.getBottomFeatured { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<BottomFeatured>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}

class BottomFeatures: Mappable {
    
    var image = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        image        <- map["image"]
    }
}
