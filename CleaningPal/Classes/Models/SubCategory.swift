//
//  SubCategory.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/6/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias SubCategoriesResult = (_ data: SubCategories?, _ error: NSError?) -> Void

class SubCategories: Mappable {
    
    var error = false
    var message = ""
    var errors = [String]()
    var subCategories = [SubCategory]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        subCategories <- map["data"]
        error   <- map["error"]
        message <- map["message"]
        errors  <- map["errors"]
    }
    
    class func getSubCategories(categoryId: Int, completion: @escaping SubCategoriesResult) {
        
        APIClient.shared.getSubCategories(categoryId: categoryId) { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<SubCategories>().map(JSONObject: data) {
                    
                    if data.error == true {
                        completion(nil, NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey : data.message]))
                        return
                    }
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}

class SubCategory: Mappable {
    
    var name = ""
    var id = -1
    var categoryId = -1
    var isSelected = false
    var isCollapsed = true
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        name        <- map["name"]
        id          <- map["id"]
        categoryId  <- map["category_id"]
    }
}

