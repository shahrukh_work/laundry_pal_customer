//
//  CardStatus.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/16/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias CardStatusResponse = (_ data: CardPaymentStatus?, _ error: NSError?) -> Void

class CardPaymentStatus: Mappable {
    
    var error = false
    var message = ""
    var errors = [String]()
    var cardStatus = [CardStatus]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error       <- map["error"]
        errors      <- map["errors"]
        message     <- map["message"]
        cardStatus  <- map["data"]
    }
    
    class func getCardStatus(completion: @escaping CardStatusResponse) {
        
        APIClient.shared.getCardStatus{ (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<CardPaymentStatus>().map(JSONObject: data) {
                    
                    if data.error == true {
                        completion(nil, NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey : data.message]))
                        return
                    }
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}


class CardStatus: Mappable {
    
    var id = -1
    var paymentMethodName = ""
    var status = -1
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id                  <- map["id"]
        paymentMethodName   <- map["payment_method_name"]
        status              <- map["status"]
    }
    
}
