//
//  LaundaryOptions.swift
//  CleaningPal
//
//  Created by Mian Faizan Nasir on 6/16/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation

class LaundaryOption {
    var name = ""
    var price = 0
    var isSelected = false
    
    init(name: String, price: Int) {
        self.name = name
        self.price = price
    }
}
