//
//  Cart.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/9/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper


class Cart: Mappable {
    
    var cartItems = [CartItem]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        cartItems       <- map["items"]
    }
    
    func getJSON() -> [String: [[String: AnyObject]]] {
        var items = [[String: AnyObject]]()
        
        cartItems.forEach { (item) in
            items.append(item.getJSON())
        }
        
        return ["items": items]
    }
    
    func addToCart(cartItem: CartItem) {
        
        let index = cartItems.firstIndex { (item) in
            return item.itemId == cartItem.itemId && item.washingMethod == cartItem.washingMethod
        }
        
        if let index = index {
            cartItems[index].quantity = cartItems[index].quantity + cartItem.quantity
            
        } else {
            cartItems.append(cartItem)
        }
    }
    
    func updateItemCount(cartItem: CartItem) {
        
        let index = cartItems.firstIndex { (item) in
            return item.itemId == cartItem.itemId && item.washingMethod == cartItem.washingMethod
        }
        
        if let index = index {
            
            if cartItem.quantity == 0 {
                cartItems.remove(at: index)
                return
            }
            
            cartItems[index].quantity = cartItem.quantity
        }
    }
}


class CartItem: Mappable {
    
    var itemId = -1
    var price = -1
    var washingMethod = ""
    var quantity = -1
    var name = ""
    var subCat = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        itemId          <- map["item_id"]
        price           <- map["price"]
        washingMethod   <- map["washing_method"]
        quantity        <- map["quantity"]
        name            <- map["name"]
        subCat          <- map["sub_cat"]
    }
    
    func getJSON() -> [String: AnyObject] {
        return [
            "item_id" : itemId,
            "price" : price,
            "washing_method" : washingMethod,
            "quantity": quantity
            
        ] as [String: AnyObject]
    }
}
