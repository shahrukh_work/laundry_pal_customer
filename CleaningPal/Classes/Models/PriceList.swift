//
//  PriceList.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/6/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias PriceListResult = (_ data: PriceLists?, _ error: NSError?) -> Void

class PriceLists: Mappable {
    
    var priceList = [PriceList]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        priceList <- map["prices_list"]
    }
    
    class func getPriceList(subCatId: Int, completion: @escaping PriceListResult) {
        
        APIClient.shared.getPriceList(subCatId: subCatId) { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<PriceLists>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}

class PriceList: Mappable {
    
    var name = ""
    private var washingMethodsString = ""
    var washingMethods = [WashingMethod]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        name                  <- map["name"]
        washingMethodsString  <- map["item_washing_methods"]
        postMap()
    }
    
    func postMap() {
        
        if let data = Mapper<WashingMethod>().mapArray(JSONString: washingMethodsString) {
            washingMethods = data
        }
    }
}

class WashingMethod: Mappable {
    
    var name = ""
    var price = 0
    var id = -1
    var position = -1
    var isSelected = false
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        name        <- map["washing_method_name"]
        price       <- map["price"]
        id          <- map["washing_method_id"]
        position    <- map["position"]
    }
}
