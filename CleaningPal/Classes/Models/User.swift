//
//  User.swift
//  CleaningPal
//
//  Created by Mac on 11/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper
import SocketIO

class AppUser: Mappable {
    
    static let shared = Mapper<AppUser>().map(JSON: [:])!
    static var authData = DataManager.shared.getAuthentication()
    var showLoader = false
    var country = Mapper<Country>().map(JSON: [:])!
    var cartItemsCount = 0
    var phoneNumber = ""
    var phoneWithoutCode = ""
    var fcm = ""
    var socketManager: SocketManager!
    var socket:SocketIOClient!
    
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
    }
}
