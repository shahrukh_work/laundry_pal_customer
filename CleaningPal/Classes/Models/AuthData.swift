//
//  AuthData.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/2/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias LoginResults = (_ data: AuthData?, _ error: NSError?) -> Void

class AuthData: Mappable {
    
    var user: AuthUser = Mapper<AuthUser>().map(JSON: [:])!
    var token = ""
    var status = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        user    <- map["user"]
        token   <- map["token"]
        status  <- map["status"]
    }
    
    
    class func login(phone: String, token:String, completion: @escaping LoginResults) {
        
        APIClient.shared.login(phone: phone, token: token) { (data, error, status) in
            
            if error == nil {
                
                if let auth = Mapper<AuthData>().map(JSONObject: data) {
                    
                    if auth.status == "ACTIVE" {
                        DataManager.shared.setAuthentication(auth: auth.toJSONString()!)
                        let user = auth.user
                        DataManager.shared.setUser(user: user.toJSONString()!)
                    }
                    AppUser.authData = auth
                    completion(auth, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
    
    class func signup(firstName: String, lastName: String, phone: String, email:String, completion: @escaping LoginResults) {
        
        APIClient.shared.signup(firstName: firstName, lastName: lastName, phone: phone, email: email) { (data, error, status) in
            
            if error == nil {
                
                if let auth = Mapper<AuthData>().map(JSONObject: data) {
                    DataManager.shared.setAuthentication(auth: auth.toJSONString()!)
                    
                    let user = auth.user
                    AppUser.authData = auth
                    DataManager.shared.setUser(user: user.toJSONString()!)
                    completion(auth, nil)
                    
                } else {
                    completion(nil, NSError(domain: "" , code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
    
    
}
