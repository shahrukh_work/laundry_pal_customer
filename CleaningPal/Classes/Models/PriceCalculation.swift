//
//  PriceCalculation.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/30/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias PriceCalculationResponse = (_ data: PriceCalculation?, _ error: NSError?) -> Void

class PriceCalculation: Mappable {
    
    var totalPrice = ""
    var totalCount = -1
    var promo = ""
    var gst = ""
    var discountedPrice = 0
    var walletAmount = 0
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        totalPrice      <- map["total_price"]
        totalCount      <- map["total_count"]
        promo           <- map["promo"]
        gst             <- map["gst"]
        discountedPrice <- map["discounted_price"]
        walletAmount    <- map["wallet_amount"]
    }
    
    class func calculatePrice(params: [String: AnyObject], completion: @escaping PriceCalculationResponse) {
        
        APIClient.shared.calculatePrice(params: params) { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<PriceCalculation>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}
