//
//  Chat.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/15/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias ChatHistoryResults = (_ data: ChatHistory?, _ error: NSError?) -> Void

class ChatHistory: Mappable {
    
    var chat = [Chat]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        chat <- map["chat_history"]
    }
    
    class func getChatHistory(completion: @escaping ChatHistoryResults) {
        
        APIClient.shared.getChatHistory { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<ChatHistory>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}


class Chat: Mappable {
    
    var customerImage = ""
    var adminImage = ""
    var customerId = -1
    var adminId = -1
    var message = ""
    var createdAt = ""
    var sendBy = -1
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        customerImage  <- map["customer_image"]
        adminId        <- map["admin_image"]
        customerId     <- map["customer_id"]
        adminId        <- map["admin_id"]
        message        <- map["message"]
        createdAt      <- map["created_at"]
        sendBy         <- map["send_by"]
    }
}
