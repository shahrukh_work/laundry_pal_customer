//
//  Category.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/2/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias CategoriesResult = (_ data: Categories?, _ error: NSError?) -> Void

class Categories: Mappable {
    
    var categories = [Category]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        categories <- map["categories"]
    }
    
    class func getCategories(showLoading: Bool = AppUser.shared.showLoader,completion: @escaping CategoriesResult) {
        
        APIClient.shared.getCategories { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<Categories>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}

class Category: Mappable {
    
    var name = ""
    var id = -1
    var image = ""
    var isSelected = false
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        name    <- map["name"]
        id      <- map["id"]
        image   <- map["image"]
    }
}
