//
//  AuthUser.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/2/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias UserDataResult = (_ data: AuthUser?, _ error: NSError?) -> Void


class AuthUser: Mappable {
    
    var id = -1
    var firstName = "Guest"
    var lastName = ""
    var email = ""
    var phone = ""
    var image = ""
    var status = -1
    var notification = -1
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id           <- map["id"]
        firstName    <- map["first_name"]
        lastName     <- map["last_name"]
        email        <- map["email"]
        phone        <- map["phone"]
        image        <- map["image"]
        status       <- map["status"]
        notification <- map["notifications"]
    }
    
    class func getUserData(completion: @escaping UserDataResult) {
        
        APIClient.shared.getUserData { (data, error, status) in
            
            if error == nil {
                
                if let user = Mapper<AuthUser>().map(JSONObject: data) {
                    DataManager.shared.setUser(user: user.toJSONString()!)
                    
                    if let auth = DataManager.shared.getAuthentication() {
                        auth.user = user
                        DataManager.shared.setAuthentication(auth: auth.toJSONString()!)
                        AppUser.authData = auth
                    }
                    
                    completion(user, nil)
                    
                } else {
                    completion(nil, NSError(domain: error?.localizedDescription ?? "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}
