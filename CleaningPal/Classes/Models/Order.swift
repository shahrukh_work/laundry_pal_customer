//
//  Order.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/10/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias OrderResults = (_ data: OrderPlacement?, _ error: NSError?) -> Void

class OrderPlacement: Mappable {
    
    var error = false
    var message = ""
    var errors = [String]()
    var order = Mapper<Order>().map(JSON: [:])!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        order <- map["data"]
        error   <- map["error"]
        message <- map["message"]
        errors  <- map["errors"]
    }
    
    class func placeOrder(params: [String: AnyObject], completion: @escaping OrderResults) {
        
        APIClient.shared.placeOrders(params: params) { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<OrderPlacement>().map(JSONObject: data) {
                    
                    if data.error == true {
                        completion(nil, NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey : data.message]))
                        return
                    }
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
    
    class func placeEasyOrder(params: [String: AnyObject], completion: @escaping OrderResults) {
        
        APIClient.shared.createEasyOrder(params: params) { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<OrderPlacement>().map(JSONObject: data) {
                    
                    if data.error == true {
                        completion(nil, NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey : data.message]))
                        return
                    }
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}

class Order: Mappable {
    
    var orderId = -1
    var totalQuantity = -1
    var totalPrice = -1
    var discountedPrice = -1
    var availableStartTime = ""
    var availableEndTime = ""
    var pickupDateTime = ""
    var pickupAddress = ""
    var walletAmount = 0
    var gst = 0
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        orderId             <- map["order_id"]
        totalQuantity       <- map["total_quantity"]
        totalPrice          <- map["total_price"]
        discountedPrice     <- map["discounted_price"]
        availableStartTime  <- map["available_start_time"]
        availableEndTime    <- map["available_end_time"]
        pickupDateTime      <- map["pickup_date"]
        pickupAddress       <- map["pickup_address"]
        gst                 <- map["gst"]
        walletAmount        <- map["wallet_amount"]
    }
}
