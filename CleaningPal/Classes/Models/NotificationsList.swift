//
//  Notifications.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/17/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias NotificationListResponse = (_ data: NotificationsList?, _ error: NSError?) -> Void

class NotificationsList: Mappable {
    
    var notifications = [NotificationEntity]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        notifications       <- map["notification_list"]
    }
    
    class func getNotificationList(completion: @escaping NotificationListResponse) {
        
        APIClient.shared.getNotificationList { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<NotificationsList>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}


class NotificationEntity: Mappable {
    
    var id = -1
    var message = ""
    var title = ""
    var customerId = -1
    var status = -1
    var createdAt = ""
    var updatedAt = ""
    var promoId = -1
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id          <- map["id"]
        message     <- map["message"]
        title       <- map["title"]
        customerId  <- map["customer_id"]
        status      <- map["status"]
        createdAt   <- map["created_at"]
        updatedAt   <- map["updated_at"]
        promoId     <- map["promo_id"]
    }
    
    
}
