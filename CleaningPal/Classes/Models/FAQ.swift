//
//  FAQ.swift
//  CleaningPal
//
//  Created by Mac on 21/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation


class FAQ {
    var title = ""
    var description = ""
    var isCollapsed = true
        
    init(title: String, isCollapsed: Bool = true, description: String) {
        self.title = title
        self.isCollapsed = isCollapsed
        self.description = description
    }
}
