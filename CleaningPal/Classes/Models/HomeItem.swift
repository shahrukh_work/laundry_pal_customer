//
//  HomeItem.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/6/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias HomeItemsResults = (_ data: HomeItems?, _ error: NSError?) -> Void

class HomeItems: Mappable {
    
    var error = false
    var message = ""
    var data = [HomeItem]()
    var errors = [String]()
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error   <- map["error"]
        message <- map["message"]
        data    <- map["data"]
        errors  <- map["errors"]
    }
    
    class func getHomeItems(query: String, offset: Int, completion: @escaping HomeItemsResults) {
        
        if let _ = DataManager.shared.getUser() {
            
        } else {
            Utility.showLoading()
        }
        
        APIClient.shared.getHomeItemList(offset: offset, query: query) { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<HomeItems>().map(JSONObject: data) {
                    
                    if data.error == true {
                        completion(nil, NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey : data.message]))
                        return
                    }
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
    
    class func searchItems(catId: String, subCatId: String, searchTerm: String, offset: Int, completion: @escaping HomeItemsResults) {
        
        APIClient.shared.searchItems(searchTerm: searchTerm, catId: catId, subCatId: subCatId) { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<HomeItems>().map(JSONObject: data) {
                    
                    if data.error == true {
                        completion(nil, NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey : data.message]))
                        return
                    }
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}


class HomeItem: Mappable {
    
    var name = ""
    private var washingMethodsString = ""
    var washingMethods = [WashingMethod]()
    var id = -1
    var image = ""
    var subCategory = ""
    var selectedServiceIndex = 0
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        name                  <- map["name"]
        washingMethodsString  <- map["item_washing_methods"]
        id                    <- map["id"]
        image                 <- map["image"]
        subCategory           <- map["sub_category_name"]
        
        postMap()
    }
    
    func postMap() {
        
        if let data = Mapper<WashingMethod>().mapArray(JSONString: washingMethodsString) {
            washingMethods = data
        }
        
    }
}
