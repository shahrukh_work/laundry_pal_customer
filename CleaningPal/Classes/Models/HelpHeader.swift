//
//  HelpHeader.swift
//  CleaningPal
//
//  Created by Mac on 21/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation

class HelpHeader {
    var title = ""
    var isCollapsed = true
        
    init(title: String, isCollapsed: Bool = true) {
        self.title = title
        self.isCollapsed = isCollapsed
    }
}
