//
//  Card.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 6/17/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation

class Card {
    var holderName = ""
    var cardNumber = ""
    var expiry = ""
    var cvv = ""
    var type: CardTypes = .visa
    var isSelected = false
    
    init(holderName: String, cardNumber: String, expiry: String, cvv: String, type: CardTypes) {
        self.holderName = holderName
        self.cardNumber = cardNumber
        self.expiry = expiry
        self.cvv = cvv
        self.type = type
    }
}
