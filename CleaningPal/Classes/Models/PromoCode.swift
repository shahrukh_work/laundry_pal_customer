//
//  PromoCode.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/10/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias PromoCodeResults = (_ data: PromoCode?, _ error: NSError?) -> Void

class PromoCode: Mappable {
    
    var discountedPrice = -1
    var discountId = -1
    var isValid = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        discountedPrice <- map["discounted_price"]
        isValid         <- map["is_valid"]
        discountId      <- map["discount_id"]
    }
    
    class func verifyPromoCode(promoCode: String, city: String, completion: @escaping PromoCodeResults) {
        
        APIClient.shared.getPromoCode(promoCode: promoCode, city: city) { (data, error, status) in
            
            if error == nil {
                
                if let data = Mapper<PromoCode>().map(JSONObject: data) {
                    completion(data, nil)
                    
                } else {
                    completion(nil, NSError(domain: "", code: -1, userInfo: nil))
                }
                
            } else {
                completion(nil, error)
            }
        }
    }
}
