//
//  HelpHeaderHelpTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 21/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

protocol HelpHeaderDelegate: AnyObject {
    func sectionTapped (section: Int)
}

class HelpHeaderHelpTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    

    //MARK: - Variable
    weak var delegate: HelpHeaderDelegate?
    var section: Int = -1
    

    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Configure
    func configure (_ data: HelpHeader) {
        titleLabel.text = data.title
        arrowImageView.image = #imageLiteral(resourceName: "forward_arrow")
        arrowImageView.transform = CGAffineTransform.init(rotationAngle: CGFloat(0))
        
        if !data.isCollapsed {
            arrowImageView.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi / 2) )
        }
        
//        if section == 2 {
//
//            if data.isCollapsed {
//                titleLabel.text = "Contact Us"
//
//            } else {
//                titleLabel.text = "Email"
//            }
//        }
    }
    
    
    //MARK: - Actions
    @IBAction func openSectionPressed(_ sender: Any) {
        delegate?.sectionTapped(section: section)
    }
    
    
    //MARK: - Methods
}
