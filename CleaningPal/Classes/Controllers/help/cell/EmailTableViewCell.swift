//
//  EmailTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 21/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class EmailTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var commentTextField: UITextView!
    
    
    //MARK: - Variable
    var delegate: EmailTableViewCellDelegate?
    

    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        emailTextField.text = ""
        commentTextField.text = ""
    }
    
    
    //MARK: - Setup
    func setupView () {
        emailTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        commentTextField.text = "Your message"
        commentTextField.textColor = UIColor.lightGray
        commentTextField.delegate = self
    }
    
    
    //MARK: - Actions
    @IBAction func sendButtonTapped(_ sender: Any) {
        delegate?.sendEmail(email: emailTextField.text!, comments: commentTextField.text)
    }
    
    
    //MARK: - Selectors
    @objc func textFieldDidChange(_ textField: FloatingTextField) {
        textField.floatTitleLabel(titleLabel: emailLabel)
    }
}


//MARK: - UITextViewDelegate
extension EmailTableViewCell: UITextViewDelegate {
        
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Your message"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        UIView.animate(withDuration: 0.3) {
            
            if textView.text == "" {
                self.messageLabel.alpha = 0
                
            } else {
                self.messageLabel.alpha = 1
            }
        }
    }
}
