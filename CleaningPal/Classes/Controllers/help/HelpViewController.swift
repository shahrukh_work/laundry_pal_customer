//
//  HelpViewController.swift
//  CleaningPal
//
//  Created by Mac on 21/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import MessageUI

class HelpViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    
    //MARK: - Variable
    var dataArray: [HelpHeader] = []
    var selectedSection = -1
    
    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var curveView: UIView!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - Setup
    func setupView () {
        tableView.delegate = self
        tableView.dataSource = self
        dataSource()
        
        curveView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        dataArray.append(HelpHeader(title: "FAQ's"))
        dataArray.append(HelpHeader(title: "Chat"))
        dataArray.append(HelpHeader(title: "Contact Us"))
    }
    
    
    //MARK: - Actions
    @objc func handleTap () {
        
        switch selectedSection {
            
        case 0:
            let faqsVC = FAQViewController()
            faqsVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(faqsVC, animated: true)
            
        case 1:
            let chatVC = SupportCenterChatViewController()
            self.navigationController?.pushViewController(chatVC, animated: true)
            
        default:
            print("hello")
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        (self.slideMenuController()?.rightViewController as? SideMenuViewController)?.switchToHome()
    }
    
    
    //MARK: - Methods
    
    
    
    //MARK: - MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
    }
    
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension HelpViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        dataArray.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.register(HelpHeaderHelpTableViewCell.self, indexPath: IndexPath(row: 0, section: section))
        cell.delegate = self
        selectedSection = section
        cell.section = section
        cell.configure(dataArray[section])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 2 && !dataArray[section].isCollapsed {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(EmailTableViewCell.self, indexPath: indexPath)
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
}


//MARK: - HelpHeaderDelegate
extension HelpViewController: HelpHeaderDelegate {
    
    func sectionTapped(section: Int) {
        switch section {
            
        case 0:
            let faqsVC = FAQViewController()
            self.navigationController?.pushViewController(faqsVC, animated: true)
            
        case 1:
            self.navigationController?.pushViewController(SupportCenterChatViewController(), animated: true)
            
        case 2:
            dataArray[section].isCollapsed.toggle()
            
            if dataArray[section].isCollapsed {
                dataArray[section].title = "Contact Us"
                
            } else {
                dataArray[section].title = "Email"
            }
            
            UIView.transition(with: tableView,
                              duration: 0.2,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.tableView.reloadData()
                                
            })
            
        default:
            print("hello")
        }
    }
}


//MARK: - EmailTableViewCellDelegate
extension HelpViewController: EmailTableViewCellDelegate {
    
    func sendEmail(email: String, comments: String) {
        
        if !Utility.isValidEmail(emailStr: email) {
            self.showOkAlert("Invalid Email")
            return
        }
        Utility.showLoading()
        
        PostApi.sendComplaint(email: email, message: comments) { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                self.showOkAlertWithOKCompletionHandler("Our representative will contact you shortly.") { ( _) in
                    self.tableView.reloadData()
                }
                
            } else {
            
            }
        }
    }
}
