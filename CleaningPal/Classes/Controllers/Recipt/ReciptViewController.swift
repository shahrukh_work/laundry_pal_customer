//
//  ReciptViewController.swift
//  CleaningPal
//
//  Created by Mac on 19/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper


class ReciptViewController: UIViewController {


    //MARK: - Variable
    var currentOrder: [OrderTableViewEntry] = []
    var data = Mapper<Order>().map(JSON: [:])!
    var paymentMethod: PaymentType = .cod
    var deliveryType: DeliveryType = .express
    weak var delegate: ReciptViewControllerDelegate?
    
    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomImageView: UIImageView!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var receiptView: UIView!
    

    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    
    //MARK: - Setup
    func setupView () {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        dataSource()
        loadFeatured()
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        
        let timeFormatter = DateFormatter()
        timeFormatter.locale = Locale(identifier: "en_US_POSIX")
        timeFormatter.dateFormat = "h a"
        timeFormatter.timeZone = TimeZone(abbreviation: "GMT")
        
        let dateFormatterMonthDay = DateFormatter()
        dateFormatterMonthDay.locale = Locale(identifier: "en_US_POSIX")
        dateFormatterMonthDay.dateFormat = "MMMM d"
        dateFormatterMonthDay.timeZone = TimeZone(abbreviation: "GMT")
        
        let startTime = dateFormatter.date(from: "1111-11-11T\(data.availableStartTime).000Z")
        let endTime = dateFormatter.date(from: "1111-11-11T\(data.availableEndTime).000Z")
        let monthDay = dateFormatter.date(from: data.pickupDateTime)
        
        let startTimeString = timeFormatter.string(from: startTime ?? Date())
        let endTimeString = timeFormatter.string(from: endTime ?? Date())
        let monthDayString = dateFormatterMonthDay.string(from: monthDay ?? Date())
        
        let dateTimeString = "\(monthDayString ), \(startTimeString ) - \(endTimeString)"
        
        let obj1 = OrderTableViewEntry()
        obj1.item = "Order ID"
        obj1.itemValue = "#\(data.orderId)"
        
        let obj2 = OrderTableViewEntry()
        obj2.item = "No of items"
        obj2.itemValue = "\(data.totalQuantity)"
        
        let obj3 = OrderTableViewEntry()
        obj3.item = "Payable Price"
        obj3.itemValue = "\(data.totalPrice)"
        obj3.isPrice = true
        
        let objWallet = OrderTableViewEntry()
        objWallet.item = "Wallet Amount"
        objWallet.itemValue = "\(data.walletAmount < 0 ? "+": "-")\(data.walletAmount)"
        objWallet.isPrice = true
        
        let obj4 = OrderTableViewEntry()
        obj4.item = "GST"
        obj4.itemValue = "\(data.gst)"
        obj4.isPrice = true
        
        let obj5 = OrderTableViewEntry()
        obj5.item = "You Saved"
        obj5.itemValue = "\(data.discountedPrice)"
        obj5.isPrice = true
        
        let obj8 = OrderTableViewEntry()
        obj8.item = "Pay with"
        obj8.itemValue = paymentMethod.rawValue
        
        let obj6 = OrderTableViewEntry()
        obj6.item = "Date & Time"
        obj6.itemValue = dateTimeString
        
        let objDeliveryType = OrderTableViewEntry()
        objDeliveryType.item = "Delivery Type"
        objDeliveryType.itemValue = deliveryType.rawValue
        
        let obj7 = OrderTableViewEntry()
        obj7.item = "Address"
        obj7.itemValue = data.pickupAddress
        
        currentOrder.append(obj1)
        currentOrder.append(obj2)
        currentOrder.append(obj3)
        
        if data.walletAmount != 0 {
            currentOrder.append(objWallet)
        }
        currentOrder.append(obj4)
        currentOrder.append(obj5)
        currentOrder.append(obj8)
        currentOrder.append(obj6)
        currentOrder.append(objDeliveryType)
        currentOrder.append(obj7)
        tableView.reloadData()
        tableViewHeightConstraint.constant =  self.tableView.contentSize.height
        self.view.layoutIfNeeded()
    }
    
    //MARK: - Selectors
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        
        if let error = error {
            showOkAlert(error.localizedDescription)
            
        } else {
            showOkAlert("Receipt saved to photo library.")
        }
    }
    
    
    //MARK: - Actions
    @IBAction func sharePressed(_ sender: Any) {
        crossButton.isHidden = true
        buttonStackView.isHidden = true
        let image = receiptView.asImage()
        crossButton.isHidden = false
        buttonStackView.isHidden = false
        
        let imageToShare = [image]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func downloadPressed(_ sender: Any) {
        crossButton.isHidden = true
        buttonStackView.isHidden = true
        let image = receiptView.asImage()
        crossButton.isHidden = false
        buttonStackView.isHidden = false
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @IBAction func crossPressed(_ sender: Any) {
        delegate?.crossPressed()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - Private Methods
    private func loadFeatured() {
        Utility.showLoading()
        
        BottomFeatured.getFeatured { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    
                    if !data.featured.isEmpty {
                        
                        let random = Int(arc4random_uniform(UInt32(data.featured.count)))
                        let feature = data.featured[random]
                        
                        if let url = URL(string:  APIRoutes.baseUrl + APIRoutes.featureImageBase + (feature.image)) {
                            
                            self.bottomImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ad_icon"))
                        }
                    }
                }
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension ReciptViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(CurrentOrderTableViewCell.self, indexPath: indexPath)
        cell.selectionStyle = .none
        cell.configure(currentOrder: currentOrder[indexPath.row])
        return cell
    }
}
