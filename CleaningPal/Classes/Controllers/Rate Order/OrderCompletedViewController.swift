//
//  OrderCompletedViewController.swift
//  CleaningPal
//
//  Created by Mac on 20/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import Cosmos

class OrderCompletedViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var remarksLabel: UILabel!
    

    //MARK: - Variable
    var performanceString = ""
    var skipRating = false
    var orderStatus: OrderStatusData?


    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView ()
    }
    
    
    //MARK: - Setup
    func setupView () {
        commentTextView.text = "Your comment about our rider"
        commentTextView.textColor = UIColor.lightGray
        commentTextView.delegate = self
        ratingView.didTouchCosmos = {[weak self] rate in
            
            if self?.ratingView.rating ?? 0.0 == 2 {
                self?.performanceString = "Satisfactory"
                
            } else if self?.ratingView.rating ?? 0.0 == 3 {
                self?.performanceString = "Good"
                
            } else if self?.ratingView.rating ?? 0.0 == 4 {
                self?.performanceString = "Very Good"
                
            } else if self?.ratingView.rating ?? 0.0 == 5 {
                self?.performanceString = "Excellent"
                
            } else {
                self?.performanceString = "Poor"
            }
            self?.remarksLabel.text = self?.performanceString
        }
        
    }
    
    //MARK: - Actions
    @IBAction func addPressed(_ sender: Any) {
        rateOrder(performance: performanceString, star: "\(ratingView.rating)", comments: commentTextView.text)
        
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        rateOrder()
    }
    
    //MARK: - Methods
    private func rateOrder (performance: String = "", star: String = "", comments: String = "") {
        
        APIClient.shared.rateOrder(orderId: "\(orderStatus?.data?.id ?? -1)", performance: performance, star: star, comments: comments, isSkip: true) { (result, error, status) in
            
            if error  == nil {
                self.dismiss(animated: true, completion: nil)
                
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
}


//MARK: - UITextViewDelegate
extension OrderCompletedViewController: UITextViewDelegate {
        
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Your comment about our rider"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        UIView.animate(withDuration: 0.3) {
            
            if textView.text == "" {
                self.commentLabel.alpha = 0
                
            } else {
                self.commentLabel.alpha = 1
            }
        }
    }
}
