//
//  RateDriverViewController.swift
//  CleaningPal
//
//  Created by Mac on 20/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import Cosmos

class RateDriverViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var commentTextView: UITextView!

    
    //MARK: - Variable


    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    //MARK: - Setup
    func setupView () {
        commentTextView.text = "Your comment about our rider"
        commentTextView.textColor = UIColor.lightGray
        commentTextView.delegate = self
    }
    
    //MARK: - Actions
    
    
    //MARK: - Methods
}


//MARK: - UITextViewDelegate
extension RateDriverViewController: UITextViewDelegate {
        
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Your comment about our rider"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        UIView.animate(withDuration: 0.3) {
            
            if textView.text == "" {
                self.commentLabel.alpha = 0
                
            } else {
                self.commentLabel.alpha = 1
            }
        }
    }
}
