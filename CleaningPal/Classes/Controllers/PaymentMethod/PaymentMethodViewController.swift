//
//  PaymentMethodViewController.swift
//  CleaningPal
//
//  Created by Mac on 13/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class PaymentMethodViewController: UIViewController {
    
    
    //MARK: - Variable
    var paymentMethod: PaymentType = .card
    var month = ["01", "02", "03", "04", "05", "06", "07", "08", "09" , "10", "11", "12"]
    var year = [String]()
    var picker = UIPickerView()
    var selectedMonthIndex = 0
    var selectedYearIndex = 0
    weak var delegate: PaymentMethodViewControllerDelegate?
    
    
    //MARK: - Outlet
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cardNumLabel: UILabel!
    @IBOutlet weak var cvcLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var codButton: UIButton!
    @IBOutlet weak var cardButton: UIButton!
    @IBOutlet weak var nameTextField: FloatingTextField!
    @IBOutlet weak var cardNoTextField: FloatingTextField!
    @IBOutlet weak var expiryTextField: FloatingTextField!
    @IBOutlet weak var CVCTextField: FloatingTextField!
    @IBOutlet weak var payAdvanceButton: UIButton!
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var payLabel: UILabel!
    @IBOutlet weak var fieldsStackView: UIStackView!
    @IBOutlet weak var cardsImageView: UIImageView!
    @IBOutlet weak var creditButton: UIButton!
    @IBOutlet weak var cardImage: UIImageView!
    
    

    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - View Setup
    private func setupView() {
        
        if paymentMethod == .none {
            paymentMethod = .cod
        }
        selectPaymentMethod()
        
        nameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        cardNoTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        expiryTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        CVCTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
        CVCTextField.delegate = self
        cardNoTextField.delegate = self
        expiryTextField.tintColor = .clear
        expiryTextField.inputView = picker
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        let yearNow = Int(formatter.string(from: Date())) ?? 0
        
        for i in yearNow...(yearNow + 30) {
            year.append("\(i)")
        }
        
        picker.delegate = self
        picker.dataSource = self
        picker.reloadAllComponents()
        getCardPaymentStatus()
    }
    
    
    //MARK: - Actions
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addPressed(_ sender: Any) {
        
        if paymentMethod == .card {
            let check = [nameTextField.text ?? "", cardNoTextField.text ?? "", CVCTextField.text ?? "", expiryTextField.text ?? ""]
            
            if check.contains("") {
                self.showOkAlert("Fill All the fields")
                return
            }
            
            if (cardNoTextField.text ?? "").count < 19 {
                self.showOkAlert("Invalid Card Number")
                return
            }
            
            if (CVCTextField.text ?? "").count < 3 {
                self.showOkAlert("Invalid CVC")
                return
            }
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func codPressed(_ sender: Any) {
        paymentMethod = .cod
        delegate?.paymentMethodSelected(method: paymentMethod)
        self.selectPaymentMethod()
    }
    
    @IBAction func creditCardPressed(_ sender: Any) {
        paymentMethod = .card
        delegate?.paymentMethodSelected(method: paymentMethod)
        self.selectPaymentMethod()
    }
    
    @IBAction func cardNumberChanged(_ sender: Any) {
        cardNoTextField.text = modifyCreditCardString(creditCardString: cardNoTextField.text ?? "")
    }
    
    
    //MARK: - Selectors
    @objc func textFieldDidChange(_ textField: FloatingTextField) {
        
        if textField == nameTextField {
            textField.floatTitleLabel(titleLabel: nameLabel)
            
        } else if textField == cardNoTextField {
            textField.floatTitleLabel(titleLabel: cardNumLabel)
            
        } else if textField == expiryTextField {
            textField.floatTitleLabel(titleLabel: dateLabel)
            
        } else if textField == CVCTextField {
            textField.floatTitleLabel(titleLabel: cvcLabel)
        }
    }
    
    
    //MARK: - Private Methods
    private func selectPaymentMethod () {
        
        if paymentMethod == .cod {
            codButton.setImage(#imageLiteral(resourceName: "circle_filled"), for: .normal)
            cardButton.setImage(#imageLiteral(resourceName: "circle_unfilled"), for: .normal)
            return
        }
        codButton.setImage(#imageLiteral(resourceName: "circle_unfilled"), for: .normal)
        cardButton.setImage(#imageLiteral(resourceName: "circle_filled"), for: .normal)
    }
    
    private func modifyCreditCardString(creditCardString : String) -> String {
        let trimmedString = creditCardString.components(separatedBy: .whitespaces).joined()
        
        let arrOfCharacters = Array(trimmedString)
        var modifiedCreditCardString = ""
        
        if(arrOfCharacters.count > 0) {
            for i in 0...arrOfCharacters.count-1 {
                modifiedCreditCardString.append(arrOfCharacters[i])
                if((i+1) % 4 == 0 && i+1 != arrOfCharacters.count){
                    modifiedCreditCardString.append(" ")
                }
            }
        }
        return modifiedCreditCardString
    }
    
    private func getCardPaymentStatus() {
        Utility.showLoading()
        
        CardPaymentStatus.getCardStatus { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data?.cardStatus.first {
                    
                    self.payAdvanceButton.isHidden = data.status == 0
                    self.buttonStackView.isHidden = data.status == 0
                    self.payLabel.isHidden = data.status == 0
                    self.fieldsStackView.isHidden = data.status == 0
                    self.cardsImageView.isHidden = data.status == 0
                    self.creditButton.isHidden = data.status == 0
                    self.cardImage.isHidden = data.status == 0
                }
            }
        }
    }
}


//MARK: - UITextFieldDelegate
extension PaymentMethodViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case CVCTextField:
            let bool = maxLengthForTextfield(textfield: textField, range: range, string: string, length: 3)
            return bool
        case cardNoTextField:
            let bool = maxLengthForTextfield(textfield: textField, range: range, string: string, length: 19)
            return bool
        default:
            break
        }
        return true
    }
    
    func maxLengthForTextfield(textfield: UITextField, range: NSRange, string: String, length: Int) -> Bool {
        let currentText = textfield.text ?? ""
        guard let strRange = Range(range, in: currentText) else { return false }
        let newString = currentText.replacingCharacters(in: strRange, with: string)
        
        return newString.count <= length
    }
    
}


//MARK: - PickerView Delegate & DataSource
extension PaymentMethodViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return month.count
        }
        return year.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return month[row]
        }
        return year[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if component == 0 {
            selectedMonthIndex = row
            
        } else {
            selectedYearIndex = row
        }
        
        expiryTextField.text = "\(month[selectedMonthIndex])/\(year[selectedYearIndex])"
    }
    
}
