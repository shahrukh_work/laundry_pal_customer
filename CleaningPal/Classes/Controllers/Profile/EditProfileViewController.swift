//
//  EditProfileViewController.swift
//  CleaningPal
//
//  Created by Mac on 20/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import SDWebImage


class EditProfileViewController: UIViewController {
    
    
    //MARK: - Outlet
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var firstNameTextField: FloatingTextField!
    @IBOutlet weak var lastNameTextField: FloatingTextField!
    @IBOutlet weak var emailTextField: FloatingTextField!
    @IBOutlet weak var phoneTextField: FloatingTextField!
    @IBOutlet weak var containerView: UIView!
    
    
    //MARK: - Variable
    var imagePicker = UIImagePickerController()
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView ()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    //MARK: - Setup
    func setupView () {
        firstNameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        lastNameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        emailTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        phoneTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        Utility.cornerRadiusPostioned(corners: [.layerMinXMinYCorner,.layerMaxXMinYCorner], view: containerView, cornerRadius: 20)
        
        let user = DataManager.shared.getUser()
        firstNameTextField.text = user?.firstName
        lastNameTextField.text = user?.lastName
        emailTextField.text = user?.email
        phoneTextField.text = user?.phone
        nameLabel.text = "\(user?.firstName ?? "") \(user?.lastName ?? "")"
        
        if let url = URL(string: APIRoutes.baseUrl + APIRoutes.profileBase + (user?.image ?? "")) {
            profileImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholderUser"))
        }
        
        firstNameTextField.floatTitleLabel(titleLabel: firstNameLabel)
        lastNameTextField.floatTitleLabel(titleLabel: lastNameLabel)
        emailTextField.floatTitleLabel(titleLabel: emailLabel)
        phoneTextField.floatTitleLabel(titleLabel: phoneLabel)
    }
    
    //MARK: - Actions
    @IBAction func photoPickerPressed(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func saveChangePressed(_ sender: Any) {
        
        let check = [firstNameTextField.text ?? "", lastNameTextField.text ?? "", emailTextField.text ?? "", phoneTextField.text ?? ""]
        
        if check.contains("") {
            self.showOkAlert("Fill all the fields")
            return
        }
        
        if !Utility.isValidEmail(emailStr: emailTextField.text ?? "") {
            self.showOkAlert("Invalid Email")
            return
        }
        
        Utility.showLoading()
        PostApi.updateProfile(firstName: check[0], lastName: check[1], phone: check[3], email: check[2], image: profileImageView.image ?? UIImage()) { (data, error) in
            
            Utility.hideLoading()
            
            if error == nil {
                Utility.showLoading()
                
                AuthUser.getUserData { (data, error) in
                    Utility.hideLoading()
                    
                    if error == nil {
                        
                        self.showOkAlertWithOKCompletionHandler("Profile Updated") { ( _) in
                            
                            (self.slideMenuController()?.rightViewController as? SideMenuViewController)?.switchToHome()
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    } else {
                        Utility.showAlertController(self.navigationController!, "Profile could not be updated.")
                    }
                }
                
            } else {
                Utility.showAlertController(self.navigationController!, "Profile could not be updated.")
            }
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        (self.slideMenuController()?.rightViewController as? SideMenuViewController)?.switchToHome()
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Selectors
    @objc func textFieldDidChange(_ textField: FloatingTextField) {
        
        if textField == firstNameTextField {
            textField.floatTitleLabel(titleLabel: firstNameLabel)
            
        } else if textField == lastNameTextField {
            textField.floatTitleLabel(titleLabel: lastNameLabel)
            
        } else if textField == emailTextField {
            textField.floatTitleLabel(titleLabel: emailLabel)
            
        } else if textField == phoneTextField {
            textField.floatTitleLabel(titleLabel: phoneLabel)
        }
    }
}


//MARK: - UIImagePickerDelegate
extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profileImageView.contentMode = .scaleAspectFill
            profileImageView.image = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}


//MARK: UITextFieldDelegate
extension EditProfileViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let float = true
        
        switch textField {
            
        case firstNameTextField:
            firstNameTextField.floatTitle(float: float, titleLabel: firstNameLabel)
            break
            
        case lastNameTextField:
            lastNameTextField.floatTitle(float: float, titleLabel: lastNameLabel)
            break
            
        case emailTextField:
            emailTextField.floatTitle(float: float, titleLabel: emailLabel)
            break
            
        default: //phoneTextField
            phoneTextField.floatTitle(float: float, titleLabel: phoneLabel)
            break
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let float = false
        
        if textField.text == "" {
            
            switch textField {
                
            case firstNameTextField:
                firstNameTextField.floatTitle(float: float, titleLabel: firstNameLabel)
                break
                
            case lastNameTextField:
                lastNameTextField.floatTitle(float: float, titleLabel: lastNameLabel)
                break
                
            case emailTextField:
                emailTextField.floatTitle(float: float, titleLabel: emailLabel)
                break
                
            default: //phoneTextField
                phoneTextField.floatTitle(float: float, titleLabel: phoneLabel)
                break
            }
        }
    }
}
