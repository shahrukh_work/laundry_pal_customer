//
//  PromotionTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 12/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class PromotionTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var itemImageView: UIImageView!


    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Configure
    func configure (featured: Features) {
        if let url = URL(string:  APIRoutes.baseUrl + APIRoutes.featureImageBase + (featured.image)) {
            self.itemImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ad_icon"))
        }
    }
}
