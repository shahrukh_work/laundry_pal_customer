//
//  PromotionListViewController.swift
//  CleaningPal
//
//  Created by Mac on 12/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class PromotionListViewController: UIViewController {

    
    //MARK: - Variable
    var currentPage = 0
    var isLoadingData = true
    var featured = [Features]()
    
    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerView: UIView!


    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView ()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.layoutIfNeeded()
    }
    
    
    //MARK: - Setup
    func setupView () {
        self.navigationController?.navigationBar.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        loadData()
    }
    
    
    //MARK: - Private Methods
    func loadData() {
        
        if isLoadingData {
            
            Utility.showLoading()
            Featured.getFeatured(offset: currentPage, limit: 10, featuredType: 1) { (data, error) in
                Utility.hideLoading()
                
                if error == nil {
                    
                    if let data = data {
                        self.featured.append(contentsOf: data.featured)
                        self.tableView.reloadData()
                    }
                } else {
                    Utility.showAlertController(self, error?.localizedDescription ?? "")
                }
                
                self.isLoadingData = false
            }
        }
    }
    
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension PromotionListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return featured.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(PromotionTableViewCell.self, indexPath: indexPath)
        cell.configure(featured: featured[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let promotionDetail = PromotionDetailViewController()
        promotionDetail.featured = featured[indexPath.row]
        self.navigationController?.pushViewController(promotionDetail, animated: true)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == tableView {
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                
                
                if !isLoadingData {
                    currentPage+=1
                    isLoadingData = true
                    loadData()
                }
                
            }
        }
    }
}
