//
//  PromotionDetailViewController.swift
//  CleaningPal
//
//  Created by Mac on 12/06/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper


class PromotionDetailViewController: UIViewController {

    
    //MARK: - Variable
    var featured = Mapper<Features>().map(JSON: [:])!
    
    
    //MARK: - Outlet
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var termsAndConditonLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!


    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - View Setup
    func setupView () {
        
        containerView.cornerRadius = 16
        scrollView.cornerRadius = 16
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        scrollView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        termsAndConditonLabel.text = featured.description
        
        if let url = URL(string: APIRoutes.baseUrl + APIRoutes.featureImageBase + (featured.image)) {
            self.itemImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ad_icon"))
        }
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        
    }
    
    //MARK: - Actions
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Methods
}


