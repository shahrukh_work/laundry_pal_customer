//
//  CartItemTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 13/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper


class CartItemTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var qauntityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var processLabel: UILabel!
    

    //MARK: - Variable
    var qauntity = 1
    var data = Mapper<CartItem>().map(JSON: [:])!
    weak var delegate: CartItemTableViewCellDelegate?

    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Actions
    @IBAction func minusPressed(_ sender: Any) {
        
        if data.quantity > 0 {
            data.quantity -= 1
            self.qauntityLabel.text = "\(data.quantity)"
        }
        
        let cart = DataManager.shared.getCart()
        cart.updateItemCount(cartItem: data)
        DataManager.shared.setCart(cart: cart.toJSONString()!)
        delegate?.cartItemQuantityChanged()
        
    }
    
    @IBAction func plusPressed(_ sender: Any) {
        data.quantity += 1
        self.qauntityLabel.text = "\(data.quantity)"
        let cart = DataManager.shared.getCart()
        cart.updateItemCount(cartItem: data)
        DataManager.shared.setCart(cart: cart.toJSONString()!)
        delegate?.cartItemQuantityChanged()
    }
    
    
    //MARK: - Public Methods
    func configure(data: CartItem) {
        self.data = data
        self.qauntityLabel.text = "\(data.quantity)"
        self.titleLabel.text = data.name
        self.typeLabel.text = data.subCat == "" ? "____" : data.subCat
        self.processLabel.text = data.washingMethod
        priceLabel.text = "\(data.price * data.quantity)"
    }
}
