//
//  CartItemsViewController.swift
//  CleaningPal
//
//  Created by Mac on 13/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper


class CartItemsViewController: UIViewController {
    
    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var itemPriceLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var noItemsView: UIView!
    
    
    //MARK: - Variable
    var cartItems = Mapper<Cart>().map(JSON: [:])!
    var totalItems = 0
    var totalPrice = 0
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        cartItems = DataManager.shared.getCart()
        tableView.reloadData()
        calculateTotal()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    
    //MARK: - Setup
    func setupView () {
        tableView.delegate = self
        tableView.dataSource = self
        containerView.clipsToBounds = true
    }
    
    
    //MARK: - Actions
    @IBAction func addMorePressed(_ sender: Any) {
        self.tabBarController?.selectedIndex = 0
    }
    
    @IBAction func confirmPressed(_ sender: Any) {
        
        guard let _ = DataManager.shared.getUser() else {
            Utility.showLoginOptions(tabController: self.tabBarController)
            return
        }
        
        if cartItems.cartItems.isEmpty {
            self.showOkAlert("Cart is Empty, Please add items to continue.")
            return
        }
        let selecAddress = SelectAddressViewController()
        selecAddress.calBack = {
            print("cal")
            self.tabBarController?.selectedIndex = 0
        }
        self.navigationController?.pushViewController(selecAddress, animated: true)
    }
    
    
    @objc func popToHome (notification: NSNotification) {
        print("tabbar is working")
       
    }
    
    //MARK: - Private Methods
    private func calculateTotal() {
        
        totalItems = 0
        totalPrice = 0
        
        cartItems.cartItems.forEach { (item) in
            totalItems = totalItems + item.quantity
            totalPrice = totalPrice + (item.quantity * item.price)
        }
        
        totalPriceLabel.text = "\(totalPrice)"
        itemPriceLabel.text = "\(totalItems)"
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension CartItemsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        noItemsView.isHidden = !cartItems.cartItems.isEmpty
        return cartItems.cartItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(CartItemTableViewCell.self, indexPath: indexPath)
        cell.configure(data: cartItems.cartItems[indexPath.row])
        cell.delegate = self
        return cell
    }
}


//MARK: - CartItemTableViewCellDelegate
extension CartItemsViewController: CartItemTableViewCellDelegate {
    
    func cartItemQuantityChanged() {
        cartItems = DataManager.shared.getCart()
        tableView.reloadData()
        AppUser.shared.cartItemsCount = cartItems.cartItems.count
        tabBarController?.tabBar.items?[2].badgeValue = "\(AppUser.shared.cartItemsCount)"
        calculateTotal()
    }
}
