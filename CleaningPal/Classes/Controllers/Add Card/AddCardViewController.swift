//
//  AddCardViewController.swift
//  CleaningPal
//
//  Created by Mac on 18/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class AddCardViewController: UIViewController {
    
    
    //MARK: - Outlet
    @IBOutlet weak var selectCardTextField: UITextField!
    @IBOutlet weak var nameTextField: FloatingTextField!
    @IBOutlet weak var cvvTextField: FloatingTextField!
    @IBOutlet weak var expiryTextField: FloatingTextField!
    @IBOutlet weak var cardNoTextField: FloatingTextField!
    @IBOutlet weak var selectedCardImageView: UIImageView!
    @IBOutlet weak var curvedView: UIView!
    @IBOutlet weak var nameOnCardLabel: UILabel!
    @IBOutlet weak var cardOnNumberLabel: UILabel!
    @IBOutlet weak var expiryDateLabel: UILabel!
    @IBOutlet weak var cvvDateLabel: UILabel!
    
    
    //MARK: - Variable
    var data: [CardTypes] = [.visa, .masterCard]
    var datePicker = UIPickerView()
    var month = ["01", "02", "03", "04", "05", "06", "07", "08", "09" , "10", "11", "12"]
    var year = [String]()
    var picker = UIPickerView()
    var selectedCardIndex = 0
    var selectedMonthIndex = 0
    var selectedYearIndex = 0
    var cardNumberCursorPreviousPosition = 0
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - Setup
    func setupView () {
        Utility.cornerRadiusPostioned(corners: [.layerMinXMinYCorner,.layerMaxXMinYCorner], view: curvedView, cornerRadius: 16)
        createPicker()
        doneButtonSelectors()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        let yearNow = Int(formatter.string(from: Date())) ?? 0
        
        for i in yearNow...(yearNow + 30) {
            year.append("\(i)")
        }
        
        datePicker.delegate = self
        datePicker.dataSource = self
        datePicker.reloadAllComponents()
        expiryTextField.inputView = datePicker
        cardNoTextField.delegate = self
        cvvTextField.delegate = self
        selectCardTextField.tintColor = .clear
        
        nameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        cvvTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        expiryTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        cardNoTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }
    
    
    //MARK: - Selectors
    @objc func cardSelectDoneTapped () {
        
        if data[selectedCardIndex] == .visa {
            selectedCardImageView.image = #imageLiteral(resourceName: "visa")
            
        } else {
            selectedCardImageView.image = #imageLiteral(resourceName: "mastercard")
        }
        selectCardTextField.text = data[selectedCardIndex].rawValue
        self.view.endEditing(true)
    }
    
    @objc func textFieldDidChange(_ textField: FloatingTextField) {
        
        if textField == nameTextField {
            textField.floatTitleLabel(titleLabel: nameOnCardLabel)
            
        } else if textField == cvvTextField {
            textField.floatTitleLabel(titleLabel: cvvDateLabel)
            
        } else if textField == expiryTextField {
            textField.floatTitleLabel(titleLabel: expiryDateLabel)
            
        } else if textField == cardNoTextField {
            textField.floatTitleLabel(titleLabel: cardOnNumberLabel)
        }
    }
    
    
    //MARK: - Actions
    @IBAction func submitPressed(_ sender: Any) {
        
        let check = [nameTextField.text ?? "", cardNoTextField.text ?? "", cvvTextField.text ?? "", expiryTextField.text ?? ""]
        
        if check.contains("") {
            self.showOkAlert("Fill All the fields")
            return
        }
        
        if (cardNoTextField.text ?? "").count < 19 {
            self.showOkAlert("Invalid Card Number")
            return
        }
        
        if (cvvTextField.text ?? "").count < 3 {
            self.showOkAlert("Invalid CVC")
            return
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cardNumberChanged(_ sender: Any) {
        cardNoTextField.text = modifyCreditCardString(creditCardString: cardNoTextField.text ?? "")
    }
    
    
    //MARK: - Private Methods
    private func modifyCreditCardString(creditCardString : String) -> String {
        let trimmedString = creditCardString.components(separatedBy: .whitespaces).joined()
        
        let arrOfCharacters = Array(trimmedString)
        var modifiedCreditCardString = ""
        
        if(arrOfCharacters.count > 0) {
            for i in 0...arrOfCharacters.count-1 {
                modifiedCreditCardString.append(arrOfCharacters[i])
                if((i+1) % 4 == 0 && i+1 != arrOfCharacters.count){
                    modifiedCreditCardString.append(" ")
                }
            }
        }
        return modifiedCreditCardString
    }
    
    private func createPicker () {
        picker = UIPickerView(frame: CGRect(x: 0, y: 200, width: view.frame.width, height: 300))
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        selectCardTextField.inputView = picker
        selectCardTextField.text = data[selectedCardIndex].rawValue
        selectedCardImageView.image = #imageLiteral(resourceName: "visa")
    }
    
    private func doneButtonSelectors () {
        self.selectCardTextField.addDoneOnKeyboardWithTarget(self, action: #selector(cardSelectDoneTapped))
    }
}


//MARK: - UIPickerViewDelegate & UIPickerViewDataSource
extension AddCardViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        if pickerView == datePicker {
            return 2
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        if pickerView == datePicker && component == 0 {
            return month.count
            
        } else if pickerView == datePicker && component == 1 {
            return year.count
        }
        return data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == datePicker && component == 0 {
            return month[row]
            
        } else if pickerView == datePicker && component == 1 {
            return year[row]
        }
        return data[row].rawValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == datePicker && component == 0 {
            selectedMonthIndex = row
            
        } else if pickerView == datePicker && component == 1 {
            selectedYearIndex = row
        }
        
        if pickerView == datePicker {
            expiryTextField.text = "\(month[selectedMonthIndex])/\(year[selectedYearIndex])"
            expiryTextField.floatTitleLabel(titleLabel: expiryDateLabel)
            return
        }
        selectedCardIndex = row
    }
}


//MARK: - UITextFieldDelegate
extension AddCardViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
            
        case cvvTextField:
            let bool = maxLengthForTextfield(textfield: textField, range: range, string: string, length: 3)
            return bool
            
        case cardNoTextField:
            let bool = maxLengthForTextfield(textfield: textField, range: range, string: string, length: 19)
            return bool
            
        default:
            break
        }
        return true
    }
    
    func maxLengthForTextfield(textfield: UITextField, range: NSRange, string: String, length: Int) -> Bool {
        let currentText = textfield.text ?? ""
        guard let strRange = Range(range, in: currentText) else { return false }
        let newString = currentText.replacingCharacters(in: strRange, with: string)
        
        return newString.count <= length
    }
}
