//
//  SideMenuTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 19/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemTitleLabel: UILabel!
    

    //MARK: - Variable


    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - configure
    func configure (title: String, image: UIImage) {
        itemTitleLabel.text = title
        itemImageView.image = image
    }
    
    //MARK: - Actions
    
    
    //MARK: - Methods
}
