//
//  SideMenuViewController.swift
//  CleaningPal
//
//  Created by Mac on 19/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var blueView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratedLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerView: UIView!
    
    
    //MARK: - Variable
    var itemTitles = ["My Orders", "My Wallet", "Invite Friends & Family",/*"Price List",*/"Help","Settings","Logout"]
    var itemImages: [UIImage] = [#imageLiteral(resourceName: "myorders"), #imageLiteral(resourceName: "wallet"), #imageLiteral(resourceName: "invite"), /*#imageLiteral(resourceName: "pricelist"),*/ #imageLiteral(resourceName: "help"), #imageLiteral(resourceName: "settings"), #imageLiteral(resourceName: "logout")]
    var homeController: UINavigationController!

    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView ()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Utility.cornerRadiusPostioned(corners: [.layerMinXMinYCorner], view: blueView, cornerRadius: 20)
        Utility.cornerRadiusPostioned(corners: [.layerMinXMinYCorner], view: containerView, cornerRadius: 20)
        let user = DataManager.shared.getUser()
        
        nameLabel.text = "\(user?.firstName ?? "Guest") \(user?.lastName ?? "")"
        numberLabel.text = "\(user?.phone ?? "")"
        
        if let url = URL(string: APIRoutes.baseUrl + APIRoutes.profileBase + (user?.image ?? "")) {
            profileImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholderUser"))
        }
        
        guard let _ = DataManager.shared.getUser() else {
            itemTitles.removeLast()
            itemTitles.append("Login")
            tableView.reloadData()
            return
        }

    }
    
    //MARK: - Setup
    func setupView () {
        tableView.delegate = self
        tableView.dataSource = self
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    //MARK: - dataSource
    func dataSource () {
        
    }
    
    
    //MARK: - Actions
    @IBAction func editPressed(_ sender: Any) {
        guard let _ = DataManager.shared.getUser() else {
            Utility.showLoginOptions(tabController: self.tabBarController)
            return
        }
        self.slideMenuController()?.changeMainViewController(EditProfileViewController(), close: true)
        self.closeRight()
    }
    
    
    //MARK: - Public Methods
    func switchToHome() {
        self.slideMenuController()?.changeMainViewController(homeController, close: true)
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        itemTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(SideMenuTableViewCell.self, indexPath: indexPath)
        cell.selectionStyle = .none
        cell.configure(title: itemTitles[indexPath.row], image: itemImages[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
            
        case 0:
            
            guard let _ = DataManager.shared.getUser() else {
                Utility.showLoginOptions(tabController: self.tabBarController)
                return
            }
            let currentOrderVC = UINavigationController(rootViewController: CurrentOrderViewController())
            currentOrderVC.isNavigationBarHidden = true
            self.slideMenuController()?.changeMainViewController(currentOrderVC, close: true)
            
        case 1:
            guard let _ = DataManager.shared.getUser() else {
                Utility.showLoginOptions(tabController: self.tabBarController)
                return
            }
            let walletVC = UINavigationController(rootViewController: WalletViewController())
            walletVC.isNavigationBarHidden = true
            self.slideMenuController()?.changeMainViewController(walletVC, close: true)
            
        case 2:
            guard let _ = DataManager.shared.getUser() else {
                Utility.showLoginOptions(tabController: self.tabBarController)
                return
            }
            let socialVC = SocialMediaViewController()
            socialVC.modalPresentationStyle = .overCurrentContext
            tabBarController?.present(socialVC, animated: true, completion: nil)
            
        case 3:
            guard let _ = DataManager.shared.getUser() else {
                Utility.showLoginOptions(tabController: self.tabBarController)
                return
            }
            let helpVC = UINavigationController(rootViewController: HelpViewController())
            helpVC.isNavigationBarHidden = true
            helpVC.hidesBottomBarWhenPushed = true
            self.slideMenuController()?.changeMainViewController(helpVC, close: true)
            
        case 4:
            guard let _ = DataManager.shared.getUser() else {
                Utility.showLoginOptions(tabController: self.tabBarController)
                return
            }
            let settingsVC = UINavigationController(rootViewController: SettingsViewController())
            settingsVC.isNavigationBarHidden = true
            self.slideMenuController()?.changeMainViewController(settingsVC, close: true)
            
        case 5:
            
            if let _ = DataManager.shared.getUser() {
                
                Utility.showLoading()
                
                PostApi.customerLogout { (data, error) in
                    
                    if error == nil {
                        Utility.hideLoading()
                        Utility.loginRootViewController()
                        DataManager.shared.deleteUser()
                        DataManager.shared.deleteSkipData()
                        DataManager.shared.deleteAuthentication()
                        print("logout")
                        
                    } else {
                        Utility.showAlertController(self, "Some thing went wrong. You can not logout right now.")
                    }
                }
                
            } else {
                Utility.loginRootViewController()
            }
            
        default:
            print("noting")
        }
    }
}
