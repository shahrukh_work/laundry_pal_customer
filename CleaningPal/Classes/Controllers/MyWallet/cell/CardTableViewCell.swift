//
//  CardTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 19/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class CardTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var cardLabel: UILabel!
    @IBOutlet weak var cardSelectedButton: UIButton!
    
    
    //MARK: - Variable


    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - configure
    func configure (data: Card) {
        
        if data.type == .visa {
            cardImageView.image = #imageLiteral(resourceName: "visa")
            
        } else {
            cardImageView.image = #imageLiteral(resourceName: "mastercard")
        }
        
        if !data.isSelected {
            cardSelectedButton.setImage(#imageLiteral(resourceName: "circle_unfilled"), for: .normal)
            
        } else {
            cardSelectedButton.setImage(#imageLiteral(resourceName: "check_process"), for: .normal)
        }
        cardLabel.text = data.cardNumber
    }
    
    //MARK: - Actions
    
    
    //MARK: - Methods
}
