//
//  WalletViewController.swift
//  CleaningPal
//
//  Created by Mac on 19/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController {
  
    
    //MARK: - Variable
    var cards = [Card]()
    
    
    //MARK: - Outlet
    @IBOutlet weak var availableBalanceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var savedBalanceLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var buttonsStackView: UIStackView!
    

    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - View Setup
    func setupView() {
        tableView.delegate = self
        tableView.dataSource = self
        containerView.cornerRadius = 20
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        loadCards()
        getCardPaymentStatus()
        getAvailableBalance()
    }
    
    
    //MARK: - Actions
    @IBAction func deleteCardPressed(_ sender: Any) {
        
        let cardIndex = cards.firstIndex { (card) -> Bool in
            return card.isSelected
        }
        
        if let cardIndex = cardIndex {
            cards.remove(at: cardIndex)
            cards.first?.isSelected = true
            tableView.reloadData()
            
        }
    }
    
    @IBAction func addCardPressed(_ sender: Any) {
        self.navigationController?.pushViewController(AddCardViewController(), animated: true)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        (self.slideMenuController()?.rightViewController as? SideMenuViewController)?.switchToHome()
    }
    
    
    //MARK: - Private Methods
    private func loadCards() {
        
        cards.append(contentsOf: [
            Card(holderName: "John Doe", cardNumber: "4222-2222-3333-1111", expiry: "12/12", cvv: "123", type: .visa),
            Card(holderName: "John Doe", cardNumber: "5222-2222-3333-1111", expiry: "12/12", cvv: "123", type: .masterCard)
        ])
        cards.first?.isSelected = true
        tableView.reloadData()
    }
    
    private func getCardPaymentStatus() {
        Utility.showLoading()
        
        CardPaymentStatus.getCardStatus { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data?.cardStatus.first {
                    self.tableView.isHidden = data.status == 0
                    self.buttonsStackView.isHidden = data.status == 0
                }
            }
        }
    }
    
    private func getAvailableBalance() {
        Utility.showLoading()
        
        Balance.getBalance { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    self.availableBalanceLabel.text = "Rs. \(data.availableAmount)"
                    self.savedBalanceLabel.text = "\(data.discount)"
                    let formatter = DateFormatter()
                    formatter.dateFormat = "EEE, MMMM d, YYYY"
                    self.dateLabel.text = formatter.string(from: Date())
                }
                
            } else {
                 Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension WalletViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(CardTableViewCell.self, indexPath: indexPath)
        cell.configure(data: cards[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        cards.forEach { (card) in
            card.isSelected = false
        }
        cards[indexPath.row].isSelected = true
        self.tableView.reloadData()
    }
}
