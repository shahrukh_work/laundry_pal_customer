//
//  ItemListTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 20/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper

class ItemListTableViewCell: UITableViewCell {

    
    //MARK: - Variable
    var indexPath = IndexPath(row: 0, section: 0)
    var isCollapsed = false
    weak var delegate: ItemListTableViewCellDelegate?
    var data = Mapper<PriceLists>().map(JSON: [:])!
    
    
    //MARK: - Outlet
    @IBOutlet weak var tableView: CustomTableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var arrowImage: UIImageView!
    

    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView ()
    }
    
    
    //MARK: - Setup
    func setupView () {
        tableView.delegate = self
        tableView.dataSource = self
        let gestureRecon = UITapGestureRecognizer(target: self, action: #selector(headerTapped))
        headerView.isUserInteractionEnabled = true
        headerView.addGestureRecognizer(gestureRecon)
    }
    
    
    //MARK: - Selectors
    @objc func headerTapped() {
        delegate?.headerTapped(indexPath: indexPath)
    }
    
    
    //MARK: - Public Methods
    func configure(subCat: SubCategory, indexPath: IndexPath, priceList: PriceLists?) {
        titleLabel.text = subCat.name
        self.indexPath = indexPath
        isCollapsed = subCat.isCollapsed
        self.tableView.reloadData()
        var frameTable = self.tableView.frame;
        frameTable.size.height = self.tableView.contentSize.height;
        self.tableView.frame = frameTable
        
        if subCat.isCollapsed {
            arrowImage.image = #imageLiteral(resourceName: "arrowLeft")
            arrowImage.transform = CGAffineTransform(scaleX: -1, y: 1)
            
        } else {
            arrowImage.image = #imageLiteral(resourceName: "downArrow")
        }
        
        if let priceList = priceList {
            data = priceList
            tableView.reloadData()
        }
    }
    
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension ItemListTableViewCell: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isCollapsed ? 0 : data.priceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(PriceItemTableViewCell.self, indexPath: indexPath)
        cell.config(data: data.priceList[indexPath.row])
        return cell
    }
}
