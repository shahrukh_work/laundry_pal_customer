//
//  PriceItemTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 20/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class PriceItemTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var washIronPrice: UILabel!
    @IBOutlet weak var ironPriceLabel: UILabel!
    @IBOutlet weak var drycleanPriceLabel: UILabel!
    

    //MARK: - Variable


    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Public Methods
    func config(data: PriceList) {
        itemNameLabel.text = data.name
        
        washIronPrice.text = "-"
        ironPriceLabel.text = "-"
        drycleanPriceLabel.text = "-"
        
        data.washingMethods.forEach { (method) in
            
            if method.name == "Wash & Iron" {
                washIronPrice.text = "\(method.price)"
                
            } else if method.name == "Iron Only" {
                ironPriceLabel.text = "\(method.price)"
                
            } else if method.name == "Dry Clean" {
                drycleanPriceLabel.text = "\(method.price)"
            }
        }
    }
}
