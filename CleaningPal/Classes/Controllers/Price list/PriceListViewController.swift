//
//  PriceListViewController.swift
//  CleaningPal
//
//  Created by Mac on 20/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class PriceListViewController: UIViewController {
    
    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint!
    
    
    //MARK: - Variable
    var catDataSource = [Category]()
    var typeDataSource = [SubCategory]()
    var priceList = [Int: PriceLists]()
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView ()
    }
    
    
    //MARK: - Setup
    func setupView () {
        collectionView.delegate = self
        collectionView.dataSource = self
        tableView.delegate = self
        tableView.dataSource = self
        
        let alignedFlowLayout = AlignedCollectionViewFlowLayout()
        alignedFlowLayout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = alignedFlowLayout
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        loadCategories()
    }
    
    
    //MARK: - Actions
    @IBAction func backPressed(_ sender: Any) {
        (self.slideMenuController()?.rightViewController as? SideMenuViewController)?.switchToHome()
    }
    
    
    //MARK: - Private Methods
    private func loadCategories() {
        Utility.showLoading()
        
        Categories.getCategories { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    self.catDataSource = data.categories
                    
                    if let cat = data.categories.first {
                        self.loadSubCategories(catId: cat.id)
                        cat.isSelected = true
                    }
                    
                    self.collectionView.reloadData()
                }
    
            } else {
                 Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
    
    private func loadSubCategories(catId: Int) {
        Utility.showLoading()
        
        SubCategories.getSubCategories(categoryId: catId) { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    self.typeDataSource = data.subCategories
                    self.loadPrice()
                }
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
    
    func loadPrice() {
        priceList.removeAll()
        Utility.showLoading()
        
        DispatchQueue.global(qos: .userInitiated).async {
            let dispatchGroup = DispatchGroup()
            
            self.typeDataSource.forEach { (subCat) in
                dispatchGroup.enter()
                PriceLists.getPriceList(subCatId: subCat.id) { (data, error) in
                    
                    if error == nil {
                        
                        if let data = data {
                            self.priceList[subCat.id] = data
                        }
                        
                    } else {
                        Utility.showAlertController(self, error?.localizedDescription ?? "")
                    }
                    
                    dispatchGroup.leave()
                }
            }
            
            dispatchGroup.wait()
            DispatchQueue.main.async {
                Utility.hideLoading()
                self.tableView.reloadData()
            }
        }
        
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension PriceListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        typeDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(ItemListTableViewCell.self, indexPath: indexPath)
        cell.configure(subCat: typeDataSource[indexPath.row], indexPath: indexPath, priceList: priceList[typeDataSource[indexPath.row].id])
        cell.delegate = self
        return cell
    }
}


//MARK: - UICollectionViewDelegate & UICollectionViewDataSource
extension PriceListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return catDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.register(TypeCollectionViewCell.self, indexPath: indexPath)
        cell.configure(category: catDataSource[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        for item in catDataSource {
            item.isSelected = false
        }
        catDataSource[indexPath.row].isSelected = true
        loadSubCategories(catId: catDataSource[indexPath.row].id)
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let label = UILabel(frame: CGRect.zero)
        label.text = catDataSource[indexPath.row].name
        label.sizeToFit()
        return CGSize(width: label.frame.width + 20, height: 32)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}


//MARK: - ItemListTableViewCellDelegate
extension PriceListViewController: ItemListTableViewCellDelegate {
    
    func headerTapped(indexPath: IndexPath) {
        typeDataSource[indexPath.row].isCollapsed.toggle()
        tableView.reloadData()
    }
}

