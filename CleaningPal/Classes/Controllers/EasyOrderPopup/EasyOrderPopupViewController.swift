//
//  EasyOrderPopupViewController.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 6/9/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
enum ShowAlertMode {
    case easyOrder
    case alert
    case loginOption
}

class EasyOrderPopupViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    
    //MARK: - Variables
    weak var delegate: EasyOrderPopupViewControllerDelegate?
    var alertType: ShowAlertMode = .easyOrder
    var descriptionText = "The minimum price for Easy Order is Rs 2000. \n The Driver will insert details upon Pickup. \n Please review the Price List."
    var titleText = "Easy way to Order:"
    
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        titleLabel.text = titleText
        descriptionLabel.text = descriptionText
        
        if alertType == .alert {
            okButton.setTitle("OK", for: .normal)
            cancelButton.isHidden = true
            
        } else if  alertType == .loginOption {
            titleLabel.text = "Login to proceed"
            descriptionLabel.text = "You are not currently login to our system. Only logged in user can place order."
            okButton.setTitle("Login", for: .normal)
        }
    }
    
    //MARK: - Actions
    @IBAction func okPressed(_ sender: Any) {
    
        if alertType == .easyOrder {
            self.dismiss(animated: true, completion: nil)
            delegate?.acceptPressed()
            
        } else if alertType == .loginOption {
            Utility.loginRootViewController()
            
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
