//
//  CategoryCollectionViewCell.swift
//  CleaningPal
//
//  Created by Mac on 14/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import SDWebImage

class CategoryCollectionViewCell: UICollectionViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var whiteBackgrounf: UIView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var selectionBubbleImage: UIImageView!


    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Configure
    func configure (category: Category, isCategorySelected: inout Bool, indexPath: IndexPath) {
        nameLabel.text = category.name
        
        if category.name == "Easy Order" {
            itemImageView.image = #imageLiteral(resourceName: "iOS")
            
        } else {
            
            if let url = URL(string: APIRoutes.baseUrl + APIRoutes.categoryImageBase + category.image) {
                itemImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "categoryPlaceholder"))
            }
        }
        
        if indexPath.row == 0 {
            itemImageView.alpha = 1.0
            nameLabel.alpha = 1.0
            whiteBackgrounf.alpha = 1.0
            
        } else {
            
            if category.isSelected {
                isCategorySelected = category.isSelected
                itemImageView.alpha = 1.0
                nameLabel.alpha = 1.0
                whiteBackgrounf.alpha = 1.0
                
            } else {
                
                if !isCategorySelected {
                    itemImageView.alpha = 1.0
                    nameLabel.alpha = 1.0
                    whiteBackgrounf.alpha = 1.0
                    return
                }
                itemImageView.alpha = 0.6
                whiteBackgrounf.alpha = 0.6
                nameLabel.alpha = 0.6
            }
        }
    }
}
