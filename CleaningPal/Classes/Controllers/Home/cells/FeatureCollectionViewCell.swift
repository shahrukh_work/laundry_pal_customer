//
//  FeatureCollectionViewCell.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/16/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class FeatureCollectionViewCell: UICollectionViewCell {

    
    //MARK: - Outlets
    @IBOutlet weak var imageView: UIImageView!
    
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Public Methods
    func configure(data: Features) {
        
        if let url = URL(string:  APIRoutes.baseUrl + APIRoutes.featureImageBase + data.image) {
            self.imageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ad_icon"))
        }
    }
}
