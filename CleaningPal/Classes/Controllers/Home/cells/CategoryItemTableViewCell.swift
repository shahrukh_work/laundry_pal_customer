//
//  CategoryItemTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 14/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper


class CategoryItemTableViewCell: UITableViewCell {

    
    //MARK: - Variable
    weak var delegate: CategoryItemCellDelegate?
    var selectedServiceIndex = 0
    var pickerView = UIPickerView()
    var indexPath = IndexPath(row: 0, section: 0)
    var dataSource = Mapper<HomeItem>().map(JSON: [:])!
    
    
    //MARK: - Outlet
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var categoryTextField: UITextField!
    

    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    
    //MARK: - Setup
    func setupView () {
        categoryTextField.tintColor = .clear
        categoryTextField.inputView = pickerView
        pickerView.dataSource = self
        pickerView.delegate = self
        categoryTextField.keyboardAppearance = .dark
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneTapped))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        categoryTextField.inputAccessoryView = toolBar
    }
    
    
    //MARK: - Selectors
    @objc func doneTapped() {
        delegate?.serviceSelected(selectedServiceType: selectedServiceIndex, indexPath: indexPath)
    }
    
    
    //MARK: - Actions
    @IBAction func addPressed(_ sender: Any) {
        delegate?.addTapped(indexPath: indexPath)
    }
    
    @IBAction func dropdownPressed(_ sender: Any) {
        categoryTextField.becomeFirstResponder()
    }
    
    
    //MARK: - Public Methods
    func config(data: HomeItem, indexPath: IndexPath) {
        self.indexPath = indexPath
        self.dataSource = data
        
        if !self.dataSource.washingMethods.isEmpty {
            let cat = dataSource.washingMethods[dataSource.selectedServiceIndex]
            categoryTextField.text = cat.name
            priceLabel.text = "\(cat.price)"
            
        } else {
            categoryTextField.text = ""
            priceLabel.text = ""
        }
        
        if let url = URL(string: APIRoutes.baseUrl + APIRoutes.itemImageBase + data.image) {
            itemImageView.sd_setImage(with: url, placeholderImage: nil)
        }
        titleLabel.text = data.name
    }
}


//MARK: - PickerView Delegate & DataSource
extension CategoryItemTableViewCell: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSource.washingMethods.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSource.washingMethods[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedServiceIndex = row
    }
}
