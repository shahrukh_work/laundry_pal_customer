//
//  TypeCollectionViewCell.swift
//  CleaningPal
//
//  Created by Mac on 14/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class TypeCollectionViewCell: UICollectionViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var gradientView: UILabel!
    

    //MARK: - Variable


    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - configure
    func configure (data: SubCategory) {
        
        self.layoutIfNeeded()
        let gradient = CAGradientLayer()
        gradient.frame = gradientView.bounds
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        if let layer = gradientView.layer.sublayers?.first {
            
            if layer.isKind(of: CAGradientLayer.self) {
                layer.removeFromSuperlayer()
            }
        }
        
        if data.isSelected {
            gradient.colors = [#colorLiteral(red: 0.07843137255, green: 0.6705882353, blue: 0.9568627451, alpha: 1).cgColor, #colorLiteral(red: 0.3333333333, green: 0.7764705882, blue: 0.9294117647, alpha: 1).cgColor]
            titleLabel.textColor = .white
            
        } else {
            gradient.colors = [#colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1).cgColor, #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1).cgColor]
            titleLabel.textColor = .gray
        }
        
        titleLabel.text = data.name
        gradientView.layer.insertSublayer(gradient, at: 0)
        
    }
    
    func configure (category: Category) {
        
        self.layoutIfNeeded()
        let gradient = CAGradientLayer()
        gradient.frame = gradientView.bounds
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        if let layer = gradientView.layer.sublayers?.first {
            
            if layer.isKind(of: CAGradientLayer.self) {
                layer.removeFromSuperlayer()
            }
        }
        
        if category.isSelected {
            gradient.colors = [#colorLiteral(red: 0.07843137255, green: 0.6705882353, blue: 0.9568627451, alpha: 1).cgColor, #colorLiteral(red: 0.3333333333, green: 0.7764705882, blue: 0.9294117647, alpha: 1).cgColor]
            titleLabel.textColor = .white
            
        } else {
            gradient.colors = [#colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1).cgColor, #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1).cgColor]
            titleLabel.textColor = .gray
        }
        
        titleLabel.text = category.name
        gradientView.layer.insertSublayer(gradient, at: 0)
        
    }
}
