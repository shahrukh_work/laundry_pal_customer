//
//  HomeViewController.swift
//  CleaningPal
//
//  Created by Mac on 14/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import ObjectMapper
import AlamofireImage
import FirebaseAuth
import SwiftyGif
import AVFoundation
import AVKit



protocol CartItemDelegate {
    func updateCartItemCount()
}

class HomeViewController: UIViewController {
    
    
    //MARK: - Variable
    var catDataSource = [Category]()
    var typeDataSource = [SubCategory]()
    var items = [HomeItem]()
    var lastContentOffset: CGFloat = 0
    private let locationManager = CLLocationManager()
    private let placesClient = GMSPlacesClient()
    private let imageMaxConstraint = 165
    private let catCollectionViewMaxConstraint = 90
    private let catCollectionViewMinConstraint = 80
    private let catCollectionViewMaxTopConstraint = 25
    private let catCollectionViewMinTopConstraint = 8
    var searchTerm = ""
    var currentPage = 0
    var isLoadingData = false
    var feature = [Features]()
    var collectionTimer: Timer!
    var apiOffset = 0
    var isCategorySelected = false
    var selectedCategoryId = ""
    var selectedSubCatId = ""
    var player = AVPlayer()
    var playerLayer: AVPlayerLayer? = nil
    
    
    //MARK: - Outlet
    @IBOutlet weak var optionsStackView: UIStackView!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var dashView: UIView!
    @IBOutlet weak var adCollectionView: UICollectionView!
    @IBOutlet weak var typeCollectionView: UICollectionView!
    @IBOutlet weak var showCollectionButton: UIButton!
    @IBOutlet weak var showListButton: UIButton!
    @IBOutlet weak var itemTableView: UITableView!
    @IBOutlet weak var itemCollectionView: UICollectionView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationlabel: UILabel!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var categoryTopConstraints: NSLayoutConstraint!
    @IBOutlet weak var adCollectionHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var noItemsFoundView: UIView!
    @IBOutlet weak var reloadindDataView: UIView!
    
    
    //MARK - Variables
    
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setupView()
        
        if let _ = DataManager.shared.getUser() {
            DataManager.shared.refreshUserInfo()
        }
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        adCollectionView.collectionViewLayout.invalidateLayout()
        categoryCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadindDataView.isHidden = true
        tabBarController?.tabBar.isHidden = false
        self.edgesForExtendedLayout = .bottom
        
        if let location = DataManager.shared.getLocation() {
            locationlabel.text = location.locationName
            
        } else {
            locationSetup()
        }
        
        let user = DataManager.shared.getUser()
        
        nameLabel.text = "\(user?.firstName ?? "Guest") \(user?.lastName ?? "")"
        
        if let url = URL(string: APIRoutes.baseUrl + APIRoutes.profileBase + (user?.image ?? "")) {
            profileButton.sd_setImage(with: url, for: .normal, placeholderImage: #imageLiteral(resourceName: "placeholderUser"))
        }
    }
    
    
    //MARK: - View Setup
    func setupView () {
        searchView.alpha = 0.0
        
        let alignedFlowLayout = AlignedCollectionViewFlowLayout(horizontalAlignment: .left, verticalAlignment: .top)
        alignedFlowLayout.scrollDirection = .horizontal
        typeCollectionView.collectionViewLayout = alignedFlowLayout
        
        bottomContainerView.cornerRadius = 20
        bottomContainerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        itemTableView.cornerRadius = 20
        itemTableView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        itemCollectionView.cornerRadius = 20
        itemCollectionView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        self.categoryCollectionView.delegate = self
        self.categoryCollectionView.dataSource = self
        self.typeCollectionView.delegate = self
        self.typeCollectionView.dataSource = self
        
        self.itemTableView.delegate = self
        self.itemTableView.dataSource = self
        self.itemCollectionView.delegate = self
        self.itemCollectionView.dataSource = self
        
        adCollectionView.delegate = self
        adCollectionView.dataSource = self
        
        showListButton.setImage(#imageLiteral(resourceName: "list_selected"), for: .normal)
        showCollectionButton.setImage(#imageLiteral(resourceName: "collection_unselected"), for: .normal)
        Utility.setPlaceHolderTextColor(searchTextField, "Search", #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.8490475171))
        loadCategories()
        loadItems(subCatId: -1, loadWithCat: false)
        loadFeatured()
        
        collectionTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(collectionScrollTicker), userInfo: nil, repeats: true)
        
        let categoryLayout = HomeCollectionLayout()
        categoryLayout.scrollDirection = .horizontal
        categoryCollectionView.collectionViewLayout = categoryLayout
        
        let adsLayout = HomeCollectionLayout()
        adsLayout.scrollDirection = .horizontal
        adCollectionView.collectionViewLayout = adsLayout
    }
    
    
    
    //MARK: - Selectors
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        
        if gesture.direction == .down {
            
            self.reloadindDataView.isHidden = false
            self.reloadindDataView.alpha = 1.0
            
            
            if self.searchTextField.text == "" {
               // self.items.removeAll()
              
                Utility.showLoading()
                if self.selectedSubCatId != "" {
                    self.loadSubCategories (catId: Int(self.selectedCategoryId)!, searchText: self.searchTextField.text ?? "")
                    
                } else {
                    self.loadSubCategories (catId: -1, searchText: self.searchTextField.text ?? "")
                }

            } else {
                Utility.showLoading()
                self.loadSearchResults()
            }
            
            UIView.animate(withDuration: 3.0) {
                self.reloadindDataView.alpha = 0.0
            }
        }
    }
    
    @objc func collectionScrollTicker() {
        
        if feature.count > currentPage {
            adCollectionView.scrollToItem(at: IndexPath(row: currentPage, section: 0), at: .left, animated: true)
            currentPage += 1
            
            if currentPage >= feature.count {
                currentPage = 0
            }
        }
    }
    
    
    //MARK: - Actions
    @IBAction func listPressed(_ sender: Any) {
        
        showListButton.setImage(#imageLiteral(resourceName: "list_selected"), for: .normal)
        showCollectionButton.setImage(#imageLiteral(resourceName: "collection_unselected"), for: .normal)
        itemTableView.isHidden = false
        itemCollectionView.isHidden = true
    }
    
    @IBAction func collectionPressed(_ sender: Any) {
        showListButton.setImage(#imageLiteral(resourceName: "list_unselected"), for: .normal)
        showCollectionButton.setImage(#imageLiteral(resourceName: "collection_selected"), for: .normal)
        itemTableView.isHidden = true
        itemCollectionView.isHidden = false
    }
    
    @IBAction func searchPressed(_ sender: Any) {
        adCollectionHeightConstraints.constant = 0
        adCollectionView.reloadData()
        adCollectionView.alpha = 0
        collectionViewHeightConstraint.constant = CGFloat(catCollectionViewMinConstraint)
        categoryTopConstraints.constant = CGFloat(catCollectionViewMinTopConstraint)
        categoryCollectionView.reloadData()
        
        searchView.isHidden = false
        
        UIView.animate(withDuration: 0.4, animations: {
            self.profileView.alpha = 0
            self.searchView.alpha = 1
            self.view.layoutIfNeeded()
            
        }, completion: { _ in
            self.profileView.isHidden = true
        })
    }
    
    @IBAction func searhTextChanged(_ sender: Any) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            
            if self?.searchTerm == self?.searchTextField.text {
                self?.isLoadingData = true
                self?.apiOffset = 0
                self?.loadSearchResults()
            }
        }
        
        searchTerm = searchTextField.text ?? ""
    }
    
    @IBAction func notificationPressed(_ sender: Any) {
        
        if let _ = DataManager.shared.getUser() {
            let controller = NotificationListViewController()
            self.navigationController?.pushViewController(controller, animated: true)
            
        } else {
            Utility.showLoginOptions(tabController: self.tabBarController)
        }
    }
    
    @IBAction func profilePressed(_ sender: Any) {
        
        if let _ = DataManager.shared.getUser() {
            let profileVC = EditProfileViewController()
            self.navigationController?.pushViewController(profileVC, animated: true)
            
        } else {
            Utility.showLoginOptions(tabController: self.tabBarController)
        }
    }
    
    
    //MARK: - Private Methods
    private func loadCategories() {
        
        Categories.getCategories { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    self.catDataSource = data.categories
                    let categoryEasy = Mapper<Category>().map(JSON: [:])!
                    categoryEasy.name = "Easy Order"
                    self.catDataSource.insert(categoryEasy, at: 0)
                    self.categoryCollectionView.reloadData()
                }
                
            } else {
                 Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
    
    private func loadSubCategories(catId: Int, searchText: String) {
        
        SubCategories.getSubCategories(categoryId: catId) { (data, error) in
            
            if error == nil {
                
                if let data = data {
                    self.typeDataSource = data.subCategories
                    
                    if let data = self.typeDataSource.first {
                        data.isSelected = true
                        self.selectedSubCatId = "\(data.id)"
                        
                        if self.searchTextField.text == "" {
                            
                            if self.selectedSubCatId != "" {
                                Utility.showLoading()
                                self.loadItems(subCatId: Int(self.selectedSubCatId)!)
                                
                            } else {
                                self.loadItems(subCatId: data.id)
                            }
                           
                        } else {
                            self.loadSearchResults()
                        }
                        
                    } else {
                        self.items.removeAll()
                        if self.searchTextField.text == "" {
                            self.loadItems(subCatId: -1, loadWithCat: false)
                        } else {
                            self.loadSearchResults()
                        }
                    }
                    self.typeCollectionView.reloadData()
                }
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
    
    private func loadItems(subCatId: Int, loadWithCat: Bool = true) {

        if apiOffset == 0 {
            items.removeAll()
        }
        Utility.showLoading()
        HomeItems.getHomeItems(query: loadWithCat ? "?sub_category_id=\(subCatId)" : "", offset: apiOffset) { (data, error) in
            Utility.hideLoading()
            self.isLoadingData = false
            
            if error == nil {
                
                if let data = data {
                    
//                    if data.data.count > 0 {
//                        self.items.removeAll()
//                    }
                    
                    self.items.append(contentsOf: data.data)
                    self.noItemsFoundView.isHidden = !self.items.isEmpty
                    
                    self.items.forEach { (item) in
                        item.washingMethods.sort { (a, b) -> Bool in
                            return a.position < b.position
                        }
                    }
                    
                    self.items.forEach({ (item) in
                        item.washingMethods.first?.isSelected = true
                    })
                    self.itemTableView.reloadData()
                    self.itemCollectionView.reloadData()
                }
                
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
    
    private func loadFeatured() {
            
        Featured.getFeatured(offset: 0, limit: 10, featuredType: 1) { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    
                    self.feature = data.featured
                    self.adCollectionView.reloadData()
                }
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
    
    private func locationSetup() {
        locationManager.delegate = self
        
        switch(CLLocationManager.authorizationStatus()) {
            
        case .restricted, .denied:
            self.showOkAlertWithOKCompletionHandler("Application uses location data for navigations and maps. Kindly enable location services in order to use the application.") { ( _) in
                UIApplication.shared.open(NSURL(string:UIApplication.openSettingsURLString)! as URL)
            }
            
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
        default:
            break
        }
    }
    
    private func loadSearchResults() {

        categoryCollectionView.reloadData()
      //  typeCollectionView.isHidden = true
        self.isLoadingData = false
        HomeItems.searchItems(catId: selectedCategoryId, subCatId: selectedSubCatId, searchTerm: searchTerm, offset: apiOffset) { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    self.items = data.data
                    self.noItemsFoundView.isHidden = !data.data.isEmpty
                    
                    self.items.forEach { (item) in
                        item.washingMethods.sort { (a, b) -> Bool in
                            return a.position < b.position
                        }
                    }
                    
                    self.items.forEach({ (item) in
                        item.washingMethods.first?.isSelected = true
                    })
                    self.itemTableView.reloadData()
                    self.itemCollectionView.reloadData()
                    self.isLoadingData = false
                }
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
    
    private func easyOrderTapped() {
        let controller = EasyOrderPopupViewController()
        controller.delegate = self
        controller.modalPresentationStyle = .overCurrentContext
        self.tabBarController?.present(controller, animated: true, completion: nil)
    }
    
    private func getAddressFromLatLon(coordinates: CLLocationCoordinate2D) {
        let geoCoder = GMSGeocoder()
        
        geoCoder.reverseGeocodeCoordinate(coordinates) { (response, error) in
            
            if error == nil {
                
                if let response = response {
                    
                    if let pm = response.firstResult() {
                        
                        var addressString : String = ""
                        var city = ""
                        var state = ""
                        
                        if let name = pm.thoroughfare {
                            addressString = addressString + name + ", "
                        }
                        
                        if let subLocality = pm.subLocality {
                            addressString = addressString + subLocality + ", "
                        }
                        
                        if let locality = pm.locality {
                            addressString = addressString + locality + ", "
                            city = locality
                        }
                        
                        if let admin = pm.administrativeArea {
                            state = admin
                            addressString = addressString + admin + ", "
                        }
                        
                        if let country = pm.country {
                            addressString = addressString + country
                        }
                        
                        
                        let location = Mapper<Location>().map(JSON: [
                            "location_name": addressString,
                            "loc_city": city,
                            "location_coordinates": "\(coordinates.latitude),\(coordinates.longitude)",
                            "loc_state": state
                        ])
                        
                        DataManager.shared.setLocation(location: location?.toJSONString() ?? "")
                        self.locationlabel.text = addressString
                        
                        
                    }
                }
            } else {
                print(error?.localizedDescription)
            }
        }
    }
}


//MARK: - UICollectionViewDelegate & UICollectionViewDataSource
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.categoryCollectionView {
            return self.catDataSource.count
        }
        
        if collectionView == itemCollectionView {
            return items.count
        }
        
        if collectionView == adCollectionView {
            return feature.count
        }
        return self.typeDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.categoryCollectionView {
            let cell = collectionView.register(CategoryCollectionViewCell.self, indexPath: indexPath)
            cell.configure(category: catDataSource[indexPath.row], isCategorySelected: &isCategorySelected, indexPath:indexPath)
            return cell
        }
        
        if collectionView == self.itemCollectionView {
            let cell = collectionView.register(CategoryItemCollectionViewCell.self, indexPath: indexPath)
            cell.config(data: items[indexPath.row], indexPath: indexPath)
            cell.delegate = self
            return cell
        }
        
        if collectionView == self.adCollectionView {
            let cell = collectionView.register(FeatureCollectionViewCell.self, indexPath: indexPath)
            cell.configure(data: feature[indexPath.row])
            return cell
        }
        
        let cell = collectionView.register(TypeCollectionViewCell.self, indexPath: indexPath)
        cell.configure(data: typeDataSource[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == itemCollectionView {
            return
        }
        
        if collectionView == adCollectionView {
            let controller = PromotionDetailViewController()
            controller.featured = feature[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
            
        } else if collectionView == categoryCollectionView {
            
            if indexPath.row == 0 {
                
                if let _ = DataManager.shared.getUser() {
                    easyOrderTapped()
                    
                } else {
                    Utility.showLoginOptions(tabController: self.tabBarController)
                }
                return
            }
            
            let option = catDataSource[indexPath.row].isSelected
            
            for item in catDataSource {
                item.isSelected = false
            }
            
            catDataSource[indexPath.row].isSelected = !option
            typeCollectionView.isHidden = option
            
            apiOffset = 0
            if catDataSource[indexPath.row].isSelected {
                selectedCategoryId =
                    "\(catDataSource[indexPath.row].id)"
                Utility.showLoading()
                loadSubCategories(catId: catDataSource[indexPath.row].id, searchText: searchTextField.text ?? "")
                
            } else {
                selectedCategoryId = ""
                selectedSubCatId = ""
                if searchTextField.text == "" {
                    Utility.showLoading()
                    loadItems(subCatId: -1, loadWithCat: false)
                    
                } else {
                    Utility.showLoading()
                    loadSearchResults()
                }
               //
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                
                let cell = collectionView.cellForItem(at: indexPath)
                
                UIView.animate(withDuration: 0.2, animations: {
                    cell?.frame.origin.x += 10
                    
                }) { (completed) in
                    UIView.animate(withDuration: 0.2, animations: {
                        cell?.frame.origin.x -= 10
                        
                    })
                }
            }
            
        } else {
            
            apiOffset = 0
            for item in typeDataSource {
                item.isSelected = false
            }
            typeDataSource[indexPath.row].isSelected = true
            selectedSubCatId = "\(typeDataSource[indexPath.row].id)"
            
            if searchTextField.text == "" {
                loadItems(subCatId: typeDataSource[indexPath.row].id)
                
            } else {
                loadSearchResults()
            }
        }
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == categoryCollectionView {
            return CGSize(width: collectionView.frame.height, height: collectionView.frame.height)
        }
        
        if collectionView == adCollectionView {
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        }
        
        if collectionView == itemCollectionView {
            let padding: CGFloat =  0
            let collectionViewSize = collectionView.frame.size.width + padding
            return CGSize (width: collectionViewSize/2, height: 206)
        }
        
        let label = UILabel(frame: CGRect.zero)
        label.text = typeDataSource[indexPath.row].name
        label.sizeToFit()
        return CGSize(width: label.frame.width + 20, height: 32)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == itemCollectionView  {
            return 0
        }
        return 10
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == itemCollectionView {
            return 0
        }
        
        if collectionView == adCollectionView {
            return 0
        }
        
        if collectionView == categoryCollectionView && catDataSource.count > 4 {
            return 6
        }
        return ((collectionView.frame.width - CGFloat(collectionView.frame.height * CGFloat(catDataSource.count))) / CGFloat(catDataSource.count) - 1)
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(CategoryItemTableViewCell.self, indexPath: indexPath)
        cell.config(data: items[indexPath.row], indexPath: indexPath)
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            
            let y: CGFloat = scrollView.contentOffset.y
            let newHeaderViewHeight: CGFloat = self.adCollectionHeightConstraints.constant - y
            
            if Int(newHeaderViewHeight) > self.imageMaxConstraint {
                self.adCollectionHeightConstraints.constant = CGFloat(self.imageMaxConstraint)
                self.adCollectionView.reloadData()
                self.adCollectionView.alpha = 1
                self.collectionViewHeightConstraint.constant = CGFloat(self.catCollectionViewMaxConstraint)
                self.categoryTopConstraints.constant = CGFloat(self.catCollectionViewMaxTopConstraint)
                self.categoryCollectionView.reloadData()
                self.view.layoutIfNeeded()
                
            } else if newHeaderViewHeight < 0 {
                self.adCollectionHeightConstraints.constant = 0
                self.adCollectionView.reloadData()
                self.adCollectionView.alpha = 0
                self.collectionViewHeightConstraint.constant = CGFloat(self.catCollectionViewMinConstraint)
                self.categoryTopConstraints.constant = CGFloat(self.catCollectionViewMinTopConstraint)
                self.categoryCollectionView.reloadData()
                
            } else {
                let ratio = newHeaderViewHeight/CGFloat(self.imageMaxConstraint)
                let catHeight = CGFloat(self.catCollectionViewMaxConstraint) * (ratio)
                let catTop = CGFloat(self.catCollectionViewMaxTopConstraint) * (ratio)
                self.adCollectionHeightConstraints.constant = newHeaderViewHeight
                self.adCollectionView.reloadData()
                self.collectionViewHeightConstraint.constant = catHeight > CGFloat(self.catCollectionViewMinConstraint) ? catHeight : CGFloat(self.catCollectionViewMinConstraint)
                self.categoryTopConstraints.constant = catTop > CGFloat(self.catCollectionViewMinTopConstraint) ? catTop : CGFloat(self.catCollectionViewMinTopConstraint)
                self.categoryCollectionView.reloadData()
                self.adCollectionView.alpha = ratio
                
                if ratio > 0.5 {
                    
                    UIView.animate(withDuration: 0.5, animations: {
                        self.profileView.alpha = 1
                        self.searchView.alpha = 0
                        
                    }, completion: { _ in
                        self.profileView.isHidden = false
                        self.searchView.isHidden = true
                    })
                    
                } else {
                    
                    UIView.animate(withDuration: 0.5, animations: {
                        self.profileView.alpha = 0
                        self.searchView.alpha = 1
                        
                    }, completion: { _ in
                         self.searchView.isHidden = false
                        self.profileView.isHidden = true
                    })
                }
                scrollView.contentOffset.y = 0
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == itemTableView || scrollView == itemCollectionView {
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                
                if !isLoadingData {
                    
                    let filter = catDataSource.first { (cat) -> Bool in
                        return cat.isSelected
                    }
                    
                    if filter != nil {
                        
                        let subCat = typeDataSource.first { (subCat) -> Bool in
                            return subCat.isSelected
                        }
                        
                        if let subCat = subCat {
                            selectedSubCatId = "\(subCat.id)"
                            apiOffset += 1
                            isLoadingData = true
                            
                            if searchTextField.text == "" {
                                Utility.showLoading()
                                self.loadItems(subCatId: subCat.id)
                                
                            } else {
                                Utility.showLoading()
                                loadSearchResults()
                            }
                        }
                        
                    } else {
                        isLoadingData = true
                        apiOffset += 1
                        if searchTextField.text == "" {
                            Utility.showLoading()
                            loadItems(subCatId: -1, loadWithCat: false)
                        } else {
                            Utility.showLoading()
                            loadSearchResults()
                        }
                    }
                }
            }
        }
    }
}


//MARK: - AddItemDelegate
extension HomeViewController: CategoryItemCellDelegate {
    
    func addTapped(indexPath: IndexPath) {
        let controller = AddItemViewController()
        controller.delegate = self
        controller.product = items[indexPath.row]
        controller.modalPresentationStyle = .overCurrentContext
        tabBarController?.present(controller, animated: true, completion: nil)
    }
    
    func serviceSelected(selectedServiceType: Int, indexPath: IndexPath) {
        items[indexPath.row].selectedServiceIndex = selectedServiceType
        
        items[indexPath.row].washingMethods.forEach { (method) in
            method.isSelected = false
        }
        items[indexPath.row].washingMethods[selectedServiceType].isSelected = true
        itemTableView.reloadData()
        itemCollectionView.reloadData()
    }
}


//MARK: - CartItemDelegate
extension HomeViewController: CartItemDelegate {
    
    func updateCartItemCount() {
        let cartItems = DataManager.shared.getCart()
        AppUser.shared.cartItemsCount = cartItems.cartItems.count
        tabBarController?.tabBar.items?[2].badgeValue = "\(AppUser.shared.cartItemsCount)"
    }
}


//MARK: - CLLocationManagerDelegate
extension HomeViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        locationManager.stopUpdatingLocation()
        
        if let location = locations.first {
            getAddressFromLatLon(coordinates: location.coordinate)
        }
    }
}


//MARK: - EasyOrderPopupViewControllerDelegate
extension HomeViewController: EasyOrderPopupViewControllerDelegate {
    
    func acceptPressed() {
        let controller = SelectAddressViewController()
        controller.orderType = .easy
        controller.extendedLayoutIncludesOpaqueBars = true
        controller.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
