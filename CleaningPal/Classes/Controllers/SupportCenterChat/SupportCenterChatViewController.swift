//
//  SupportCenterChatViewController.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 5/14/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper
import IQKeyboardManagerSwift
import SocketIO


class SupportCenterChatViewController: UIViewController {
    
    
    //MARK: - Outlets
    @IBOutlet weak var bottomMessageView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var sendMessageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var messageTextViewHeightConstraint: NSLayoutConstraint!
    
    
    //MARK: - Variables
    var chat = [Chat]()
    var onlineAdmin = Mapper<OnlineAdmin>().map(JSON: [:])!
    let dateFormatter = DateFormatter()
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        setupView()
        registerKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
        AppUser.shared.socket.disconnect()
        self.tabBarController?.tabBar.isHidden = false
        NotificationCenter.default.removeObserver(self)
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        bottomMessageView.layer.cornerRadius = bottomMessageView.frame.height/2
        tableView.delegate = self
        tableView.dataSource = self
        messageTextView.delegate = self
        messageTextView.text = "Type"
        messageTextView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        if tableView.numberOfRows(inSection: 0) > 0 {
            tableView.scrollToRow(at: IndexPath(row: tableView.numberOfRows(inSection: 0) - 1, section: 0), at: .bottom, animated: true)
        }
      //  dateFormatter.locale = Locale(identifier: "en_PK")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
     //   dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        tableView.cornerRadius = 20
        tableView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        let doneToolBar = UIToolbar()
        doneToolBar.sizeToFit()
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let btnDone = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(keyboardDonePressed))
        doneToolBar.items = [spaceItem, btnDone]
        messageTextView.inputAccessoryView = doneToolBar
        
        AppUser.shared.socketManager = SocketManager(socketURL: URL(string: APIRoutes.baseUrl)!, config: [.log(true), .compress, .extraHeaders(["Authorization" : "Bearer \(AppUser.authData?.token ?? "")"])])
        AppUser.shared.socket = AppUser.shared.socketManager.defaultSocket
        AppUser.shared.socket.connect()
        AppUser.shared.socket.on(clientEvent: .connect) { (data, error) in
            AppUser.shared.socket.emit("join-customer", ["customer_id": DataManager.shared.getUser()?.id ?? -1])
        }
        
        AppUser.shared.socket.on(clientEvent: .error) { (data, ackEmitter) in
            print(data)
        }
        
        AppUser.shared.socket.on("chat-message") { [weak self] (data, ackEmitter) in
            
            if let data = data.first {
                
                if let data = Mapper<Chat>().map(JSONObject: data) {
                    data.customerImage = DataManager.shared.getUser()?.image ?? ""
                    data.adminImage = self?.onlineAdmin.image ?? ""
                    data.createdAt = self?.dateFormatter.string(from: Date()) ?? ""
                    self?.chat.append(data)
                    self?.tableView.reloadData()
                    
                    if let tableView = self?.tableView {
                        
                        if tableView.numberOfRows(inSection: 0) > 0 {
                            tableView.scrollToRow(at: IndexPath(row: tableView.numberOfRows(inSection: 0) - 1, section: 0), at: .bottom, animated: true)
                        }
                    }
                }
            }
        }
        
        loadChatHistory()
        loadOnlineAdmin()
    }
    
    
    //MARK: - Selectors
    @objc func keyboardDonePressed() {
        self.view.endEditing(true)
    }
    
    
    //MARK: - Actions
    @IBAction func sendButonPressed(_ sender: Any) {
        
        if messageTextView.text != "" {
            
            let user = DataManager.shared
            .getUser()
            
            let data = [
                "customer_id": user?.id ?? -1,
                "admin_id": onlineAdmin.id,
                "message": messageTextView.text ?? "",
                "send_by": 1,
                "type": "customer",
                "customer_image": user?.image ?? "",
                "admin_image": self.onlineAdmin.image
                
            ] as [String: AnyObject]
            
            if let data = Mapper<Chat>().map(JSON: data) {
                data.createdAt = self.dateFormatter.string(from: Date())
                self.chat.append(data)
                self.tableView.reloadData()
                if tableView.numberOfRows(inSection: 0) > 0 {
                    tableView.scrollToRow(at: IndexPath(row: tableView.numberOfRows(inSection: 0) - 1, section: 0), at: .bottom, animated: true)
                }
            }
            
            AppUser.shared.socket.emit("support-chat", data)
            messageTextView.text = ""
            
        }
        messageTextViewHeightConstraint.constant = 37
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Private Methods
    private func loadChatHistory() {
        Utility.showLoading()
        
        ChatHistory.getChatHistory { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    self.chat = data.chat
                    self.chat.reverse()
                    self.tableView.reloadData()
                    if self.tableView.numberOfRows(inSection: 0) > 0 {
                        self.tableView.scrollToRow(at: IndexPath(row: self.tableView.numberOfRows(inSection: 0) - 1, section: 0), at: .bottom, animated: true)
                    }
                }
            }
        }
    }
    
    private func loadOnlineAdmin() {
        Utility.showLoading()
        
        OnlineAdmin.getOnlineAdmin { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    self.onlineAdmin = data
                }
            }
        }
    }
}


//MARK: - TableView Delegate & DataSource
extension SupportCenterChatViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return DateTableHeaderView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chatMessage = chat[indexPath.row]
        
        if chatMessage.sendBy == 0 {
            let cell = tableView.register(IncomingMessageTableViewCell.self, indexPath: indexPath)
            cell.configCell(chat: chatMessage)
            return cell
        }
        
        let cell = tableView.register(OutgoingMessageTableViewCell.self, indexPath: indexPath)
        cell.configCell(chat: chatMessage)
        return cell
    }
}


//MARK: - TextView Delegate
extension SupportCenterChatViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            messageTextViewHeightConstraint.constant = 37
            textView.text = "Type"
            textView.textColor = #colorLiteral(red: 0.3294117647, green: 0.3647058824, blue: 0.4352941176, alpha: 1)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let minTextViewHeight: CGFloat = 37
        let maxTextViewHeight: CGFloat = 111
        
        var height = ceil(textView.contentSize.height) // ceil to avoid decimal
        
        if (height < minTextViewHeight) {
            height = minTextViewHeight
        }
        
        if (height > maxTextViewHeight) {
            height = maxTextViewHeight
        }
        
        if height != messageTextViewHeightConstraint.constant {
            messageTextViewHeightConstraint.constant = height
            textView.setContentOffset(.zero, animated: false) // scroll to top to avoid "wrong contentOffset" artefact when line count changes
        }
    }
}


//MARK: - KeyBoardNotifications
extension SupportCenterChatViewController {
    
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
                
        if keyboardSize.height > 200  && sendMessageViewBottomConstraint.constant < 100{
            let window = UIApplication.shared.keyWindow
            
            UIView.animate(withDuration: 0.1) {
                
                if let win = window {
                    self.sendMessageViewBottomConstraint.constant = self.sendMessageViewBottomConstraint.constant + keyboardSize.height -  win.safeAreaInsets.bottom
                }
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        
        if keyboardSize.height > 200  && sendMessageViewBottomConstraint.constant > 100 {
            
            UIView.animate(withDuration: 0.1) {
                self.sendMessageViewBottomConstraint.constant = 8
                self.view.layoutIfNeeded()
            }
        }
    }
}
