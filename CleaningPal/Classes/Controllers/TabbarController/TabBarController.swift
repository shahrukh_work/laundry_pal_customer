//
//  TabBarController.swift
//  CleaningPal
//
//  Created by Mac on 28/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import SwiftyGif
import AVFoundation
import AVKit


class TabBarController:  UITabBarController, UITabBarControllerDelegate,SwiftyGifDelegate {
    
    
    //MARK: - Variables
    let cartNavViewController = UINavigationController()
    let promotionsViewController = UINavigationController()
    let promotionNavViewController = UINavigationController()
    let homeNavigationController = UINavigationController(rootViewController: HomeViewController())
    let logoAnimationView = LogoAnimationView()
    var player = AVPlayer()
    var playerLayer: AVPlayerLayer? = nil
    var backgroundView: UIView {
        return UIView()
    }
    
    
    //MARK: -  ViewController LifeCycle
    override func viewDidLoad(){
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: nil)
        self.delegate = self
        
        let tabBackgroundView = UIView(frame: tabBar.bounds)
        tabBackgroundView.backgroundColor = UIColor.white
        tabBackgroundView.translatesAutoresizingMaskIntoConstraints = false;


        tabBar.addSubview(tabBackgroundView)
        tabBar.sendSubviewToBack(tabBackgroundView)
        tabBackgroundView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        tabBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabBackgroundView.layer.shadowRadius = 4.0
        tabBackgroundView.layer.cornerRadius = 12
        tabBackgroundView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        tabBackgroundView.layer.shadowColor = UIColor.gray.cgColor
        tabBackgroundView.layer.shadowOpacity = 0.6
        tabBar.clipsToBounds = false
        tabBar.backgroundImage = UIImage()
        tabBar.shadowImage = UIImage()

        if #available(iOS 13.0, *) {
            let appearance = tabBar.standardAppearance
            appearance.shadowImage = nil
            appearance.shadowColor = nil
            appearance.backgroundEffect = nil
            tabBar.standardAppearance = appearance
            
        } else {
            let image = UIImage()
            tabBar.shadowImage = image
            tabBar.backgroundImage = image
        }
        
        homeNavigationController.isNavigationBarHidden = true
        
        let rightViewController = SideMenuViewController()
        rightViewController.homeController = homeNavigationController
        
        let sideMenuVC = SlideMenuController(mainViewController: homeNavigationController, rightMenuViewController: rightViewController)
        sideMenuVC.delegate = self
        sideMenuVC.changeRightViewWidth(Utility.getScreenWidth() - 100)
        sideMenuVC.tabBarItem.image = UIImage(named: "home")
        sideMenuVC.tabBarItem.selectedImage = #imageLiteral(resourceName: "home_active")//UIImage(named: "home-active")
        sideMenuVC.tabBarItem.title = "Home"
        
        let cartItemsVC = CartItemsViewController()
        cartItemsVC.tabBarItem.image = UIImage(named: "cart")
        cartItemsVC.tabBarItem.selectedImage = #imageLiteral(resourceName: "cart_active")//UIImage(named: "cart-active")
        
        let count = DataManager.shared.getCart().cartItems.count
        cartItemsVC.tabBarItem.badgeValue = "\(count)"
        AppUser.shared.cartItemsCount = count
        cartItemsVC.tabBarItem.title = "Cart"
        cartNavViewController.viewControllers = [cartItemsVC]
        cartNavViewController.navigationBar.isHidden = true
        
        let priceList = PriceListViewController()
        priceList.tabBarItem.image = UIImage(named: "pricelist")
        priceList.tabBarItem.selectedImage = #imageLiteral(resourceName: "pricelist_selected")
        priceList.tabBarItem.title = "Price List"
        
        
        let promotionVC = PromotionListViewController()
        promotionsViewController.viewControllers = [promotionVC]
        promotionVC.tabBarItem.image = UIImage(named: "promotions")
        promotionVC.tabBarItem.selectedImage = #imageLiteral(resourceName: "promotions_active")// UIImage(named: "promotions-active")
        promotionVC.tabBarItem.title = "Promotions"
        
        let sideMenu = EmptyViewController()
        sideMenu.tabBarItem.image = UIImage(named: "more-1")
        sideMenu.tabBarItem.selectedImage = UIImage(named: "more_active")
        sideMenu.tabBarItem.title = "More"
        
        tabBar.tintColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.8745098039, alpha: 1)
        
        viewControllers = [sideMenuVC, priceList, cartNavViewController, promotionsViewController, sideMenu]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !showLoader {
            loadVideo()
            showLoader = true
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        logoAnimationView.logoGifImageView.startAnimatingGif()
    }
    
    func gifDidStop(sender: UIImageView) {
        logoAnimationView.isHidden = true
        AppUser.shared.showLoader = true
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        if viewController is EmptyViewController {
            tabBarController.selectedIndex = 0
            homeNavigationController.openRight()
            viewController.tabBarItem.image = UIImage(named: "more_active")?.withRenderingMode(.alwaysOriginal)
            return false
            
        } else {
            viewControllers?.last?.tabBarItem.image = UIImage(named: "more-1")
        }
        
        return true
    }
    
    
    //MARK: - Selectors
    @objc func playerItemDidReachEnd(notification: NSNotification) {
        playerLayer?.zPosition = -1
    }
    
    
    //MARK: - Methods
    private func loadVideo() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
        } catch { }

        let path = Bundle.main.path(forResource: "launch1", ofType:"mp4")

        player = AVPlayer(url: NSURL(fileURLWithPath: path!) as URL)
        playerLayer = AVPlayerLayer(player: player)

        playerLayer?.frame = self.view.frame
        playerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer?.zPosition = 1

        self.view.layer.addSublayer(playerLayer!)

        player.seek(to: CMTime.zero)
        player.play()
        
    }
}


extension TabBarController: SlideMenuControllerDelegate {
    
    func rightDidClose() {
        viewControllers?.last?.tabBarItem.image = UIImage(named: "more_tab")
    }
}
