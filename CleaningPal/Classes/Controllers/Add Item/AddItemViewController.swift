//
//  AddItemViewController.swift
//  CleaningPal
//
//  Created by Mac on 13/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper


class AddItemViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var selectedItemLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    

    //MARK: - Variable
    var delegate: CartItemDelegate?
    var quantity = 1
    var bill = 0
    var product = Mapper<HomeItem>().map(JSON: [:])!
    

    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    
    //MARK: - Setup
    func setupView () {
        tableView.delegate = self
        tableView.dataSource = self
        containerView.clipsToBounds = true
        calculateBill()
        self.countLabel.text = "\(quantity)"
        selectedItemLabel.text = product.name
    }
    
    
    //MARK: - Actions
    @IBAction func addPressed(_ sender: Any) {
        
        let cart = DataManager.shared.getCart()
        let selectedItem = product.washingMethods.first { (method) -> Bool in
            return method.isSelected
        }
        
        
        if let selectedItem = selectedItem {
            
            let item = Mapper<CartItem>().map(JSON: [
                "item_id" : product.id,
                "price": selectedItem.price,
                "washing_method": selectedItem.name,
                "quantity": quantity,
                "sub_cat": product.subCategory,
                "name": product.name
            ])
            
            if quantity == 0 {
                self.showOkAlert("Please set quantity.")
                return
            }
            
            if let item = item {
                
                cart.addToCart(cartItem: item)
                DataManager.shared.setCart(cart: cart.toJSONString()!)
                
                
                if bill > 0 {
                    delegate?.updateCartItemCount()
                    
                }
                
            }
            
        } else {
            self.showOkAlert("Please select service type.")
            return
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func minusPressed(_ sender: Any) {
        
        if quantity > 1 {
            quantity -= 1
            self.countLabel.text = "\(quantity)"
            calculateBill()
        }
    }
    
    @IBAction func plusPressed(_ sender: Any) {
        quantity += 1
        self.countLabel.text = "\(quantity)"
        calculateBill()
    }
    
    @IBAction func crossPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - Private Methods
    
    private func calculateBill() {
        bill = 0
        
        for option in product.washingMethods {
            if option.isSelected {
                bill = bill + (quantity * option.price)
            }
        }
        priceLabel.text = "\(bill)"
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension AddItemViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        product.washingMethods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(ItemsTableViewCell.self, indexPath: indexPath)
        cell.configCell(option: product.washingMethods[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        product.washingMethods.forEach { (option) in
            option.isSelected = false
        }
        product.washingMethods[indexPath.row].isSelected = true
        calculateBill()
        tableView.reloadData()
    }
}
