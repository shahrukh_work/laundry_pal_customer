//
//  ItemsTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 13/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class ItemsTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var selectedButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    //MARK: - Variable

    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    //MARK: - Actions
    
    
    //MARK: - Methods
    func configCell(option: WashingMethod) {
        titleLabel.text = option.name
        priceLabel.text = "\(option.price)"
        
        if option.isSelected {
            selectedButton.setImage(#imageLiteral(resourceName: "item_selected"), for: .normal)
            
        } else {
            selectedButton.setImage(#imageLiteral(resourceName: "item_unselected"), for: .normal)
        }
    }
}
