//
//  NotificationTableViewCell.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/17/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    
    //MARK: - Outlets
    @IBOutlet weak var notificationTitleLabel: UILabel!
    @IBOutlet weak var notificationMessageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Public Methods
    func configure(data: NotificationEntity) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        
        let notificationDate = dateFormatter.date(from: data.createdAt)
        let hours = Calendar.current.dateComponents([.hour], from: Date(), to: notificationDate ?? Date())
    
        let dateFormat = DateFormatter()
        dateFormat.locale = Locale(identifier: "en_US_POSIX")
        dateFormat.dateFormat = "MMMM d"
        dateFormat.timeZone = TimeZone(abbreviation: "GMT")
        
        let date = dateFormat.string(from: notificationDate ?? Date())
        
        notificationTitleLabel.text = data.title
        notificationMessageLabel.text = data.message
        var hrs = hours.hour
        hrs = ((hrs! * 2) - hrs!) * -1 //to avoid negative value, multiply with -1
        if hrs ?? 0 >= 24 {
            
            timeLabel.text = "\(Int(floor(Double(Int(((hrs ?? 0) / 24)))))) days ago"
            
        } else {
            timeLabel.text = "\(hrs ?? 0) hr ago"
        }
        dateLabel.text = date
    }
    
}
