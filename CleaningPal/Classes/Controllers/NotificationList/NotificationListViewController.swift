//
//  NotificationListViewController.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/17/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class NotificationListViewController: UIViewController {

    
    //MARK: - Variables
    var notification = [NotificationEntity]()
    
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noNotificationView: UIView!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - View Setup
    func setupView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.cornerRadius = 20
        tableView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        noNotificationView.cornerRadius = 20
        loadNotifications()
    }
    
    
    //MARK: - Actions
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Private Methods
    private func loadNotifications() {
        Utility.showLoading()
        
        NotificationsList.getNotificationList { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    self.notification = data.notifications
                    self.tableView.reloadData()
                }
                
            } else {
                 Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
}


//MARK: - TableView Datasource & Delegate
extension NotificationListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        noNotificationView.isHidden = !notification.isEmpty
        return notification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(NotificationTableViewCell.self, indexPath: indexPath)
        cell.configure(data: notification[indexPath.row])
        return cell
    }
}
