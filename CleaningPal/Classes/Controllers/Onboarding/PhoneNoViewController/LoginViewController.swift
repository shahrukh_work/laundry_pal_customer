//
//  LoginViewController.swift
//  CleaningPal
//
//  Created by Work on 12/07/2019.
//  Copyright © 2019 Work. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import ObjectMapper
import AlamofireImage
import FirebaseAuth
import SwiftyGif
import AVFoundation
import AVKit

class LoginViewController: UIViewController, SelectCountryViewControllerDelegate, SwiftyGifDelegate {
    
    
    //MARK: - Variables
    var isComingFromViewDidLoad = false
    var player = AVPlayer()
    var playerLayer: AVPlayerLayer? = nil
    
    
    // MARK: - Outlets
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var nextButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var getStartedLabel: UILabel!
    @IBOutlet weak var phoneTextField: FloatingTextField!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var countryFlagImageView: UIImageView!
    @IBOutlet weak var bottomTappableView: UIView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    
    //MARK: - Outlets
    let logoAnimationView = LogoAnimationView()
    
    
    // MARK: - UIViewController Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if !showLoader {
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(playerItemDidReachEnd),
                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                   object: nil)
            
        }
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
        if showLoader == false{
            loadVideo()
            showLoader = true
        }
        setupView()
        
        if isComingFromViewDidLoad {
            isComingFromViewDidLoad = false
            
        } else {
            phoneTextField.becomeFirstResponder()
        }
        
    }
    
    
    // MARK: - View Setup
    func setupView() {
        phoneTextField.backgroundColor = .white
        isComingFromViewDidLoad = true
        countryFlagImageView.af_setImage(withURL: URL(string: AppUser.shared.country.image)!)
        countryCodeLabel.text = AppUser.shared.country.dial_code
        
        phoneTextField.inputAccessoryView = UIView()
        phoneTextField.addTarget(self, action: #selector(phoneNumberChanged), for: .editingChanged)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tapGesture.delegate = self
        bottomTappableView.addGestureRecognizer(tapGesture)
        nextButton.isUserInteractionEnabled = false
        self.navigationController?.isNavigationBarHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 2
        logoImageView.isUserInteractionEnabled = true
        logoImageView.addGestureRecognizer(tap)
        phoneTextField.delegate = self
    }
    
    func setupNavigationBarUI() {
        let backButton: UIButton = UIButton (type: UIButton.ButtonType.custom)
        backButton.setImage(#imageLiteral(resourceName: "back_arrow2"), for: .normal)
        backButton.tintColor = UIColor.black
        backButton.addTarget(self, action: #selector(self.animateViewToOrignalPosition(_:)), for: UIControl.Event.touchUpInside)
        backButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        let barButton = UIBarButtonItem(customView: backButton)
        navigationItem.leftBarButtonItem = barButton
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func loadVideo() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
        } catch { }

        let path = Bundle.main.path(forResource: "launch1", ofType:"mp4")

        player = AVPlayer(url: NSURL(fileURLWithPath: path!) as URL)
        playerLayer = AVPlayerLayer(player: player)

        playerLayer?.frame = self.view.frame
        playerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer?.zPosition = 1

        self.view.layer.addSublayer(playerLayer!)

        player.seek(to: CMTime.zero)
        player.play()
        
    }
    
    func gifDidStop(sender: UIImageView) {
        logoAnimationView.isHidden = true
    }
    
    
    // MARK: - Selectors
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            nextButtonBottomConstraint.constant = keyboardSize.height + 30
            self.view.layoutIfNeeded()
        }
        nextButton.isHidden = false
    }
    
    @objc func doubleTapped() {
        
        let alertController = UIAlertController(title: "Set Base URL", message: "", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Staging", style: UIAlertAction.Style.default) {
            UIAlertAction in
            APIRoutes.baseUrl = "https://cleaningpal-backend-staging.codesorbit.com"
        }
        
        let cancelAction = UIAlertAction(title: "Live", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            APIRoutes.baseUrl = "https://cleaningpal-backend.codesorbit.com"
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if (phoneTextField.text ?? "") == "" {
            nextButton.alpha = 0.5
            nextButton.isUserInteractionEnabled = false
            nextButton.isHidden = true
        }
    }
    
    @objc func animateViewToOrignalPosition(_ sender: UIButton) {
        phoneTextField.resignFirstResponder()
        navigationItem.leftBarButtonItem = nil
        self.navigationController?.isNavigationBarHidden = true
        
        bottomViewHeightConstraint.constant = 230
        phoneTextField.isUserInteractionEnabled = false
        countryButton.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.25) {
            self.nextButton.alpha = 0.0
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        bottomViewHeightConstraint.constant = (Utility.getScreenHeight() + 350) / 2
        phoneTextField.isUserInteractionEnabled = true
        countryButton.isUserInteractionEnabled = true
        self.navigationController?.isNavigationBarHidden = false
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
            self.getStartedLabel.text = "Enter your mobile number"
            self.bottomTappableView.isHidden = true
            self.setupNavigationBarUI()
            self.phoneTextField.becomeFirstResponder()
        }
    }
    
    @objc func phoneNumberChanged() {
        if (phoneTextField.text?.count ?? 0) > 0 {
            nextButton.isUserInteractionEnabled = true
            nextButton.alpha = 1
            
        } else {
            nextButton.isUserInteractionEnabled = false
            nextButton.alpha = 0.5
        }
        phoneTextField.floatTitleLabel(titleLabel: phoneNumberLabel)
    }
    
    @objc func playerItemDidReachEnd(notification: NSNotification) {
        playerLayer?.zPosition = -1
    }
    
    
    // MARK: - Actions
    @IBAction func skipPressed(_ sender: Any) {
        DataManager.shared.setSkipData(isSkip: true)
        Utility.setupHomeAsRootViewController()
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        
        if phoneTextField.text == "" {
            nextButton.shake()
            
        } else {
            
            if !Utility.isValidPhoneNumber(phoneTextField.text ?? "") {
                self.showOkAlert("Phone number you have entered seems to be invalid")
                return
            }
            
            let phoneNumber = "\(countryCodeLabel.text!)\(phoneTextField.text!)"
            Utility.showLoading()
            
            UserStatus.getCustomerStatus(phone: phoneNumber) { (data, error) in
                Utility.hideLoading()
                
                if error == nil {
                    
                    if let data = data {
                        
                        if data.status == 1 {
                            self.sendCode(phoneNumber: phoneNumber)
                            
                        } else {
                            self.showOkAlert("This user is not active.")
                        }
                    }
                    
                } else if error?.localizedDescription == "NO_USER_FOUND" {
                    self.sendCode(phoneNumber: phoneNumber)
                }
            }
        }
    }
    
    @IBAction func countryButtonTapped(_ sender: Any) {
        let selectCountryViewController = SelectCountryViewController()
        selectCountryViewController.delegate = self
        self.present(selectCountryViewController, animated: true, completion: nil)
    }
    
    
    // MARK: - SelectCountryViewControllerDelegate
    func didSelectCuontry(country: Country) {
        Utility.hideLoading()
        countryCodeLabel.text = country.dial_code
        AppUser.shared.country = country
        countryFlagImageView.af_setImage(withURL: URL(string: country.image)!)
    }
    
    
    //MARK: - Private Methods
    private func sendCode(phoneNumber: String) {
        Utility.showLoading()
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            
            Utility.hideLoading()
            
            if let error = error {
                self.showOkAlert(error.localizedDescription)
                return
            }
            
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            print("!!!! verification code sent successfully !!!!")
            
            let verifyCodeViewController = CodeVerificationViewController()
            AppUser.shared.phoneNumber = phoneNumber
            AppUser.shared.phoneWithoutCode = self.phoneTextField.text!
            self.navigationController?.pushViewController(verifyCodeViewController, animated: true)
        }
    }
}


//MARK: - UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == phoneTextField {
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            if newString.length > maxLength {
                return false
            }
            
        }
        return true
    }
}
