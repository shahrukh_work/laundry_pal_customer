//
//  CodeVerificationViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/23/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FirebaseAuth


class CodeVerificationViewController: UIViewController, UITextFieldDelegate {
    
    
    //MARK: - Variables
    var timer: Timer!
    
    
    //MARK: - Outlets
    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var textField2: UITextField!
    @IBOutlet weak var textField3: UITextField!
    @IBOutlet weak var textField4: UITextField!
    @IBOutlet weak var textField5: UITextField!
    @IBOutlet weak var textField6: UITextField!
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var nextButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var invalidCodeLabel: UILabel!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        
        textField1.delegate = self
        textField2.delegate = self
        textField3.delegate = self
        textField4.delegate = self
        textField5.delegate = self
        textField6.delegate = self
        
        textField2.isUserInteractionEnabled = false
        textField3.isUserInteractionEnabled = false
        textField4.isUserInteractionEnabled = false
        textField5.isUserInteractionEnabled = false
        textField6.isUserInteractionEnabled = false
        
        textField1.tintColor = .clear
        textField2.tintColor = .clear
        textField3.tintColor = .clear
        textField4.tintColor = .clear
        textField5.tintColor = .clear
        textField6.tintColor = .clear
        
        textField1.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField2.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField3.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField4.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField5.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField6.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        
        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        phoneNumberLabel.text = "Enter the 6-digit code sent to you at \(AppUser.shared.phoneNumber)"
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(resendTimerTick), userInfo: nil, repeats: true)
    }
    
    
    // MARK: - Selectors
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            nextButtonBottomConstraint.constant = keyboardSize.height + 10
            self.view.layoutIfNeeded()
        }
        verifyButton.isHidden = false
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        let check = Set([textField1.text ?? "", textField2.text ?? "", textField3.text ?? "", textField4.text ?? ""])
        
        if check.contains("") && check.count == 1 {
            verifyButton.isHidden = true
        }
    }
    
    @objc func resendTimerTick() {
        self.resendButton.isUserInteractionEnabled = true
        self.resendButton.alpha = 1
    }
    
    
    //MARK: - Actions
    @IBAction func verifyButtonTapped(_ sender: Any) {
        
        var verificationCode = textField1.text! + textField2.text! + textField3.text!
        verificationCode = verificationCode + textField4.text! + textField5.text! + textField6.text!
        
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")!
        let credential = PhoneAuthProvider.provider().credential(
        withVerificationID: verificationID,
        verificationCode: verificationCode)
        
        Utility.showLoading()
        Auth.auth().signIn(with: credential) { (authResult, error) in
            
            Utility.hideLoading()
            if let error = error {
                print(error.localizedDescription)
                self.verifyButton.isUserInteractionEnabled = false
                self.verifyButton.alpha = 0.5
                self.invalidCodeLabel.isHidden = false
                return
            }
            
            if let result = authResult {
                
                Utility.showLoading()
                result.user.getIDTokenForcingRefresh(true) { (token, error) in
                    
                    if error == nil {
                        
                        AuthData.login(phone: AppUser.shared.phoneNumber, token: token ?? "") { (data, error) in
                            Utility.hideLoading()
                            
                            if error == nil {
                                
                                if let data = data {
                                    
                                    if data.status == "ACTIVE" {
                                        Utility.setupHomeAsRootViewController()
                                        DataManager.shared.updateFCM()
                                        
                                    } else if data.status == "USER_DISABLED" {
                                        self.showOkAlert("Your account has been disabled by admin.")
                                        
                                    } else {
                                        let signUpVC = SignUpViewController()
                                        self.navigationController?.pushViewController(signUpVC, animated: true)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func editMobileNumberButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendCodeButtonTapped(_ sender: Any) {
        Utility.showLoading()
        
        PhoneAuthProvider.provider().verifyPhoneNumber(AppUser.shared.phoneNumber, uiDelegate: nil) { (verificationID, error) in
            
            Utility.hideLoading()
            
            if let error = error {
                print("Authentication error!", error.localizedDescription)
                return
            }
            
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            self.timer.invalidate()
            self.timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.resendTimerTick), userInfo: nil, repeats: true)
            self.resendButton.isUserInteractionEnabled = false
            self.resendButton.alpha = 0.5
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - UITextFieldDelegate
    @objc func textFieldDidChange(_ textField: UITextField) {
        let tag = textField.tag
        textField.backgroundColor = #colorLiteral(red: 0.3333333333, green: 0.7764705882, blue: 0.9294117647, alpha: 1)
        if tag == 1006 {
            
            var verificationCode = textField1.text! + textField2.text! + textField3.text!
            verificationCode = verificationCode + textField4.text! + textField5.text! + textField6.text!
            
            if verificationCode.count == 6 {
                verifyButton.isUserInteractionEnabled = true
                verifyButton.alpha = 1
                
            } else {
                verifyButton.isUserInteractionEnabled = false
                verifyButton.alpha = 0.5
            }
            
        } else {
            let text = textField.text!
            
            if text != "" {
                let lastChar = String(describing: Array(text)[text.count - 1])
                textField.text = lastChar + ""
                (self.view.viewWithTag(tag+1) as! UITextField).isUserInteractionEnabled = true
                (self.view.viewWithTag(tag+1) as! UITextField).becomeFirstResponder()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            textField.backgroundColor = #colorLiteral(red: 0.9098039216, green: 0.9098039216, blue: 0.9098039216, alpha: 1)
            verifyButton.isUserInteractionEnabled = false
            verifyButton.alpha = 0.5
            textField.isUserInteractionEnabled =  true
            textField.text = ""
            invalidCodeLabel.isHidden = true
            
            if textField.tag != 1001
            {
                (self.view.viewWithTag(textField.tag-1) as! UITextField).becomeFirstResponder()
                return false
            }
        }
        let bool = maxLengthForTextfield(textfield: textField, range: range, string: string, length: 1)
        return true && bool
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func maxLengthForTextfield(textfield: UITextField, range: NSRange, string: String, length: Int) -> Bool {
        let currentText = textfield.text ?? ""
        guard let strRange = Range(range, in: currentText) else { return false }
        let newString = currentText.replacingCharacters(in: strRange, with: string)
        
        return newString.count <= length
    }
}
