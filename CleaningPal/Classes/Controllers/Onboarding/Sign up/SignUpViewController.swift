//
//  SignUpViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/22/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class SignUpViewController: UIViewController, SelectCountryViewControllerDelegate {
    
    
    //MARK: - Outlet
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var countryFlagImageView: UIImageView!
    @IBOutlet weak var firstnameTextField: FloatingTextField!
    @IBOutlet weak var lastNameTextField: FloatingTextField!
    @IBOutlet weak var emailTextField: FloatingTextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var nextButtonBottomConstraint: NSLayoutConstraint!
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        countryFlagImageView.af_setImage(withURL: URL(string: AppUser.shared.country.image)!)
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enable = false
        setupView()
    }
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        firstnameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        lastNameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        emailTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        phoneTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        countryFlagImageView.af_setImage(withURL: URL(string: AppUser.shared.country.image)!)
        countryCodeLabel.text = AppUser.shared.country.dial_code
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        firstnameTextField.inputAccessoryView = UIView()
        lastNameTextField.inputAccessoryView = UIView()
        emailTextField.inputAccessoryView = UIView()
        phoneTextField.inputAccessoryView = UIView()
        
        phoneTextField.text = AppUser.shared.phoneWithoutCode
    }
    
    
    // MARK: - Selectors
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            nextButtonBottomConstraint.constant = keyboardSize.height
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        nextButtonBottomConstraint.constant = 35
        self.view.layoutIfNeeded()
    }
    
    @objc func textFieldDidChange(_ textField: FloatingTextField) {
        
        if textField == firstnameTextField {
            textField.floatTitleLabel(titleLabel: firstNameLabel)
            
        } else if textField == lastNameTextField {
            textField.floatTitleLabel(titleLabel: lastNameLabel)
            
        } else if textField == emailTextField {
            textField.floatTitleLabel(titleLabel: emailLabel)
            
        } else if textField == phoneTextField {
            textField.floatTitleLabel(titleLabel: phoneNumberLabel)
        }
    }
    
    
    //MARK: - Actions
    @IBAction func countryButtonTapped(_ sender: Any) {
        let selectCountryViewController = SelectCountryViewController()
        selectCountryViewController.delegate = self
        self.present(selectCountryViewController, animated: true, completion: nil)
    }
    
    @IBAction func nextPressed(_ sender: Any) {
        
        let check = [firstnameTextField.text ?? "", lastNameTextField.text ?? "", emailTextField.text ?? "", phoneTextField.text ?? ""]
        
        if check.contains("") {
            self.showOkAlert("Fill all the fields")
            return
        }
        
        if !Utility.isValidEmail(emailStr: emailTextField.text ?? "") {
            self.showOkAlert("Invalid Email")
            return
        }
        
        Utility.showLoading()
        AuthData.signup(firstName: check[0], lastName: check[1], phone: AppUser.shared.phoneNumber, email: check[2]) { (data, error) in
            
            Utility.hideLoading()
            if error == nil {
                Utility.setupHomeAsRootViewController()
                DataManager.shared.updateFCM()
            }
        }
    }
    
    
    // MARK: - SelectCountryViewControllerDelegate
    func didSelectCuontry(country: Country) {
        Utility.hideLoading()
        countryCodeLabel.text = country.dial_code
        AppUser.shared.country = country
        countryFlagImageView.af_setImage(withURL: URL(string: country.image)!)
    }
}
