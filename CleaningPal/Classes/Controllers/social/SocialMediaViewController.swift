//
//  SocialMediaViewController.swift
//  CleaningPal
//
//  Created by Mac on 21/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SocialMediaViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var copyButton: UIButton!
    

    //MARK: - Variable


    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let gradient = CAGradientLayer()
        gradient.frame = copyButton.bounds
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.colors = [#colorLiteral(red: 0.07843137255, green: 0.6705882353, blue: 0.9568627451, alpha: 1).cgColor, #colorLiteral(red: 0.3333333333, green: 0.7764705882, blue: 0.9294117647, alpha: 1).cgColor]
        copyButton.layer.insertSublayer(gradient, at: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    
    //MARK: - Actions
    @IBAction func copyPressed(_ sender: Any) {
    }
    
    @IBAction func crossPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func smsPressed(_ sender: Any) {
    }
    
    @IBAction func messengerPressed(_ sender: Any) {
    }
    
    @IBAction func whatsAppPressed(_ sender: Any) {
    }
    
    @IBAction func faceBookPressed(_ sender: Any) {
    }
    
    
    //MARK: - Methods
    
}
