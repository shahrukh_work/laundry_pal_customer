//
//  SelectAddressViewController.swift
//  CleaningPal
//
//  Created by Mac on 15/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import ObjectMapper

enum Day {
    case today
    case tomorrow
    case thirdDay
}


enum Time {
    case Nineto12
    case twelveTo3
    case threeTo6
}

class SelectDayTime {
    var day: Day = .tomorrow
    var time: Time = .twelveTo3
}

class SelectAddressViewController: UIViewController {
    
    
    //MARK: - Variable
    var sections = ["Select Address", "Type of Delivery", "Schedule Pickup","Payment Method"]
    var orderType: OrderTypes = .normal
    var paymentMethod: PaymentType = .cod //.none
    var promo = Mapper<PromoCode>().map(JSON: [:])!
    var location = Mapper<Location>().map(JSON: [:])!
    var isPromoApplied = false
    var isPromoVerified = true
    var isStarchAdded = false
    var starchDescription = ""
    var notesForRider = ""
    var promoCode = ""
    var deliveryType: DeliveryType = .standard
    var date = Date()
    var startTime = "09:00:00"
    var endTime = "12:00:00"
    var cartItems = Mapper<Cart>().map(JSON: [:])!
    var calBack: (() -> ())?
    var selectDayTime = SelectDayTime()
    
    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var confirmOrderView: UIView!
    @IBOutlet weak var totalItemLabel: UILabel!
    @IBOutlet weak var discountedPriceLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var gstLabel: UILabel!
    @IBOutlet weak var walletAdjustedLabel: UILabel!
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
         
    }
    
    
    //MARK: - Setup
    func setupView () {
        tableView.delegate = self
        tableView.dataSource = self
        confirmOrderView.cornerRadius = 16
        confirmOrderView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        if let location = DataManager.shared.getLocation() {
            self.location = location
        }
        cartItems = DataManager.shared.getCart()
        calculatePrice()
    }
    
    
    //MARK: - Actions
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmOrderPressed(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-d"
        
        if orderType == .easy {
            
            var params = [
                "pickup_address": location.locationName,
                "pickup_location": location.locationCoordinates,
                "pickup_date": dateFormatter.string(from: date),
                "available_start_time": startTime,
                "available_end_time": endTime,
                "delivery_type": deliveryType.rawValue,
                "description": notesForRider,
                "payment_method": paymentMethod == .cod ? "cash" : "card",
                "state": location.state
                
                ] as [String : AnyObject]
            
            if isPromoVerified && isPromoApplied {
                params["discount_id"] = promo.discountId as AnyObject
            }
            
            Utility.showLoading()
            OrderPlacement.placeEasyOrder(params: params) { (data, error) in
                Utility.hideLoading()
                
                if error == nil {
                    
                    if let data = data {
                        
                        if data.order.orderId != -1 {
                            let reciptVC = ReciptViewController()
                            reciptVC.deliveryType = self.deliveryType
                            reciptVC.paymentMethod = self.paymentMethod
                            reciptVC.delegate = self
                            reciptVC.data = data.order
                            reciptVC.modalPresentationStyle = .overCurrentContext
                            self.present(reciptVC, animated: true, completion: nil)
                        }
                    }
                    
                } else if error?.localizedDescription == "ORDER_ALREADY_EXISTS" {
                    self.showOkAlert("You already have an active order.")
                }
            }
            
            return
        }
        
        var params = [
            "pickup_address": location.locationName,
            "pickup_location": location.locationCoordinates,
            "pickup_date": dateFormatter.string(from: date),
            "available_start_time": startTime,
            "available_end_time": endTime,
            "delivery_type": deliveryType.rawValue,
            "description": notesForRider,
            "starch_status": isStarchAdded ? 1: 0,
            "starch_description": starchDescription,
            "payment_method": paymentMethod == .cod ? "cash" : "card",
            "state": location.state,
            "city": location.city,
            "items": cartItems.getJSON()["items"] as AnyObject
            
            ] as [String: AnyObject]
        
        if isPromoVerified && isPromoApplied {
            params["promo_code"] = promoCode as AnyObject
        }
        
        Utility.showLoading()
        OrderPlacement.placeOrder(params: params) { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    
                    if data.order.orderId != -1 {
                        let reciptVC = ReciptViewController()
                        reciptVC.deliveryType = self.deliveryType
                        reciptVC.paymentMethod = self.paymentMethod
                        reciptVC.delegate = self
                        reciptVC.data = data.order
                        reciptVC.modalPresentationStyle = .overCurrentContext
                        self.present(reciptVC, animated: true, completion: nil)
                    }
                }
                
            } else if error?.localizedDescription == "ORDER_ALREADY_EXISTS" {
                self.showOkAlert("You already have an active order.")
            }
        }
    }
    
    @IBAction func addMorePressed(_ sender: Any) {
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popViewController(animated: false)
    }
    
    
    //MARK: - Private Methods
    private func applyPromo(promoCode: String) {
        Utility.showLoading()
        self.promoCode = promoCode
        
        if orderType == .normal {
            calculatePrice(promo: promoCode, applyPromo: true)
            return
        }
        
        PromoCode.verifyPromoCode(promoCode: promoCode, city: location.city) { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    self.promo = data
                    
                    if data.discountId  == -1 {
                        self.isPromoApplied = false
                        self.isPromoVerified = false
                        self.tableView.reloadData()
                        
                    } else {
                        self.isPromoApplied = true
                        self.isPromoVerified = true
                        self.tableView.reloadData()
                    }
                }
                
            } else {
                self.isPromoApplied = false
                self.isPromoVerified = false
                //self.tableView.reloadData()
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
    
    
    private func calculatePrice(promo: String = "", applyPromo: Bool = false) {
        
        var params: [String: AnyObject] = [:]
        
        
        
        if orderType == .normal {
            params = [
                "state" : location.state,
                "city": location.city,
                "order_type" : deliveryType.rawValue,
                "items": cartItems.getJSON()["items"] as AnyObject
                
            ] as [String : AnyObject]
            
            if applyPromo {
                params["promo_code"] = promo as AnyObject
            }
            Utility.showLoading()
            
            PriceCalculation.calculatePrice(params: params) { (data, error) in
                Utility.hideLoading()
                
                if error == nil {
                    
                    if let data = data {
                        self.gstLabel.text = "\(Int(Double(data.gst) ?? 0))"
                        self.discountedPriceLabel.text = "\(data.discountedPrice)"
                        self.totalPriceLabel.text = "\(data.totalPrice)"
                        self.totalItemLabel.text = "\(data.totalCount)"
                        
                        if data.walletAmount == 0 {
                            self.walletAdjustedLabel.text = "0"
                            
                        } else if data.walletAmount < 0 {
                            self.walletAdjustedLabel.text = "+\(-data.walletAmount)"
                            
                        } else {
                            self.walletAdjustedLabel.text = "-\(data.walletAmount)"
                        }
                        
                        switch data.promo {
                        case "NO_PROMO_APPLIED":
                            self.isPromoVerified = true
                            self.isPromoApplied = false
                            
                        case "PROMO_APPLIED":
                            self.isPromoVerified = true
                            self.isPromoApplied = true
                            
                        case "PROMO_INVALID":
                            self.isPromoVerified = false
                            self.isPromoApplied = false
                            
                        default:
                            break
                        }
                        
                        self.tableView.reloadData()
                    }
                } else {
                    Utility.showAlertController(self, error?.localizedDescription ?? "")
                }
            }
            
        } else {
            params = [
                "state" : location.state,
                "city": location.city,
                "order_type" : deliveryType.rawValue,
                "items": [] as AnyObject] as [String : AnyObject]
            
            if applyPromo {
                params["promo_code"] = promo as AnyObject
            }
        }
        
        
        
        
        //        } else {
        //            gstLabel.text = "0"
        //            discountedPriceLabel.text = "0"
        //            totalPriceLabel.text = "0"
        //            totalItemLabel.text = "0"
        //            walletAdjustedLabel.text = "0"
        //
        //
        //        }
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension SelectAddressViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if orderType == .easy {
            return 5
        }
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.register(AddressTableViewCell.self, indexPath: indexPath)
            cell.configure(data: location, message: notesForRider)
            cell.delegate = self
            return cell
        }
        
        if indexPath.section == 1 {
            let cell = tableView.register(TypeOfDeliveryTableViewCell.self, indexPath: indexPath)
            cell.configure(data: deliveryType)
            cell.delegate = self
            return cell
        }
        
        if indexPath.section == 2 {
            let cell = tableView.register(SchedulePickupTableViewCell.self, indexPath: indexPath)
            cell.delegate = self
            cell.selectDayTime = selectDayTime
            cell.updateUI()
            return cell
            
        } else if indexPath.section == 3 {
             let cell = tableView.register(SelectCreditOrDebitTableViewCell.self, indexPath: indexPath)
             cell.config(data: paymentMethod)
             return cell
             
        } else if indexPath.section == 4 {
            let cell = tableView.register(PromoCodeTableViewCell.self, indexPath: indexPath)
            cell.configure(promoCode: promoCode, isApplied: isPromoApplied, isVerified: isPromoVerified)
            cell.delegate = self
            return cell
        }
        
        let cell = tableView.register(StarchTableViewCell.self, indexPath: indexPath)
        cell.configure(isStarchAdded: isStarchAdded, message: starchDescription)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 3 {
            let payment = PaymentMethodViewController()
            payment.paymentMethod = paymentMethod
            payment.delegate = self
            self.navigationController?.pushViewController(payment, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 35))
        
        if section == 4 || section == 5 {
            return nil
        }
        
        headerView.backgroundColor = .white
        let label = UILabel()
        label.frame = CGRect.init(x: 24, y: 5, width: headerView.frame.width-10, height: headerView.frame.height - 5)
        label.text = sections[section]
        label.font = UIFont(name: "Lato-Bold", size: 18)
        label.textColor = UIColor.black
        label.backgroundColor = .white
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 4 || section == 5 {
            return 0
        }
        return 35
    }
}


//MARK: - PaymentMethodViewControllerDelegate
extension SelectAddressViewController: PaymentMethodViewControllerDelegate {
    
    func paymentMethodSelected(method: PaymentType) {
        paymentMethod = method
        tableView.reloadData()
    }
}


//MARK: - AddressTableViewCellDelegate
extension SelectAddressViewController: AddressTableViewCellDelegate {
    
    func addAddressTapped() {
        let autocompleteController = LocationPickerViewController()
        autocompleteController.delegate = self
        self.present(autocompleteController, animated: true, completion: nil)
    }
    
    func notesChanged(text: String) {
        notesForRider = text
    }
}


//MARK: - LocationPickerViewControllerDelegate
extension SelectAddressViewController: LocationPickerViewControllerDelegate {
    
    func didSelectLocation(locationAddress: String, coordinates: CLLocationCoordinate2D, city: String, state: String) {
        
        let location = Mapper<Location>().map(JSON: [
            "location_name": locationAddress,
            "location_coordinates": "\(coordinates.latitude),\(coordinates.longitude)",
            "loc_city": city,
            "loc_state": state
        ])
        
        DataManager.shared.setLocation(location: location?.toJSONString() ?? "")
        
        if let location = location {
            self.location = location
        }
        promoCode = ""
        isPromoApplied = false
        isPromoVerified = true
        calculatePrice()
        tableView.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
}


//MARK: - PromoCodeTableViewCellDelegate
extension SelectAddressViewController: PromoCodeTableViewCellDelegate {
    
    func verifyTapped(promoCode: String) {
        applyPromo(promoCode: promoCode)
    }
}


//MARK: - TypeOfDeliveryTableViewCellDelegate
extension SelectAddressViewController: TypeOfDeliveryTableViewCellDelegate {
    
    func deliveryTypeChange(deliveryType: DeliveryType) {
        self.deliveryType = deliveryType
        
        if isPromoVerified && isPromoApplied {
            calculatePrice(promo: promoCode, applyPromo: true)
            
        } else {
            promoCode = ""
            calculatePrice()
        }
        
        tableView.reloadData()
    }
}


//MARK: - SchedulePickupTableViewCellDelegate
extension SelectAddressViewController: SchedulePickupTableViewCellDelegate {
    
    func dateSelected(date: Date) {
        self.date = date
    }
    
    func timeChanged(startTime: String, endTime: String) {
        self.startTime = startTime
        self.endTime = endTime
    }
}


//MARK: - StarchTableViewCellDelegate
extension SelectAddressViewController: StarchTableViewCellDelegate {
    
    func descriptionTextChanged(text: String) {
        starchDescription = text
    }
    
    func starchSelectionChanged() {
        isStarchAdded.toggle()
        tableView.reloadData()
        
        if tableView.numberOfRows(inSection: tableView.numberOfSections - 1) > 0 {
            tableView.scrollToRow(at: IndexPath(row: tableView.numberOfRows(inSection: tableView.numberOfSections - 1) - 1, section: tableView.numberOfSections - 1), at: .bottom, animated: true)
        }
    }
}


//MARK: - ReciptViewControllerDelegate
extension SelectAddressViewController: ReciptViewControllerDelegate {
    
    func crossPressed() {
        
        if orderType == .easy {
            self.navigationController?.popViewController(animated: true)
            return
        }
        DataManager.shared.setCart(cart: "")
        let cartItems = DataManager.shared.getCart()
        AppUser.shared.cartItemsCount = cartItems.cartItems.count
        tabBarController?.tabBar.items?[2].badgeValue = "\(AppUser.shared.cartItemsCount)"
        
        calBack?()
        self.navigationController?.popViewController(animated: true)
        
    }
}
