//
//  AddressTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 15/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell, UITextViewDelegate {

    
    //MARK: - Variable
    weak var delegate: AddressTableViewCellDelegate?
    
    
    //MARK: - Outlet
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var noteTextView: UITextView!


    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView ()
    }
    
    
    //MARK: - Setup
    func setupView () {
        noteTextView.delegate = self
        noteTextView.text = "House No. , Street No. and Sector OR Note for the Rider"
        noteTextView.textColor = UIColor.lightGray
    }
    
    
    //MARK: - Actions
    @IBAction func addAddressPressed(_ sender: Any) {
        delegate?.addAddressTapped()
    }
 
    @IBAction func currentLocationPressed(_ sender: Any) {
    }
    
    
    //MARK: - UITextViewDelegate
    func textViewDidBeginEditing(_ textView: UITextView) {

        if noteTextView.textColor == UIColor.lightGray {
            noteTextView.text = ""
            noteTextView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {

        if noteTextView.text == "" {
            noteTextView.text = "House No. , Street No. and Sector OR Note for the Rider"
            noteTextView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        delegate?.notesChanged(text: textView.text)
    }
    
    func configure(data: Location, message: String) {
        addressLabel.text = data.locationName
        
        if message == "" {
            noteTextView.text = "House No. , Street No. and Sector OR Note for the Rider"
            return
        }
        
        noteTextView.text = message
    }
}
