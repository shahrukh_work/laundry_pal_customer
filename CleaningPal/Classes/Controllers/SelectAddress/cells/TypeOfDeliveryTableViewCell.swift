//
//  TypeOfDeliveryTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 15/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class TypeOfDeliveryTableViewCell: UITableViewCell {

    
    //MARK: - Variable
    weak var delegate: TypeOfDeliveryTableViewCellDelegate?
    
    
    //MARK: - Outlet
    @IBOutlet weak var urgentButton: UIButton!
    @IBOutlet weak var normalButton: UIButton!
    @IBOutlet weak var urgentView: UIView!
    @IBOutlet weak var normalView: UIView!
    
    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    //MARK: - Setup
    func setupView () {
        
    }
    
    //MARK: - Actions
    @IBAction func urgentPressed(_ sender: Any) {
        delegate?.deliveryTypeChange(deliveryType: .express)
    }
    
    @IBAction func normalPressed(_ sender: Any) {
        delegate?.deliveryTypeChange(deliveryType: .standard)
    }
    
    
    //MARK: - Public Methods
    func configure(data: DeliveryType) {
        updateUI(type: data)
    }
    
    
    //MARK: - Private Methods
    private func updateUI(type: DeliveryType) {
        
        if type == .express {
            urgentButton.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
            normalButton.setImage(#imageLiteral(resourceName: "tick_unselected"), for: .normal)
            
            normalView.shadowOpacity = 0.0
            urgentView.backgroundColor = .white
            normalView.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
            
            urgentView.layer.shadowColor = UIColor.black.cgColor
            urgentView.layer.shadowOpacity = 0.1
            urgentView.layer.shadowOffset = CGSize.zero
            urgentView.layer.shadowRadius = 7
            
        } else {
            normalButton.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
            urgentButton.setImage(#imageLiteral(resourceName: "tick_unselected"), for: .normal)
            
            urgentView.shadowOpacity = 0.0
            normalView.backgroundColor = .white
            urgentView.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
            
            normalView.layer.shadowColor = UIColor.black.cgColor
            normalView.layer.shadowOpacity = 0.1
            normalView.layer.shadowOffset = CGSize.zero
            normalView.layer.shadowRadius = 7
        }
    }
}
