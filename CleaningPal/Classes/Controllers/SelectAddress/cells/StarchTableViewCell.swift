//
//  StarchTableViewCell.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/3/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class StarchTableViewCell: UITableViewCell {
    
    //MARK: - Variable
    weak var delegate: StarchTableViewCellDelegate?
    
    
    //MARK: - Outlets
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var starchButton: UIButton!
    
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    
    //MARK: - Setup
    func setupView () {
        descriptionTextView.delegate = self
        descriptionTextView.text = "Description"
        descriptionTextView.textColor = UIColor.lightGray
    }
    
    
    //MARK: - Actions
    @IBAction func starchButtonPressed(_ sender: Any) {
        delegate?.starchSelectionChanged()
    }
    
    
    //MARK: - Public Method
    public func configure(isStarchAdded: Bool, message: String) {
        
        if isStarchAdded {
            starchButton.setImage(#imageLiteral(resourceName: "circle_filled"), for: .normal)
            descriptionView.isHidden = !isStarchAdded
            
        } else {
            starchButton.setImage(#imageLiteral(resourceName: "circle_unfilled"), for: .normal)
            descriptionView.isHidden = !isStarchAdded
        }
        
        if message == "" {
            descriptionTextView.text = "Description"
            return
        }
        
        descriptionTextView.text = message
    }
}


//MARK: - UITextViewDelegate
extension StarchTableViewCell: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if descriptionTextView.textColor == UIColor.lightGray {
            descriptionTextView.text = ""
            descriptionTextView.textColor = UIColor.black
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        delegate?.descriptionTextChanged(text: textView.text)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if descriptionTextView.text == "" {
            descriptionTextView.text = "Description"
            descriptionTextView.textColor = UIColor.lightGray
        }
    }
}
