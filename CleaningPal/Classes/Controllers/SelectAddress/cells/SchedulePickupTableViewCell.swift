//
//  SchedulePickupTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 15/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SchedulePickupTableViewCell: UITableViewCell {
    
    
    //MARK: - Variables
    var today = Date()
    var tomorrow = Date()
    var thirdDay = Date()
    weak var delegate: SchedulePickupTableViewCellDelegate?
    
    
    //MARK: - Outlet
    @IBOutlet weak var threeTo6Label: UILabel!
    @IBOutlet weak var threeTo6View: UIView!
    @IBOutlet weak var twelveTo3Label: UILabel!
    @IBOutlet weak var twelveTo3View: UIView!
    @IBOutlet weak var nineTo12Label: UILabel!
    @IBOutlet weak var nineTo12View: UIView!
    @IBOutlet weak var thirdDayLabel: UILabel!
    @IBOutlet weak var tomorrowLabel: UILabel!
    @IBOutlet weak var todayLabel: UILabel!
    @IBOutlet weak var todayDateLabel: UILabel!
    @IBOutlet weak var tomorrowDateLabel: UILabel!
    @IBOutlet weak var thirdDayDateLabel: UILabel!
    @IBOutlet weak var todayView: UIView!
    @IBOutlet weak var tomorrowView: UIView!
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var todayButton: UIButton!
    @IBOutlet weak var nineTwelveButton: UIButton!
    @IBOutlet weak var twelveThreeButton: UIButton!
    
    
    //MARK: - Variables
    var selectDayTime = SelectDayTime()
    
    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView (selectDay: selectDayTime)
    }
    
    
    //MARK: - Setup
    func setupView (selectDay: SelectDayTime) {
        let calendar = Calendar(identifier: .gregorian)
        var dayFutureComponents = DateComponents()
        dayFutureComponents.day = 1
        tomorrow = calendar.date(byAdding: dayFutureComponents, to: today) ?? Date()
        thirdDay = calendar.date(byAdding: dayFutureComponents, to: tomorrow) ?? Date()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMMM YY"
        
        todayDateLabel.text = dateFormatter.string(from: today)
        tomorrowDateLabel.text = dateFormatter.string(from: tomorrow)
        thirdDayDateLabel.text = dateFormatter.string(from: thirdDay)
        //selectDayTime = selectDay
        todayPressed("")
       // updateUI()
    }
    
    //MARK: - Actions
    @IBAction func todayPressed(_ sender: Any) {
        selectDayTime.day = .today
        setColorOfDay(selectedView: todayView, selectedLabel: todayLabel, selectedDate: todayDateLabel)
        delegate?.dateSelected(date: today)
        nineTwelveButton.isUserInteractionEnabled = true
        twelveThreeButton.isUserInteractionEnabled = true
        updateUI()
    }
    
    @IBAction func tomorrowPressed(_ sender: Any) {
        selectDayTime.day = .tomorrow
        setColorOfDay(selectedView: tomorrowView, selectedLabel: tomorrowLabel, selectedDate: tomorrowDateLabel)
        delegate?.dateSelected(date: tomorrow)
        nineTwelveButton.isUserInteractionEnabled = true
        twelveThreeButton.isUserInteractionEnabled = true
    }
    
    @IBAction func thirdDayPressed(_ sender: Any) {
        selectDayTime.day = .thirdDay
        setColorOfDay(selectedView: thirdView, selectedLabel: thirdDayLabel, selectedDate: thirdDayDateLabel)
        delegate?.dateSelected(date: thirdDay)
        nineTwelveButton.isUserInteractionEnabled = true
        twelveThreeButton.isUserInteractionEnabled = true
    }
    
    @IBAction func ninePressed(_ sender: Any) {
        selectDayTime.time = .Nineto12
        setColorOfTime(selectedView: nineTo12View, selectedLabel: nineTo12Label)
        delegate?.timeChanged(startTime: "09:00:00", endTime: "12:00:00")
    }
    
    @IBAction func twelvePressed(_ sender: Any) {
        selectDayTime.time = .twelveTo3
        setColorOfTime(selectedView: twelveTo3View, selectedLabel: twelveTo3Label)
        delegate?.timeChanged(startTime: "12:00:00", endTime: "15:00:00")
    }
    
    @IBAction func threePressed(_ sender: Any) {
        selectDayTime.time = .threeTo6
        setColorOfTime(selectedView: threeTo6View, selectedLabel: threeTo6Label)
        delegate?.timeChanged(startTime: "15:00:00", endTime: "18:00:00")
    }
    
    //MARK: - Public Methods
    func updateUI() {
        
        nineTwelveButton.isUserInteractionEnabled = true
        twelveThreeButton.isUserInteractionEnabled = true
        todayButton.isUserInteractionEnabled = true
        
        let comp = Calendar.current.dateComponents([.hour], from: Date())
        let currentTime = comp.hour ?? 0
        
        if selectDayTime.day == .today {
            
            if currentTime < 18 {
                
                if currentTime < 12 {
                   // selectDayTime.time = .Nineto12
                    setColorOfTime(selectedView: nineTo12View, selectedLabel: nineTo12Label)
                    delegate?.timeChanged(startTime: "09:00:00", endTime: "12:00:00")
                    
                } else if currentTime < 15 {
                   // selectDayTime.time = .twelveTo3
                    nineTwelveButton.isUserInteractionEnabled = false
                    setColorOfTime(selectedView: twelveTo3View, selectedLabel: twelveTo3Label)
                    delegate?.timeChanged(startTime: "12:00:00", endTime: "15:00:00")
                    
                } else {
                   // selectDayTime.time = .threeTo6
                    nineTwelveButton.isUserInteractionEnabled = false
                    twelveThreeButton.isUserInteractionEnabled = false
                    setColorOfTime(selectedView: threeTo6View, selectedLabel: threeTo6Label)
                    delegate?.timeChanged(startTime: "15:00:00", endTime: "18:00:00")
                }
                
            } else {
                todayButton.isUserInteractionEnabled = false
                delegate?.dateSelected(date: tomorrow)
                selectDayTime.day = .tomorrow
                selectDayTime.time = .Nineto12
                setColorOfDay(selectedView: tomorrowView, selectedLabel: tomorrowLabel, selectedDate: tomorrowDateLabel)
                setColorOfTime(selectedView: nineTo12View, selectedLabel: nineTo12Label)
                delegate?.timeChanged(startTime: "09:00:00", endTime: "12:00:00")
            }
        } else {
            
        }
    }
    
    
    //MARK: - Private Methods
    private func setColorOfTime (selectedView: UIView, selectedLabel: UILabel) {
        nineTo12View.backgroundColor = #colorLiteral(red: 0.9719446301, green: 0.9719673991, blue: 0.9719551206, alpha: 1)
        twelveTo3View.backgroundColor = #colorLiteral(red: 0.9719446301, green: 0.9719673991, blue: 0.9719551206, alpha: 1)
        threeTo6View.backgroundColor = #colorLiteral(red: 0.9719446301, green: 0.9719673991, blue: 0.9719551206, alpha: 1)
        
        nineTo12Label.textColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        twelveTo3Label.textColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        threeTo6Label.textColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        timeCheck()
    }
    
    private func setColorOfDay (selectedView: UIView, selectedLabel: UILabel, selectedDate: UILabel) {
        todayView.backgroundColor = #colorLiteral(red: 0.9719446301, green: 0.9719673991, blue: 0.9719551206, alpha: 1)
        tomorrowView.backgroundColor = #colorLiteral(red: 0.9719446301, green: 0.9719673991, blue: 0.9719551206, alpha: 1)
        thirdView.backgroundColor = #colorLiteral(red: 0.9719446301, green: 0.9719673991, blue: 0.9719551206, alpha: 1)
        
        todayLabel.textColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        tomorrowLabel.textColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        thirdDayLabel.textColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        
        todayDateLabel.textColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 0.72)
        tomorrowDateLabel.textColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 0.72)
        thirdDayDateLabel.textColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 0.72)
        
        if selectDayTime.day == .today {
            todayView.backgroundColor = #colorLiteral(red: 0, green: 0.6579127908, blue: 0.9000582099, alpha: 1)
            todayLabel.textColor = #colorLiteral(red: 1, green: 0.9819169641, blue: 0.9875573516, alpha: 1)
            todayDateLabel.textColor = .white
            return
        }
        
        if selectDayTime.day == .tomorrow {
            tomorrowView.backgroundColor = #colorLiteral(red: 0, green: 0.6579127908, blue: 0.9000582099, alpha: 1)
            tomorrowLabel.textColor = #colorLiteral(red: 1, green: 0.9819169641, blue: 0.9875573516, alpha: 1)
            tomorrowDateLabel.textColor = .white
            return
        }
        
        if selectDayTime.day == .thirdDay {
            thirdView.backgroundColor = #colorLiteral(red: 0, green: 0.6579127908, blue: 0.9000582099, alpha: 1)
            thirdDayLabel.textColor = #colorLiteral(red: 1, green: 0.9819169641, blue: 0.9875573516, alpha: 1)
            thirdDayDateLabel.textColor = .white
            return
        }
    }
    
    func timeCheck () {
        
        if selectDayTime.time == .Nineto12 {
            nineTo12View.backgroundColor = #colorLiteral(red: 0, green: 0.6579127908, blue: 0.9000582099, alpha: 1)
            nineTo12Label.textColor = #colorLiteral(red: 1, green: 0.9819169641, blue: 0.9875573516, alpha: 1)
            return
        }
        
        if selectDayTime.time == .twelveTo3 {
            twelveTo3View.backgroundColor = #colorLiteral(red: 0, green: 0.6579127908, blue: 0.9000582099, alpha: 1)
            twelveTo3Label.textColor = #colorLiteral(red: 1, green: 0.9819169641, blue: 0.9875573516, alpha: 1)
            return
        }
        
        if selectDayTime.time == .threeTo6 {
            threeTo6View.backgroundColor = #colorLiteral(red: 0, green: 0.6579127908, blue: 0.9000582099, alpha: 1)
            threeTo6Label.textColor = #colorLiteral(red: 1, green: 0.9819169641, blue: 0.9875573516, alpha: 1)
            return
        }
    }
}
