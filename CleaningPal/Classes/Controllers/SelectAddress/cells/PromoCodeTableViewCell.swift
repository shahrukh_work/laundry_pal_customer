//
//  PromoCodeTableViewCell.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/3/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class PromoCodeTableViewCell: UITableViewCell {

    
    //MARK: - Delegate
    weak var delegate: PromoCodeTableViewCellDelegate?
    
    
    //MARK: - Outlets
    @IBOutlet weak var promoCodeLabel: UILabel!
    @IBOutlet weak var promoCodeTextField: FloatingTextField!
    @IBOutlet weak var tickImageView: UIImageView!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    
    //MARK: - View Setup
    func setupView() {
        let gestureRecon = UITapGestureRecognizer(target: self, action: #selector(tickTapped))
        tickImageView.isUserInteractionEnabled = true
        tickImageView.addGestureRecognizer(gestureRecon)
    }
    
    
    //MARK: - Selectors
    @objc func tickTapped() {
        delegate?.verifyTapped(promoCode: promoCodeTextField.text ?? "")
    }
    
    
    //MARK: - Actions
    @IBAction func promoCodeChange(_ sender: Any) {
        promoCodeTextField.floatTitleLabel(titleLabel: promoCodeLabel)
    }
    
    
    //MARK: - Public Methods
    func configure(promoCode: String, isApplied: Bool, isVerified: Bool) {
        
        promoCodeTextField.text = promoCode
        
        if isApplied {
            tickImageView.image = #imageLiteral(resourceName: "tick")
            
        } else {
            tickImageView.image = #imageLiteral(resourceName: "tick_unselected")
        }
        
        if !isVerified {
            tickImageView.image = #imageLiteral(resourceName: "tickred")
            borderView.borderColor = #colorLiteral(red: 0.9254901961, green: 0, blue: 0.1058823529, alpha: 1)
            errorLabel.isHidden = false
            return
        }
        
        borderView.borderColor = #colorLiteral(red: 0, green: 0.6233851314, blue: 1, alpha: 1)
        errorLabel.isHidden = true
    }
}
