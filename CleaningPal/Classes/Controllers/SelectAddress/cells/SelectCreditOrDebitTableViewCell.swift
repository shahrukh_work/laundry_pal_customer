//
//  SelectCreditOrDebitTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 15/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SelectCreditOrDebitTableViewCell: UITableViewCell {

    
    //MARK: - Outlets
    @IBOutlet weak var paymentTypeLabel: UILabel!
    
    
    //MARK: - View  Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Public Methods
    func config(data: PaymentType) {
        paymentTypeLabel.text = data.rawValue
    }
}
