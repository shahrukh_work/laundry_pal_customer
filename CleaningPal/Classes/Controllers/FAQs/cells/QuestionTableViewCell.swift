//
//  QuestionTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 21/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ExpandableLabel

protocol FAQDelegate: AnyObject {
    func showDescription (indexPath: IndexPath)
}

class QuestionTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    

    //MARK: - Variable
    var indexPath: IndexPath?
    weak var delegate: FAQDelegate?

    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - configure
    func configure (faq: FAQ) {
        titleLabel.text = faq.title
        
        if !faq.isCollapsed {
            descriptionLabel.text = faq.description
            arrowImageView.image = #imageLiteral(resourceName: "blueArrowUp")
            titleLabel.textColor = #colorLiteral(red: 0.1607843137, green: 0.5019607843, blue: 0.7254901961, alpha: 1)
            titleLabel.font = UIFont(name: "Lato Medium", size: 13) ?? UIFont.systemFont(ofSize: 13, weight: .medium)
            
        } else {
            descriptionLabel.text = ""
            arrowImageView.image = #imageLiteral(resourceName: "downArrow")
            titleLabel.textColor = #colorLiteral(red: 0.137254902, green: 0.1215686275, blue: 0.1254901961, alpha: 1)
            titleLabel.font = UIFont(name: "Lato Regular", size: 15) ?? UIFont.systemFont(ofSize: 15, weight: .medium)
        }
        
        self.layoutIfNeeded()
    }
    
    
    //MARK: - Actions
    @IBAction func collapsePressed(_ sender: Any) {
        delegate?.showDescription(indexPath: indexPath!)
    }
    
    
    //MARK: - Methods
}
