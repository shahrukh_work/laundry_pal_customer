//
//  FAQViewController.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 6/29/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class FAQViewController: UIViewController {

    
    //MARK: - Variable
    var data: [FAQ] = []
    
    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    

    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - Setup
    func setupView () {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.cornerRadius = 20
        tableView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        dataSource ()
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        data.append(FAQ(title: "In Which Areas Do You Operate?", description: "Selected Sectors in Islamabad only."))
        data.append(FAQ(title: "How Can I Place An Order?", description: "You can place an order via our LaundryPal app (available on Play store and App store) in three easy steps: \n \u{2022} Provide your contact details and address. \n \u{2022} Select the day and your chosen  time slot for collection and delivery. \n \u{2022} Confirm the order."))

         data.append(FAQ(title: "What Is The Easy Order?", description: "For our customer’s convenience, it’s an easy way to place an instant order. Just Click ‘Easy Order’ button on the LaundryPal App and our rider will be at your door in a few hours to receive your laundry and insert the details of your laundry. The minimum price for easy order is PKR 2000."))
         data.append(FAQ(title: "Where’s The Receipt For My Laundry?", description: "You can download your receipt after completing the order on our LaundryPal app. You can also use the invoice we send you via email as a receipt."))
        
        
        data.append(FAQ(title: "How Does The Whole Process Work?", description: "\n \u{2022} You place an order through the LaundryPal App.\n \u{2022} Our drivers come to collect your items on the date and time slot you chose.\n \u{2022} You tell the driver the service you require or any special instructions you may have.\n \u{2022} Our dry cleaning experts process the required service.\n \u{2022} Our drivers return the items back on the specified date and time slot."))
        
        
        data.append(FAQ(title: "I Placed My Order By Mistake, How Do I Cancel It?", description: "Please contact our Customer Service Team to cancel an order.\nThrough LaundryPal App Once you place and confirm your laundry order, it cannot be canceled.\nHence, we suggest planning prior to placing your order on LaundryPal. Cancellation charges will be PKR 200. "))
         data.append(FAQ(title: "Do I Have To Be Home When You Come By?", description: "Yes. Any authorized person can handover clothes for processing to LaundryPal Agent."))
         data.append(FAQ(title: "What If I Missed A Scheduled Delivery?", description: "Each first time delivery attempt is free of cost for our customers. However, for every next time we deliver the laundry, an additional amount of PKR 200 will be charged from the customer."))
         data.append(FAQ(title: "Can I Change My Delivery Address From The Pickup Address?", description: "No! You cannot change the delivery address from the pickup address.  Your laundry will be delivered to same address. Kindly mention the right address before placing your order."))
        
        
        data.append(FAQ(title: "How Do I Select My Address?", description: "\n \u{2022} Write down your complete address in the allocated box for address.\n \u{2022} Then press ‘the add new address’ button and place the pin on your accurate location as per Google maps.\n \u{2022} You tell the driver the service you require or any special instructions you may have.\n \u{2022} Press ‘use this location’ button, so that accurate address and coordinates are inserted.\n \u{2022} Please recheck the accuracy of provided address before confirming the order."))
        
        
        
         data.append(FAQ(title: "How Long Does The Cleaning Take?", description: "We offer two types of delivery packages:\n \u{2022} Express Delivery Package- After picking your laundry we deliver within 48 hours.\n \u{2022} Standard Delivery Package- After picking your laundry we deliver within 72 hours."))
        
        
         data.append(FAQ(title: "How Can I Schedule My Order Pickup?", description: "You can easily schedule your laundry for today, tomorrow, and day after tomorrow in three different time slots of 9AM-12PM, 12PM-3PM and 3PM-6PM through our LaundryPal App."))
        
        
         data.append(FAQ(title: "Is There A Minimum Order?", description: "Yes, the minimum order price is PKR 2000/-. You can also place your laundry orders above 2000 PKR."))
        
        
        data.append(FAQ(title: "How Can I Pay? Can I Pay With A Card?", description: "We keep it simple and reliable for our customers. You can easily pay after receiving your washed laundry.\n \u{2022} Cash on delivery is our only payment receiving method at the moment.\n \u{2022} Card facility will be available soon."))
       
       
       data.append(FAQ(title: "Can I Pay Extra?", description: "Yes, if you pay an extra amount then your remaining amount will be added to your LaundryPal App Wallet. You can use that amount for your next laundry order."))
       
       
       
        data.append(FAQ(title: "Can You Remove All Stains?", description: "We aim to provide best quality laundry services to our customers. Our teams use the highest quality equipment and industry-leading stain removal products available – but some stains can stump even us.\nWe’ll always try our best. Let us know in the app or tell our driver which items have stains you would like us to pay attention to."))
       
       
        data.append(FAQ(title: "Are There Any Materials You Can’t Process?", description: "We cannot process any articles having zaree work, bridals, heavy embroidery, leather and containing hazardous materials or that have been soiled in blood. Our team will inform you as soon as possible of any item that cannot be cleaned and we will return it hung or folded, but un-cleaned. If we receive and can process any of these items, they will be dealt with on a case-by-case basis and standard turnaround times do not apply."))
       
       
        data.append(FAQ(title: "How Do I Check The Status Of My Order?", description: "Once your laundry is picked, you can keep tracking your laundry status from our LaundryPal App. In this way, you can be updated about your laundry throughout the process."))
        
        data.append(FAQ(title: "What If My Clothes Are Lost?", description: "At LaundryPal, we take responsibility for missing laundry items. That is why we ensure that all your items are counted thoroughly before taking your order. In case of any negligence, we will be responsible to refund five times the cost of cleaning of that particular item."))
        
        
        
         data.append(FAQ(title: "What If My Clothes Get Damaged During Washing Or Ironing?", description: "We do not take the responsibility of any damaged or disfigured clothing item. Please go through the precondition of all your clothing items before delivering them to the rider for laundry."))
        
        
         data.append(FAQ(title: "Can I Add Extra Items To My Order When The Driver Comes To Collect?", description: "Yes, you can definitely add extra items when the rider arrives to pick your laundry. Inform the rider about your additional item so he can update your order accordingly."))
        
        
         data.append(FAQ(title: "What Should I Prepare For My Laundry Pickup?", description: "We suggest pre-arranging all your laundry items separately for Dry Cleaning, Wash & Iron, and Iron only apparel. When the rider arrives to pick your laundry inform him accordingly about each item. Our rider will bring separate bags for each laundry requirement."))
    }
    
    
    //MARK: - Actions
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Methods
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension FAQViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(QuestionTableViewCell.self, indexPath: indexPath)
        cell.selectionStyle = .none
        cell.indexPath = indexPath
        cell.delegate = self
        cell.configure(faq: data[indexPath.row])
        return cell
    }
}


//MARK: - FAQDelegate
extension FAQViewController: FAQDelegate {
    
    func showDescription(indexPath: IndexPath) {
        data[indexPath.row].isCollapsed.toggle()
        self.tableView.reloadData()
    }
}


