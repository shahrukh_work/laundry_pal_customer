//
//  CurrentOrderTableViewCell.swift
//  CleaningPal
//
//  Created by Mac on 18/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class CurrentOrderTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var rsLabel: UILabel!
    @IBOutlet weak var itemValueLabel: UILabel!
    

    //MARK: - Variable


    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - configure
    func configure (currentOrder: OrderTableViewEntry) {
        itemLabel.text = currentOrder.item
        itemValueLabel.text = currentOrder.itemValue
        rsLabel.text = ""
        
        if currentOrder.isPrice {
            rsLabel.text = "Rs."
        } else {
            rsLabel.text = ""
        }
    }
    
    //MARK: - Actions
    
    
    //MARK: - Methods
}
