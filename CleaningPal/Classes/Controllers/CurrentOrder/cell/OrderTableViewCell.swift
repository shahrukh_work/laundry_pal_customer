//
//  OrderTableViewCell.swift
//  CleaningPal
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {
    
    
    //MARK: - Outlets
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    //MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - Public Method
    func configure(data: OrderHistroy) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        
        let timeFormatter = DateFormatter()
        timeFormatter.locale = Locale(identifier: "en_US_POSIX")
        timeFormatter.dateFormat = "h a"
        timeFormatter.timeZone = TimeZone(abbreviation: "GMT")
        
        let dateFormatterMonthDay = DateFormatter()
        dateFormatterMonthDay.locale = Locale(identifier: "en_US_POSIX")
        dateFormatterMonthDay.dateFormat = "MMMM d"
        dateFormatterMonthDay.timeZone = TimeZone(abbreviation: "GMT")
        
        let startTime = dateFormatter.date(from: "1111-11-11T\(data.availableStartTime).000Z")
        let endTime = dateFormatter.date(from: "1111-11-11T\(data.availableEndTime).000Z")
        let monthDay = dateFormatter.date(from: data.pickupDateTime)
        
        let startTimeString = timeFormatter.string(from: startTime ?? Date())
        let endTimeString = timeFormatter.string(from: endTime ?? Date())
        let monthDayString = dateFormatterMonthDay.string(from: monthDay ?? Date())
        
        dateLabel.text = "\(monthDayString ), \(startTimeString ) - \(endTimeString)"
        priceLabel.text = "\(data.totalPrice - data.discountedPrice)"
        orderIdLabel.text = "Order #\(data.orderId)"
    }
}
