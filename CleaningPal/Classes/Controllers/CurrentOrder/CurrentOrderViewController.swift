//
//  CurrentOrderViewController.swift
//  CleaningPal
//
//  Created by Mac on 18/05/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class CurrentOrderViewController: UIViewController {
    
    
    //MARK: - Variables
    var currentOrder: [OrderTableViewEntry] = []
    var orderHistory = [OrderHistroy]()
    
    
    //MARK: - Outlet
    @IBOutlet weak var currentOrderView: UIView!
    @IBOutlet weak var deliveredImageView: UIImageView!
    @IBOutlet weak var processImageView: UIImageView!
    @IBOutlet weak var pickupImageView: UIImageView!
    @IBOutlet weak var orderPlaceImageView: UIImageView!
    @IBOutlet weak var curvedView: UIView!
    @IBOutlet weak var historyDownArrowImageView: UIImageView!
    @IBOutlet weak var currentOrderButton: UIButton!
    @IBOutlet weak var currentOrderDownArrowImageView: UIImageView!
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var currentOrderTableView: UITableView!
    @IBOutlet weak var noCurrentOrderView: UIView!
    @IBOutlet weak var orderPlacedProgressView: UIView!
    @IBOutlet weak var orderPickupProgressView: UIView!
    @IBOutlet weak var orderProcessedProgressView: UIView!
    @IBOutlet weak var noOrderHistoryView: UIView!
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        Utility.cornerRadiusPostioned(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], view: curvedView, cornerRadius: 16)
        tableView.delegate = self
        tableView.dataSource = self
        currentOrderTableView.delegate = self
        currentOrderTableView.dataSource = self
        self.historyDownArrowImageView.isHidden = true
        self.currentOrderDownArrowImageView.isHidden = false
        getOrderHistory()
        getCurrentOrder()
    }
    
    
    //MARK: - Actions
    @IBAction func historyButtonTapped(_ sender: Any) {
        
        UIView.animate(withDuration: 0.1) {
            self.currentOrderButton.setTitleColor(#colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1), for: .normal)
            self.historyButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.historyDownArrowImageView.isHidden = false
            self.currentOrderDownArrowImageView.isHidden = true
            self.currentOrderView.alpha = 0.0
            self.noOrderHistoryView.isHidden = !self.orderHistory.isEmpty
            self.currentOrderView.isUserInteractionEnabled = false
        }
        
    }
    
    @IBAction func currentOrderButtonTapped(_ sender: Any) {
        
        UIView.animate(withDuration: 0.1) {
            self.currentOrderButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self.historyButton.setTitleColor(#colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1), for: .normal)
            self.historyDownArrowImageView.isHidden = true
            self.currentOrderDownArrowImageView.isHidden = false
            self.currentOrderView.alpha = 1.0
            self.currentOrderView.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        (self.slideMenuController()?.rightViewController as? SideMenuViewController)?.switchToHome()
    }
    
    @IBAction func orderNowTapped(_ sender: Any) {
        (self.slideMenuController()?.rightViewController as? SideMenuViewController)?.switchToHome()
    }
    
    
    //MARK: - Private Methods
    private func getOrderHistory() {
        
        OrderHistories.getOrderHistory(offset: 0, limit: 10) { (data, error) in
            
            if error == nil {
                
                if let data = data {
                    self.orderHistory = data.orderHistory
                    self.tableView.reloadData()
                }
                
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
    
    private func getCurrentOrder() {
        Utility.showLoading()
        
        CurrentOrder.getCurrentOrder { (data, error) in
            
            Utility.hideLoading()
            
            if data?.data.isEmpty ?? true {
                self.noCurrentOrderView.isHidden = false
                return
            }
            
            if error == nil {
                
                if let data = data?.data.first {
                    
                    self.currentOrder.removeAll()
                    self.noCurrentOrderView.isHidden = true
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                    dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
                    
                    let timeFormatter = DateFormatter()
                    timeFormatter.locale = Locale(identifier: "en_US_POSIX")
                    timeFormatter.dateFormat = "h a"
                    timeFormatter.timeZone = TimeZone(abbreviation: "GMT")
                    
                    let dateFormatterMonthDay = DateFormatter()
                    dateFormatterMonthDay.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatterMonthDay.dateFormat = "MMMM d"
                    dateFormatterMonthDay.timeZone = TimeZone(abbreviation: "GMT")
                    
                    let startTime = dateFormatter.date(from: "1111-11-11T\(data.availableStartTime).000Z")
                    let endTime = dateFormatter.date(from: "1111-11-11T\(data.availableEndTime).000Z")
                    let monthDay = dateFormatter.date(from: data.pickupDateTime)
                    
                    let startTimeString = timeFormatter.string(from: startTime ?? Date())
                    let endTimeString = timeFormatter.string(from: endTime ?? Date())
                    let monthDayString = dateFormatterMonthDay.string(from: monthDay ?? Date())
                    
                    let dateTimeString = "\(monthDayString ), \(startTimeString ) - \(endTimeString)"
                    
                    let obj1 = OrderTableViewEntry()
                    obj1.item = "Order ID"
                    obj1.itemValue = "#\(data.id)"
                    
                    let obj2 = OrderTableViewEntry()
                    obj2.item = "No of items"
                    obj2.itemValue = "\(data.totalItems)"
                    
                    let obj3 = OrderTableViewEntry()
                    obj3.item = "Price"
                    obj3.itemValue = "\(data.totalPrice)"
                    obj3.isPrice = true
                    
                    let obj4 = OrderTableViewEntry()
                    obj4.item = "You Saved"
                    obj4.itemValue = "\(data.discountedPrice)"
                    obj4.isPrice = true
                    
                    let obj5 = OrderTableViewEntry()
                    obj5.item = "Pay with"
                    obj5.itemValue = data.paymentMethod
                    
                    let obj6 = OrderTableViewEntry()
                    obj6.item = "Date & Time"
                    obj6.itemValue = dateTimeString
                    
                    let obj7 = OrderTableViewEntry()
                    obj7.item = "Address"
                    obj7.itemValue = data.pickupAddress
                    
                    self.orderPlaceImageView.image = #imageLiteral(resourceName: "circle_unfilled")
                    self.pickupImageView.image = #imageLiteral(resourceName: "circle_unfilled")
                    self.processImageView.image = #imageLiteral(resourceName: "circle_unfilled")
                    self.deliveredImageView.image = #imageLiteral(resourceName: "circle_unfilled")
                    
                    self.orderPickupProgressView.backgroundColor = #colorLiteral(red: 0.7960784314, green: 0.7960784314, blue: 0.7960784314, alpha: 1)
                    self.orderPlacedProgressView.backgroundColor = #colorLiteral(red: 0.7960784314, green: 0.7960784314, blue: 0.7960784314, alpha: 1)
                    self.orderProcessedProgressView.backgroundColor = #colorLiteral(red: 0.7960784314, green: 0.7960784314, blue: 0.7960784314, alpha: 1)
                    
                    
                    switch data.status {
                    case 1, -1:
                        self.orderPlaceImageView.image = #imageLiteral(resourceName: "check_process")
                        self.orderPlacedProgressView.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.8745098039, alpha: 1)
                        
                    case 8, 4:
                        self.orderPlaceImageView.image = #imageLiteral(resourceName: "check_process")
                        self.orderPlacedProgressView.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.8745098039, alpha: 1)
                        
                        self.pickupImageView.image = #imageLiteral(resourceName: "check_process")
                        self.orderPickupProgressView.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.8745098039, alpha: 1)
                        
                    case 5:
                        self.orderPlaceImageView.image = #imageLiteral(resourceName: "check_process")
                        self.orderPlacedProgressView.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.8745098039, alpha: 1)
                        
                        self.pickupImageView.image = #imageLiteral(resourceName: "check_process")
                        self.orderPickupProgressView.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.8745098039, alpha: 1)
                        
                        self.processImageView.image = #imageLiteral(resourceName: "check_process")
                        self.orderProcessedProgressView.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.8745098039, alpha: 1)
                        
                    case 2:
                        
                        self.orderPlaceImageView.image = #imageLiteral(resourceName: "check_process")
                        self.orderPlacedProgressView.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.8745098039, alpha: 1)
                        
                        self.pickupImageView.image = #imageLiteral(resourceName: "check_process")
                        self.orderPickupProgressView.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.8745098039, alpha: 1)
                        
                        self.processImageView.image = #imageLiteral(resourceName: "check_process")
                        self.orderProcessedProgressView.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.8745098039, alpha: 1)
                        
                        self.deliveredImageView.image = #imageLiteral(resourceName: "check_process")
                    default:
                        break
                    }
                    
                    self.currentOrder.append(obj1)
                    self.currentOrder.append(obj2)
                    self.currentOrder.append(obj3)
                    self.currentOrder.append(obj4)
                    self.currentOrder.append(obj5)
                    self.currentOrder.append(obj6)
                    self.currentOrder.append(obj7)
                    self.currentOrderTableView.reloadData()
                }
                
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
}


//MARK: - TableView Delegate & DataSource
extension CurrentOrderViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tableView {
            self.noOrderHistoryView.isHidden = !orderHistory.isEmpty
            return orderHistory.count
        }
        return currentOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tableView {
            let cell = tableView.register(OrderTableViewCell.self, indexPath: indexPath)
            cell.configure(data: orderHistory[indexPath.row])
            cell.selectionStyle = .none
            return cell
        }
        
        let cell = tableView.register(CurrentOrderTableViewCell.self, indexPath: indexPath)
        cell.configure(currentOrder: currentOrder[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let controller = OrderHistoryViewController()
//        controller.delegate = self
//        controller.modalPresentationStyle = .overCurrentContext
//        self.present(controller, animated: true)
    }
}

