//
//  SettingsViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/24/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var notificationsSwitch: UISwitch!
    @IBOutlet weak var outerViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - View Setup
    func setupView() {
        containerView.cornerRadius = 20
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        getNotification()
    }
    
    
    //MARK: - Actions
    @IBAction func termsAndConditions(_ sender: Any) {
        if let link = URL(string: "https://laundrypal.pk/service-terms/") {
          UIApplication.shared.open(link)
        }
    }
    
    @IBAction func editProfileButtonTapped(_ sender: Any) {
        let controller = EditProfileViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func appRatingButtonTapped(_ sender: Any) {
    }
    
    @IBAction func notificationSwitchValueChanged(_ sender: Any) {
    }
    
    @IBAction func facebookButtonTapped(_ sender: Any) {
    }
    
    @IBAction func twitterButtonTapped(_ sender: Any) {
    }
    
    @IBAction func instagramButtonTapped(_ sender: Any) {
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        (self.slideMenuController()?.rightViewController as? SideMenuViewController)?.switchToHome()
    }
    
    @IBAction func notificationSwitchChanged(_ sender: Any) {
        setNotification(setting: notificationsSwitch.isOn ? 1 : 0)
    }
    
    //MARK: - Private Methods
    func getNotification() {
        Utility.showLoading()
        
        Notification.getNotification { (data, error) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = data {
                    self.notificationsSwitch.isOn = data.notification == 1
                }
                
            } else {
                Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
    
    func setNotification(setting: Int) {
        Utility.showLoading()
        
        Notification.setNotification(status: setting) { ( result, error) in
            
            Utility.hideLoading()
            
            if error != nil {
                 Utility.showAlertController(self, error?.localizedDescription ?? "")
            }
        }
    }
}
