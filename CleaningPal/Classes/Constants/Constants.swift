//
//  Constants.swift
//  ShaeFoodDairy
//
//  Created by Bilal Saeed on 3/17/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

let kApplicationWindow = Utility.getAppDelegate()!.window
let kGoogleMapsAPI = "AIzaSyDjjNiKps3y27gM7TpL6cyxR01YBnvKcOQ"
var showLoader = false

struct APIRoutes {
    
    static var baseUrl: String {
        
        get {
            if let url = UserDefaults.standard.string(forKey: "base_url") {
                return url
                
            } else {
                UserDefaults.standard.set("https://cleaningpal-backend.codesorbit.com", forKey: "base_url")
                return "https://cleaningpal-backend.codesorbit.com"
            }
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: "base_url")
        }
    }
    static var profileBase = "/api/customer/customerImage/"
    static var adminImageBase = "/api/admin/adminImages"
    static var categoryImageBase = "/api/admin/categoryImages/"
    static var itemImageBase = "/api/admin/itemImages/"
    static var featureImageBase = "/api/admin/featureImages/"
    static var login = "/api/authorization/auth/customerLogin"
    static var signup = "/api/customer/user/create"
    static var fcmUpdate = "/api/customer/user/updateFcmToken"
    static var updateProfile = "/api/customer/user/update"
    static var setNotification = "/api/customer/settings/setNotification"
    static var getNotification = "/api/customer/settings/getNotification"
    static var getAvailableBalance = "/api/customer/payment/availableCustomerBalance"
    static var getCategories = "/api/customer/feature/categoriesList"
    static var getUserData = "/api/customer/user/getUserDetails"
    static var getOrderHistory = "/api/customer/order/ordersHistory"
    static var rateDriver = "/api/customer/order/rateDriver"
    static var cancelOrder = "/api/customer/order/cancelOrder"
    static var getSubCategories = "/api/customer/item/getSubcategories"
    static var getPriceList = "/api/customer/item/priceList"
    static var getFeatured = "/api/customer/feature/featuredList"
    static var getHomeItems = "/api/customer/item/getItemListHome"
    static var getCurrentOrders = "/api/customer/order/getCurrentOrder"
    static var getPromoCode = "/api/customer/order/validatePromoCode"
    static var createOrder = "/api/customer/order/createOrder"
    static var getBottomFeatured = "/api/customer/feature/endingPromotionsForReceipt"
    static var createEasyOrder = "/api/customer/order/createEasyOrder"
    static var sendComplaint = "/api/customer/help/sendComplaint"
    static var searchItems = "/api/customer/item/searchItems"
    static var getOnlineAdmin = "/api/customer/chat/getOnlineAdmin"
    static var getChatHistory = "/api/customer/chat/getChatHistoy"
    static var getGST = "/api/customer/order/getGST"
    static var getCardStatus = "/api/customer/settings/getPaymentMethodStatus?payment_method_name=card"
    static var getNotificationList = "/api/customer/settings/getNotificationList"
    static var logoutUser = "/api/authorization/auth/customerLogout"
    static var customerStatus = "/api/authorization/auth/getCustomerStatus"
    static var calculatePrice = "/api/customer/order/priceCalutation"
    static var getLastOrderStatus = "/api/customer/order/getLastOrderRateStatus"
    static var rateOrder = "/api/customer/order/rateOrder"
    static var updateDistance = "/api/rider/order/updateDistance"
}

enum CardTypes: String {
    
    case visa = "Visa Card"
    case masterCard = "Mastercard"
}

enum PaymentType: String {
    case card = "Credit or Debit Card"
    case cod = "Cash on Delivery"
    case none = "Credit or Debit Card/Cash on Delivery"
}

enum DeliveryType: String {
    case express = "Express"
    case standard = "Standard"
}

enum OrderTypes {
    case easy
    case normal
}
