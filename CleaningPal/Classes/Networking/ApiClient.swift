
import UIKit
import Alamofire
import ObjectMapper

class Connectivity {
    
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

let APIClientDefaultTimeOut = 40.0

class APIClient: APIClientHandler {
    
    fileprivate var clientDateFormatter: DateFormatter
    var isConnectedToNetwork: Bool?
    
    static var shared: APIClient = {
        let baseURL = URL(fileURLWithPath: APIRoutes.baseUrl)
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = APIClientDefaultTimeOut
        
        let instance = APIClient(baseURL: baseURL, configuration: configuration)
        
        return instance
    }()
    
    // MARK: - init methods
    
    override init(baseURL: URL, configuration: URLSessionConfiguration, delegate: SessionDelegate = SessionDelegate(), serverTrustPolicyManager: ServerTrustPolicyManager? = nil) {
        clientDateFormatter = DateFormatter()
        
        super.init(baseURL: baseURL, configuration: configuration, delegate: delegate, serverTrustPolicyManager: serverTrustPolicyManager)
        
        //        clientDateFormatter.timeZone = NSTimeZone(name: "UTC")
        clientDateFormatter.dateFormat = "yyyy-MM-dd" // Change it to desired date format to be used in All Apis
    }
    
    
    // MARK: Helper methods
    
    func apiClientDateFormatter() -> DateFormatter {
        return clientDateFormatter.copy() as! DateFormatter
    }
    
    fileprivate func normalizeString(_ value: AnyObject?) -> String {
        return value == nil ? "" : value as! String
    }
    
    fileprivate func normalizeDate(_ date: Date?) -> String {
        return date == nil ? "" : clientDateFormatter.string(from: date!)
    }
    
    var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    func getUrlFromParam(apiUrl: String, params: [String: AnyObject]) -> String {
        var url = apiUrl + "?"
        
        for (key, value) in params {
            url = url + key + "=" + "\(value)&"
        }
        url.removeLast()
        return url
    }
    
    
    
    // MARK: - SignIn / SignUp
    @discardableResult
    func login(phone: String, token: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.login, parameters: nil, httpMethod: .post, headers:["Content-Type":  "application/json", "phone": phone, "token": token], completionBlock: completionBlock)
    }
    
    @discardableResult
    func signup(firstName:String, lastName: String, phone: String, email: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["first_name": firstName, "last_name": lastName, "email": email, "phone": phone] as [String: AnyObject]
        return sendRequest(APIRoutes.signup, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func fcmUpdate(fcmToken:String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["fcm_token": fcmToken] as [String: AnyObject]
        return sendRequest(APIRoutes.fcmUpdate, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    func updateProfileData(firstName: String, lastName: String, email: String, phoneNumber: String, profileImage: UIImage?, completion: @escaping APIClientCompletionHandler) {
        
        let tokenString = "Bearer \(AppUser.authData?.token ?? "")"
        let header: HTTPHeaders = ["Authorization":tokenString]
        let parameters = ["first_name": firstName, "last_name": lastName, "email": email, "phone": phoneNumber]
        let imgData = (profileImage ?? UIImage()).jpegData(compressionQuality: 0.7)!
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        },
                         to:APIRoutes.baseUrl + APIRoutes.updateProfile, headers: header)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    switch response.result {
                        
                    case .success:
                        self.showRequestDetailForSuccess(responseObject: response)
                        completion(response.result.value as AnyObject?, nil, 0)
                        break
                        
                    case .failure(let error):
                        self.showRequestDetailForFailure(responseObject: response)
                        completion(nil, error as NSError?, 0)
                        break
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    @discardableResult
    func setNotification(status: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["status": status] as [String: AnyObject]
        return sendRequest(APIRoutes.setNotification, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getNotification(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getNotification, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getBalance(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getAvailableBalance, parameters: nil, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getCategories(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getCategories, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getUserData(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getUserData, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getOrderHistory(offset: Int, limit: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["offset": offset, "limit": limit] as [String: AnyObject]
        return sendRequest(APIRoutes.getOrderHistory, parameters: params, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func rateDriver(driverId: Int, performance: String, stars: Double, message: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["driver_id": driverId, "performance": performance, "stars": stars, "message": message] as [String: AnyObject]
        return sendRequest(APIRoutes.rateDriver, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func cancelOrder(orderId: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["order_id": orderId] as [String: AnyObject]
        return sendRequest(APIRoutes.cancelOrder, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getSubCategories(categoryId: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request? {
        let params = ["category_id": categoryId] as [String: AnyObject]
        
        return rawRequest(url: APIRoutes.baseUrl + APIRoutes.getSubCategories, method: .post, parameters: params, headers: ["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getPriceList(subCatId: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["subcategory_id": subCatId] as [String: AnyObject]
        return sendRequest(APIRoutes.getPriceList, parameters: params, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getFeatured(offset: Int, limit: Int, featuredType: Int, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["offset": offset, "limit": limit, "featured_type": featuredType] as [String: AnyObject]
        return sendRequest(APIRoutes.getFeatured, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getHomeItemList(offset: Int, query: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request? {
        
        return rawRequest(url: APIRoutes.baseUrl + APIRoutes.getHomeItems + query, method: .get, parameters: ["offset": offset, "limit": 10] as [String: AnyObject], headers: ["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock, encoding: URLEncoding.default)
    }
    
    @discardableResult
    func getCurrentOrders( _ completionBlock: @escaping APIClientCompletionHandler) -> Request? {
        
        return rawRequest(url: APIRoutes.baseUrl + APIRoutes.getCurrentOrders, method: .get, parameters: nil, headers: ["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getPromoCode(promoCode: String, city: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        let params = ["promo_code": promoCode, "city": city, "customer_id": AppUser.authData?.user.id ?? -1] as [String: AnyObject]
        return sendRequest(APIRoutes.getPromoCode, parameters: params, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func placeOrders(params: [String: AnyObject], _ completionBlock: @escaping APIClientCompletionHandler) -> Request? {
        
        return rawRequest(url: APIRoutes.baseUrl + APIRoutes.createOrder, method: .post, parameters: params, headers: ["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getBottomFeatured(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        return sendRequest(APIRoutes.getBottomFeatured, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func createEasyOrder(params: [String: AnyObject], _ completionBlock: @escaping APIClientCompletionHandler) -> Request? {
        
        return rawRequest(url: APIRoutes.baseUrl + APIRoutes.createEasyOrder, method: .post, parameters: params, headers: ["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func sendComplaint(email: String, message: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request? {
        let params = ["email": email, "complaint": message] as [String: AnyObject]
        
        return rawRequest(url: APIRoutes.baseUrl + APIRoutes.sendComplaint, method: .post, parameters: params, headers: ["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func searchItems(searchTerm: String, catId: String, subCatId: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        return rawRequest(url: APIRoutes.baseUrl + APIRoutes.searchItems + "?search=\(searchTerm)&category_id=\(catId)&sub_category_id=\(subCatId)", method: .get, parameters: nil, headers: ["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getOnlineAdmin(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        return sendRequest(APIRoutes.getOnlineAdmin, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getChatHistory(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        return sendRequest(APIRoutes.getChatHistory, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getGST(state: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        return sendRequest(APIRoutes.getGST + "?state=\(state)", parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getCardStatus(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        return rawRequest(url: APIRoutes.baseUrl + APIRoutes.getCardStatus, method: .get, parameters: nil, headers: ["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getNotificationList(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        return sendRequest(APIRoutes.getNotificationList , parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func logoutUser(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        return sendRequest(APIRoutes.logoutUser , parameters: nil, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func customerStatus(phone: String, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        let params = ["number": phone] as [String: AnyObject]
        
        return sendRequest(APIRoutes.customerStatus , parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func calculatePrice(params: [String: AnyObject], _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        return sendRequest(APIRoutes.calculatePrice, parameters: params, httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func rateOrder(orderId: String, performance: String, star: String, comments: String, isSkip: Bool, _ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        
        var params : [String: Any] = [:]
        params["order_id"] = orderId
        params["performance"] = performance
        params["stars"] = star
        params["comments"] = comments
        params["is_skip"] = isSkip
        return sendRequest(APIRoutes.rateOrder, parameters: params as [String : AnyObject], httpMethod: .post, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
    
    @discardableResult
    func getLastOrderStatus (_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.getLastOrderStatus, parameters: nil, httpMethod: .get, headers:["Content-Type":  "application/json", "Authorization": "Bearer \(AppUser.authData?.token ?? "")"], completionBlock: completionBlock)
    }
}

