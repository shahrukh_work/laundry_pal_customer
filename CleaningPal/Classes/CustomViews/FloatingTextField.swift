//
//  FloatingTextField.swift
//  CleaningPal
//
//  Created by Mian Faizan Nasir on 6/18/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import UIKit

class FloatingTextField: UITextField {
    
    var isAlreadyFloating = false
    
    func floatTitleLabel(titleLabel: UILabel) {
        
        UIView.animate(withDuration: 0.3) {
            
            if self.text == "" {
                self.isAlreadyFloating = false
                titleLabel.alpha = 0
                self.placeholder = titleLabel.text
                
            } else {
                if !self.isAlreadyFloating {
                    self.isAlreadyFloating = true
                    self.placeholder = ""
                    titleLabel.alpha = 1
                }
            }
        }
    }
}
