//
//  HomeCollectionLayout.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 7/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class HomeCollectionLayout: UICollectionViewFlowLayout {

    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
}
