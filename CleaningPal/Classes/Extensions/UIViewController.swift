//
//  UIVIewController.swift
//  Face Match
//
//  Created by Work on 15/04/2019.
//  Copyright © 2019 Work. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

extension UIViewController {
    
    func showLoading(offSet: CGFloat = 0) {
        let superView = UIView(frame: CGRect(x: 0, y: 0 - offSet, width: kApplicationWindow?.frame.width ?? 0.0, height: self.view.frame.height))
        let activityIndicator = NVActivityIndicatorView(frame: CGRect(x: superView.frame.width/2 - 32.5, y: superView.frame.height/2 - 32.5, width: 65, height: 65))
        let iconImageView = UIImageView(frame: CGRect(x: superView.frame.width/2 - 32.5, y: superView.frame.height/2 - 32.5, width: 65, height: 65))
     
        superView.backgroundColor = .clear
        superView.tag = 90000
        activityIndicator.type = .circleStrokeSpin
        activityIndicator.color = #colorLiteral(red: 1, green: 0.293159306, blue: 0, alpha: 1)
        activityIndicator.startAnimating()
        superView.addSubview(iconImageView)
        superView.addSubview(activityIndicator)
        superView.bringSubviewToFront(activityIndicator)
        superView.bringSubviewToFront(iconImageView)
     
        self.view.addSubview(superView)
        self.view.bringSubviewToFront(superView)
    }
    
    func hideLoading() {
     
        if let activityView = self.view.viewWithTag(90000) {
            self.view.isUserInteractionEnabled = true
            activityView.removeFromSuperview()
        }
    }
    
    func addCustomBackButton(with color: UIColor = .black) {
        let backButton: UIButton = UIButton (type: UIButton.ButtonType.custom)
        backButton.tintColor = UIColor.black
        
        if color == .white {
            backButton.setImage(#imageLiteral(resourceName: "ic-back-white"), for: UIControl.State.normal)
            
        } else {
            backButton.setImage(#imageLiteral(resourceName: "backButton"), for: UIControl.State.normal)
        }
        
        backButton.addTarget(self, action: #selector(self.backButtonPressed(button:)), for: UIControl.Event.touchUpInside)
        backButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        let barButton = UIBarButtonItem(customView: backButton)
        
        navigationItem.leftBarButtonItem = barButton
    }
    
    func addCustomCrossButton() {
        let crossButton: UIButton = UIButton (type: UIButton.ButtonType.custom)
        crossButton.setImage(UIImage(named: "cancel"), for: UIControl.State.normal)
        
        crossButton.addTarget(self, action: #selector(self.barCancelButtonTapped(button:)), for: UIControl.Event.touchUpInside)
        
        crossButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: crossButton)
        
        navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func backButtonPressed(button : UIButton) {
        
        if navigationController != nil {
            navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
            
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func barCancelButtonTapped(button: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func backButtonTapped(button: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func topMostViewController() -> UIViewController {
        
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController!.topMostViewController()
        }
        
        if let tab = self as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        
        if self.presentedViewController == nil {
            return self
        }
        
        if let navigation = self.presentedViewController as? UINavigationController {
            return navigation.visibleViewController!.topMostViewController()
        }
        
        if let tab = self.presentedViewController as? UITabBarController {
            
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            
            return tab.topMostViewController()
        }
        
        return self.presentedViewController!.topMostViewController()
    }
    
    func dismissOnTap() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissViewControllerOnTap))
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        self.view.isUserInteractionEnabled = true
    }
    
    @objc func dismissViewControllerOnTap(gesture: UIGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showOkAlert(_ msg: String){
        let alert = UIAlertController(title:"", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func showOkAlertOnDetachedController(_ msg: String, completion: ((UIAlertAction) -> Void)? = nil){
        let alert = UIAlertController(title:"", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: completion)
        alert.addAction(okAction)
        self.view.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func presentOnDetachedController(vc: UIViewController, animated: Bool, completion: (() -> Void)? = nil){
        self.view.window?.rootViewController?.present(vc, animated: animated, completion: completion)
    }
    
    func showOkAlertWithOKCompletionHandler(_ msg: String, completion: @escaping (UIAlertAction) -> Void){
        let alert = UIAlertController(title:"", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: completion)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func addGradientView() {
     //   let gradientView = RadialGradientView(frame: CGRect(x: self.view.frame.width - 350 / 2, y: 30, width: 350, height: 350))
        let gradientImageView = UIImageView(frame: CGRect(x: self.view.frame.width - 350 / 2, y: 0, width: 450, height: 350))
        gradientImageView.image = #imageLiteral(resourceName: "AppGradient")
        gradientImageView.contentMode = .scaleAspectFill
        self.view.addSubview(gradientImageView)
        self.view.sendSubviewToBack(gradientImageView)
    }
}

extension UIViewController: UIGestureRecognizerDelegate {
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}


extension UIViewController {

    var isModal: Bool {

        let presentingIsModal = presentingViewController != nil
        let presentingIsNavigation = navigationController?.presentingViewController?.presentedViewController == navigationController
        let presentingIsTabBar = tabBarController?.presentingViewController is UITabBarController

        return presentingIsModal || presentingIsNavigation || presentingIsTabBar
    }
}
