//
//  UITabBarController.swift
//  CleaningPal
//
//  Created by Bilal Saeed on 6/29/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import UIKit

extension UITabBarController {
    
    func setTabBarVisible(visible:Bool, duration: TimeInterval) {
        
        self.tabBar.isHidden = !visible
        
        /*if (tabBarIsVisible() == visible) { return }
        let frame = self.tabBar.frame
        let height = frame.size.height
        let offsetY = (visible ? -height : height)

        // animation
        UIViewPropertyAnimator(duration: duration, curve: .linear) {
            _ = self.tabBar.frame.offsetBy(dx:0, dy:offsetY)
            self.view.frame = CGRect(x:0,y:0,width: self.view.frame.width, height: self.view.frame.height)
            self.view.setNeedsDisplay()
            self.view.layoutIfNeeded()
        }.startAnimation()*/
    }

    func tabBarIsVisible() ->Bool {
        return self.tabBar.frame.origin.y < UIScreen.main.bounds.height
    }
}
