//
//  UITextField.swift
//  ShaeFoodDairy
//
//  Created by Mac on 17/03/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

extension UITextField {
    var substituteFontName : String {
        get { return self.font!.fontName }
        set { self.font = UIFont(name: newValue, size: (self.font?.pointSize)!) }
    }
    
    func floatTitle(float: Bool, titleLabel: UILabel) {
        
        UIView.animate(withDuration: 0.1) {
            
            if float {
                titleLabel.alpha = 1
                self.placeholder = ""
            
            } else {
                titleLabel.alpha = 0
                self.placeholder = titleLabel.text
            }
        }
    }
}
