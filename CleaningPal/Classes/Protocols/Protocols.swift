//
//  Protocols.swift
//  ShaeFoodDairy
//
//  Created by Mac on 17/03/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit

protocol EmailTableViewCellDelegate {
    func sendEmail(email: String, comments: String)
}

protocol ItemListTableViewCellDelegate: AnyObject {
    func headerTapped(indexPath: IndexPath)
}

protocol CategoryItemCellDelegate: AnyObject {
    func addTapped (indexPath: IndexPath)
    func serviceSelected (selectedServiceType: Int, indexPath: IndexPath)
}

protocol PaymentMethodViewControllerDelegate: AnyObject {
    func paymentMethodSelected(method: PaymentType)
}

protocol CartItemTableViewCellDelegate: AnyObject {
    func cartItemQuantityChanged()
}

protocol AddressTableViewCellDelegate: AnyObject {
    func addAddressTapped()
    func notesChanged(text: String)
}

protocol PromoCodeTableViewCellDelegate: AnyObject {
    func verifyTapped(promoCode: String)
}

protocol TypeOfDeliveryTableViewCellDelegate: AnyObject {
    func deliveryTypeChange(deliveryType: DeliveryType)
}

protocol SchedulePickupTableViewCellDelegate: AnyObject {
    func dateSelected(date: Date)
    func timeChanged(startTime: String, endTime: String)
}

protocol StarchTableViewCellDelegate: AnyObject {
    func descriptionTextChanged(text: String)
    func starchSelectionChanged()
}

protocol ReciptViewControllerDelegate: AnyObject {
    func crossPressed()
}

protocol EasyOrderPopupViewControllerDelegate: AnyObject {
    func acceptPressed()
}
