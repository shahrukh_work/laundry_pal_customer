//
//  CustomTableView.swift
//  ShaeFoodDairy
//
//  Created by Bilal Saeed on 4/3/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class CustomTableView: UITableView {
    
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}
