//
//  AppDelegate.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/21/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import IQKeyboardManagerSwift
import FirebaseMessaging
import FirebaseAuth
import netfox
import GoogleMaps
import GooglePlaces


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSPlacesClient.provideAPIKey(kGoogleMapsAPI)
        GMSServices.provideAPIKey(kGoogleMapsAPI)
        
        Utility.autoLogin()
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        self.registerForPushNotification()
        IQKeyboardManager.shared.enable = true
        NFX.sharedInstance().start()
        UNUserNotificationCenter.current().delegate = self
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    func applicationDidBecomeActive(_ application: UIApplication) {

        guard let _ = DataManager.shared.getUser() else {
            return
        }
        
        LastOrderStatus.getLastOrderStatus { (result, error) in
            let vc = self.window?.rootViewController?.topMostViewController()

            if error == nil {
                
                if !(result?.data?.isSkip ?? false) {
                    let rateVC = OrderCompletedViewController()
                    rateVC.orderStatus = result
                    vc?.show(rateVC, sender: nil)
                }
                
            } else {
                Utility.showAlertController(vc!, error?.localizedDescription ?? "")
            }
        }
    }
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func registerForPushNotification(){
        UNUserNotificationCenter.current().delegate = self
        
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
        
                if error == nil{
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }

        } else {
            
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    
    //MARK: - Global Font Settings
    func makeCustomFontGlobal() {
        let customFont = UIFont.font(withSize: 20)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: customFont], for: .normal)
        UITextField.appearance().font = customFont
        UILabel.appearance().font = customFont
        UILabel.appearance().font = customFont
        
    }
}


//MARK: - MessagingDelegate
extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        AppUser.shared.fcm = fcmToken
        DataManager.shared.updateFCM()
    }
}


//MARK: - UNUserNotificationCenterDelegate
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print(notification.request.content.userInfo)
     
        if let aps = notification.request.content.userInfo["aps"] as? [String:Any], let alert = aps["alert"] as? [String:Any], let title = alert["title"] as? String {
            
            if title == "Payment" {
                
                LastOrderStatus.getLastOrderStatus { (result, error) in
                    let vc = self.window?.rootViewController?.topMostViewController()

                    if error == nil {
                        
                        if !(result?.data?.isSkip ?? false) {
                            let rateVC = OrderCompletedViewController()
                            rateVC.orderStatus = result
                            vc?.show(rateVC, sender: nil)
                        }
                        
                    } else {
                        Utility.showAlertController(vc!, error?.localizedDescription ?? "")
                    }
                }
            }
        }
        completionHandler([[.alert, .sound]])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print("didReceive response")
        print(userInfo)
        completionHandler()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // 1. Convert device token to string
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        // 2. Print device token to use for PNs payloads
        print("Device Token: \(token)")
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        Messaging.messaging().appDidReceiveMessage(userInfo)
      }
}
