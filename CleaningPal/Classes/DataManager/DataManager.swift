//
//  DataManager.swift
//  ShaeFoodDairy
//
//  Created by Mac on 01/04/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import ObjectMapper

class DataManager {
    
    static let shared = DataManager()
    
    func setUser (user: String) {
        UserDefaults.standard.set(user, forKey: "user_data")
    }
    
    func getUser() -> AuthUser? {
        var user: AuthUser?

        if UserDefaults.standard.object(forKey: "user_data") != nil {
            user = Mapper<AuthUser>().map(JSONString:UserDefaults.standard.string(forKey: "user_data")!)
        }
        return user
    }
    
    func deleteUser () {
         UserDefaults.standard.set(nil, forKey: "user_data")
    }
    
    func setAuthentication (auth: String) {
        UserDefaults.standard.set(auth, forKey: "auth_data")
    }
    
    func getAuthentication() -> AuthData? {
        var auth: AuthData?
        
        if UserDefaults.standard.object(forKey: "auth_data") != nil {
            auth = Mapper<AuthData>().map(JSONString:UserDefaults.standard.string(forKey: "auth_data")!)
        }
        return auth
    }
    
    func setSkipData (isSkip: Bool) {
        UserDefaults.standard.set(isSkip, forKey: "skip_data")
    }
    
    func deleteSkipData () {
        UserDefaults.standard.set(nil, forKey: "skip_data")
    }
    
    func getSkipData() -> Bool? {
        var skip: Bool?
        
        if UserDefaults.standard.object(forKey: "skip_data") != nil {
            skip = UserDefaults.standard.bool(forKey: "")
        }
        return skip
    }
    
    func deleteAuthentication () {
        UserDefaults.standard.set(nil, forKey: "auth_data")
    }
    
    func getCart() -> Cart {
        
        var cart: Cart?

        if UserDefaults.standard.object(forKey: "user_cart") != nil {
            cart = Mapper<Cart>().map(JSONString:UserDefaults.standard.string(forKey: "user_cart")!)
        }
        return cart ?? Mapper<Cart>().map(JSON: [:])!
    }
    
    func setCart(cart: String) {
        UserDefaults.standard.set(cart, forKey: "user_cart")
    }
    
    func getLocation() -> Location? {
        
        var location: Location?
        
        if UserDefaults.standard.object(forKey: "user_location") != nil {
            location = Mapper<Location>().map(JSONString:UserDefaults.standard.string(forKey: "user_location")!)
        }
        return location
    }
    
    func setLocation(location: String) {
        UserDefaults.standard.set(location, forKey: "user_location")
    }
    
    func refreshUserInfo() {
        AuthUser.getUserData { (_, _) in
            return
        }
    }
    
    func updateFCM() {
        PostApi.fcmUpdate(fcmToken: AppUser.shared.fcm) { (data, error) in
            
            if error == nil {
                print(data?.message ?? "")
            }
        }
    }
}
